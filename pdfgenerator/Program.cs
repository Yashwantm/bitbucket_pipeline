﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure;

namespace pdfgenerator
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main(string[] args)
        {
            var URL = args[0];
            var filename = args[1];
            var profileId = args[2];
            var token = args[3];
            URL = URL + "?profileId=" + profileId + "&token=" + token;
            try
            {
                using (var p = new System.Diagnostics.Process())
                {
                    var startInfo = new System.Diagnostics.ProcessStartInfo
                    {
                        FileName = "Rotativa/wkhtmltopdf.exe",
                        Arguments = URL + " " + filename,
                        UseShellExecute = false
                    };
                    p.StartInfo = startInfo;
                    p.Start();
                    p.WaitForExit();
                    p.Close();
                }
            }
            catch (Exception ex) { Console.WriteLine("Something Happened: {0}", ex.Message); }

            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("pdf");
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                using (var fileStream = System.IO.File.OpenRead(filename))
                {
                    blockBlob.Properties.ContentType = "application/pdf";
                    blockBlob.UploadFromStream(fileStream);
                }
                System.IO.File.Delete(filename);
            }
            catch (StorageException ex) { Console.WriteLine("StorageException: {0}", ex.Message); }
            catch (Exception ex) { Console.WriteLine("Exception: {0}", ex.Message); }

        }
    }
}
