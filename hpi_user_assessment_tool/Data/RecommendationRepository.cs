﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Utility;


namespace hpi_user_assessment_tool.Data
{
    public class RecommendationRepository : IRecommendationRepository
    {
        public Task<CMSAnswer> GetAnswerByIdAsync(int answerId)
        {
            // Logs.Info("RecommendationRepository.GetAnswerByIdAsync method has been called having parameter[answerId: " + answerId + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<CMSAnswer> obj = Task<USRInfo>.Run(() => UATDb.CMSAnswers.FirstOrDefault(p => p.Id == answerId));

            // Logs.Info("The response of RecommendationRepository.GetAnswerByIdAsync method is, Response[CMSAnswer, Id: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Id) + "]");
            return obj;
        }

        public Task<List<CMSAnswer>> GetAnswerByIdsAsync(List<int> answerIds)
        {
            // Logs.Info("RecommendationRepository.GetAnswerByIdsAsync method has been called having parameter[List ofanswerId, Size: " + ((answerIds == null) ? '-' : answerIds.Count) + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<CMSAnswer>> obj = Task<List<CMSAnswer>>.Run(() => UATDb.CMSAnswers.Where(p => answerIds.Contains(p.Id)).ToList<CMSAnswer>());

            // Logs.Info("The response of RecommendationRepository.GetAnswerByIdsAsync method is, Response[List<CMSAnswer>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }

        public Task<List<int>> GetAnswerByLabelsAsync(List<string> answerLabels)
        {
            // Logs.Info("RecommendationRepository.GetAnswerByLabelsAsync method has been called having parameter[List of answerLabels, Size: " + ((answerLabels == null) ? '-' : answerLabels.Count) + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<int>> obj = Task<List<int>>.Run(() => UATDb.CMSAnswers.Where(p => answerLabels.Contains(p.label)).Select(s => s.Id).ToList<int>());

            // Logs.Info("The response of RecommendationRepository.GetAnswerByLabelsAsync method is, Response[List<int>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<CMSAnswer>> GetAnswerByLabels(List<string> answerLabels)
        {
            // Logs.Info("RecommendationRepository.GetAnswerByLabels method has been called having parameter[List of answerLabels, Size: " + ((answerLabels == null) ? '-' : answerLabels.Count) + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<CMSAnswer>> obj = Task<List<int>>.Run(() => UATDb.CMSAnswers.Where(p => answerLabels.Contains(p.label)).ToList<CMSAnswer>());

            // Logs.Info("The response of RecommendationRepository.GetAnswerByLabels method is, Response[List<CMSAnswer>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        
        public List<CMSAnswer> GetAnswersByQuestionAssessmentStepAsync(string assessmentStepLabel)
        {
            // Logs.Info("RecommendationRepository.GetAnswersByQuestionAssessmentStepAsync method has been called having parameter[assessmentStepLabel: " + assessmentStepLabel + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            List<CMSQuestion> questionList = Task<List<CMSQuestion>>.Run(() => UATDb.CMSQuestionAssessmentSteps.Where(p => p.label == assessmentStepLabel).Select(s => s.CMSQuestion).ToList<CMSQuestion>()).Result;
            var cmsAnswers = questionList.SelectMany(p => p.CMSAnswers).ToList();

            // Logs.Info("The response of RecommendationRepository.GetAnswersByQuestionAssessmentStepAsync method is, Response[List<CMSAnswer>, Size: " + ((cmsAnswers == null) ? '-' : cmsAnswers.Count) + "]");
            return cmsAnswers;
        }

        public Task<List<ProductFeature>> GetProductFeaturesByPmoidAsync(string pmoid, string country, string language)
        {
            // Logs.Info("RecommendationRepository.GetProductFeaturesByPmoidAsync method has been called having parameters [ pmoid: " + pmoid + ", country: " + country + ", language: " + language + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductFeature>> obj = Task<List<ProductFeature>>.Run(() => UATDb.ProductFeatures.Where(p => p.pmoid == pmoid && p.country == country && p.language == language).ToList<ProductFeature>());

            // Logs.Info("The response of RecommendationRepository.GetProductFeaturesByPmoidAsync method is, Response[List<ProductFeature>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }

        public Task<List<ProductFeature>> GetProductFeaturesByPmoidsAsync(List<string> pmoids, string country, string language)
        {
            // Logs.Info("RecommendationRepository.GetProductFeaturesByPmoidsAsync method has been called having parameters [ List of pmoid {Size:" + ((pmoids == null) ? '-' : pmoids.Count) + "}, country: " + country + ", language: " + language + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductFeature>> obj = Task<List<ProductFeature>>.Run(() => UATDb.ProductFeatures.Where(p => pmoids.Contains(p.pmoid) && p.country == country && p.language == language).ToList<ProductFeature>());

            // Logs.Info("The response of RecommendationRepository.GetProductFeaturesByPmoidsAsync method is, Response[List<ProductFeature>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<ProductDaasFeature>> GetProductDaasFeaturesByPmoidsAsync(List<string> pmoids, string country, string language)
        {
            // Logs.Info("RecommendationRepository.GetProductDaasFeaturesByPmoidsAsync method has been called having parameters [ List of pmoid {Size:" + ((pmoids == null) ? '-' : pmoids.Count) + "}, country: " + country + ", language: " + language + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductDaasFeature>> obj = Task<List<ProductDaasFeature>>.Run(() => UATDb.ProductDaasFeatures.Where(p => pmoids.Contains(p.pmoid) && p.country == country && p.language == language).ToList<ProductDaasFeature>());

            // Logs.Info("The response of RecommendationRepository.GetProductDaasFeaturesByPmoidsAsync method is, Response[List<ProductFeature>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<ProductDaasFeature>> GetProductDaasFeaturesByPmoidAsync(string pmoid, string country, string language)
        {
            // Logs.Info("RecommendationRepository.GetProductDaasFeaturesByPmoidAsync method has been called having parameters [ List of pmoid {Size:" + ((pmoids == null) ? '-' : pmoids.Count) + "}, country: " + country + ", language: " + language + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductDaasFeature>> obj = Task<List<ProductDaasFeature>>.Run(() => UATDb.ProductDaasFeatures.Where(p => p.pmoid == pmoid && p.country == country && p.language == language).ToList<ProductDaasFeature>());

            // Logs.Info("The response of RecommendationRepository.GetProductDaasFeaturesByPmoidAsync method is, Response[List<ProductFeature>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }


        public Task<List<ProductSku>> GetSkusAsync(String query)
        {
            // Logs.Info("RecommendationRepository.GetSkusAsync method has been called having parameter [query: " + query + "]");
            Debug.WriteLine("Executing Query::" + query);
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<List<ProductSku>>.Run(() => UATDb.ProductSkus.SqlQuery(query).ToList<ProductSku>());
        }
        public Task<List<ProductAccessory>> GetAccessoriesAsync(string query)
        {
            // Logs.Info("RecommendationRepository.GetAccessoriesAsync method has been called having parameter [query: " + query + "]");
            Debug.WriteLine("Executing Query::" + query);
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<List<ProductAccessory>>.Run(() => UATDb.ProductAccessories.SqlQuery(query).ToList<ProductAccessory>());
        }

        public Task<List<ProductDaasService>> GetDaasServicesAsync(string query)
        {
            // Logs.Info("RecommendationRepository.GetDaasAccessoriesAsync method has been called having parameter [query: " + query + "]");
            Debug.WriteLine("Executing Query::" + query);
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<List<ProductDaasService>>.Run(() => UATDb.ProductDaasServices.SqlQuery(query).ToList<ProductDaasService>());
        }

        public Task<List<ProductAccessory>> GetAccessoriesBySkusAsync(List<string> accessorySkuList)
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductAccessory>> obj = Task<List<ProductAccessory>>.Run(() => UATDb.ProductAccessories.Where(p => accessorySkuList.Contains(p.sku)).ToList<ProductAccessory>());
            return obj;
        }

        public Task<List<ProductDaasService>> GetDaasServicesBySkusAsync(List<string> serviceSkuList)
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductDaasService>> obj = Task<List<ProductDaasService>>.Run(() => UATDb.ProductDaasServices.Where(p => serviceSkuList.Contains(p.sku)).ToList<ProductDaasService>());
            return obj;
        }

        public Task<ProductEDWIndustryTrend> GetEDWIndustryTrendsBySeriesPmoid(string query)
        {
            // Logs.InfoFormat("RecommendationRepository.GetEDWIndustryTrendsBySeriesPmoid method has been called having parameter [query: " + query + "]");
            Debug.WriteLine("Executing Query::" + query);
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<ProductEDWIndustryTrend>.Run(() => UATDb.ProductEDWIndustryTrends.SqlQuery(query).FirstOrDefault<ProductEDWIndustryTrend>());
        }

        public Task<ProductSeriesFeatureMatrix> GetSeriesFeatureMatrixBySeriesPmoid(string query)
        {
            // Logs.Info("RecommendationRepository.GetSeriesFeatureMatrixBySeriesPmoid method has been called having parameter [query: " + query + "]");
            Debug.WriteLine("Executing Query::" + query);
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<ProductSeriesFeatureMatrix>.Run(() => UATDb.ProductSeriesFeatureMatrices.SqlQuery(query).FirstOrDefault<ProductSeriesFeatureMatrix>());
        }

        public Task<ProductSeriesTopRecAccessory> GetSeriesTopRecAccessory(string series_PMOID, string accessorySku)
        {
            Logs.Debug("RecommendationRepository.GetSeriesTopRecAccessory method has been called having parameters [series_PMOID: " + series_PMOID + ", accessorySku: " + accessorySku + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<ProductSeriesTopRecAccessory>.Run(() => UATDb.ProductSeriesTopRecAccessories.Where(p => (p.Series_PMOID == series_PMOID && p.partNumber == accessorySku)).FirstOrDefault<ProductSeriesTopRecAccessory>());
        }

        public Task<List<ProductSkuAccessory>> GetProductSkuAccessoryByAccessorySkusAsync(List<string> accessorySKUs, string skuPmoid)
        {
            // Logs.Info("RecommendationRepository.GetProductSkuAccessoryByAccessorySkusAsync method has been called having parameters [List of accessorySKUs {Size: " + accessorySKUs.Count + "}, skuPmoid: " + skuPmoid + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<ProductSkuAccessory>> obj = Task<List<ProductSkuAccessory>>.Run(() => UATDb.ProductSkuAccessories.Where(p => accessorySKUs.Contains(p.accessorySKU) && p.sku_pmoid == skuPmoid).ToList<ProductSkuAccessory>());

            // Logs.Info("The response of RecommendationRepository. method is, Response[List<ProductSkuAccessory>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }

        public Task<List<CMSStandaloneTag>> GetStandaloneFilterTagsAsync()
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<CMSStandaloneTag>> obj = Task<List<CMSStandaloneTag>>.Run(() => UATDb.CMSStandaloneTags.ToList<CMSStandaloneTag>());
            return obj;
        }

        public Task<List<CMSStandaloneWeightedTag>> GetStandaloneWeightedTagsAsync()
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<CMSStandaloneWeightedTag>> obj = Task<List<CMSStandaloneWeightedTag>>.Run(() => UATDb.CMSStandaloneWeightedTags.ToList<CMSStandaloneWeightedTag>());
            return obj;
        }

        public Task<List<CMSReliantStandaloneTag>> GetReliantStandaloneFilterTagsAsync()
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<CMSReliantStandaloneTag>> obj = Task<List<CMSReliantStandaloneTag>>.Run(() => UATDb.CMSReliantStandaloneTags.ToList<CMSReliantStandaloneTag>());
            return obj;
        }

        public Task<List<CMSReliantStandaloneWeightedTag>> GetReliantStandaloneWeightedTagsAsync()
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            Task<List<CMSReliantStandaloneWeightedTag>> obj = Task<List<CMSReliantStandaloneWeightedTag>>.Run(() => UATDb.CMSReliantStandaloneWeightedTags.ToList<CMSReliantStandaloneWeightedTag>());
            return obj;
        }
        public Task<ProductSku> GetSkuAsync(String sku)
        {
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<ProductSku>.Run(() => UATDb.ProductSkus.FirstOrDefault(p => p.sku == sku));
        }
        public float getAccessoryRank(int devicePmoid, string accessorySku)
        {
            // Logs.Info("RecommendationRepository.getAccessoryRank method has been called having  parameters [devicePmoid: " + devicePmoid + ", accessorySku: " + accessorySku + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            string querymax = "select max(rank) from [dbo].[ProductSkuAccessories] where sku_pmoid = " + devicePmoid;
            string queryrank = "select rank from[dbo].[ProductSkuAccessories] where sku_pmoid = " + devicePmoid + " and accessorySKU = '" + accessorySku + "'";

            float calculatedRank = 0;
            try
            {
                string[] max = UATDb.Database.SqlQuery<string>(querymax).ToArray<string>();
                string[] rank = UATDb.Database.SqlQuery<string>(queryrank).ToArray<string>();

                if (max != null && max.Length > 0 && rank != null && rank.Length > 0)
                {
                    float max_f = float.Parse(max[0]);
                    float rank_f = float.Parse(rank[0]);
                    if (max_f > 0)
                    {
                        calculatedRank = 1f - (rank_f / max_f);

                        bool val = true;
                    }
                }
            }
            catch (Exception e)
            {
                //need to log
                Logs.ErrorFormat(e.Message, e);
            }
            // Logs.Info("The response of RecommendationRepository.getAccessoryRank method is calculatedRank: " + calculatedRank);

            return calculatedRank;
        }

        public Task<ProductSku> GetProductSkuByPmoid(string pmoid)
        {
            Logs.Debug("RecommendationRepository.GetProductSkuByPmoid method has been called having parameter [pmoid: " + pmoid + "]");
            UserAssessmentDBContextContainer UATDb = new UserAssessmentDBContextContainer();
            return Task<ProductSku>.Run(() => UATDb.ProductSkus.Where(p => p.pmoid == pmoid).FirstOrDefault<ProductSku>());
        }
    }

    public interface IRecommendationRepository
    {
        Task<CMSAnswer> GetAnswerByIdAsync(int answerId);
        Task<List<CMSAnswer>> GetAnswerByIdsAsync(List<int> answerIds);
        Task<List<int>> GetAnswerByLabelsAsync(List<string> answerLabels);
        Task<List<ProductFeature>> GetProductFeaturesByPmoidAsync(string pmoid, string country, string language);
        Task<List<ProductFeature>> GetProductFeaturesByPmoidsAsync(List<string> pmoids, string country, string language);
        Task<List<ProductDaasFeature>> GetProductDaasFeaturesByPmoidsAsync(List<string> pmoids, string country, string language);
        Task<List<ProductDaasFeature>> GetProductDaasFeaturesByPmoidAsync(string pmoid, string country, string language);
        Task<List<ProductSku>> GetSkusAsync(string query);
        Task<List<ProductAccessory>> GetAccessoriesAsync(string query);
        Task<List<ProductDaasService>> GetDaasServicesAsync(string query);
        Task<ProductEDWIndustryTrend> GetEDWIndustryTrendsBySeriesPmoid(string query);
        Task<ProductSeriesFeatureMatrix> GetSeriesFeatureMatrixBySeriesPmoid(string query);
        Task<ProductSeriesTopRecAccessory> GetSeriesTopRecAccessory(string series_PMOID, string accessorySku);
        float getAccessoryRank(int devicePmoid, string accessorySku);
        Task<ProductSku> GetProductSkuByPmoid(string pmoid);
        Task<List<ProductSkuAccessory>> GetProductSkuAccessoryByAccessorySkusAsync(List<string> accessorySKUs, string skuPmoid);
        List<CMSAnswer> GetAnswersByQuestionAssessmentStepAsync(string assessmentStepLabel);
        Task<List<CMSStandaloneTag>> GetStandaloneFilterTagsAsync();
        Task<List<CMSStandaloneWeightedTag>> GetStandaloneWeightedTagsAsync();
        Task<List<CMSReliantStandaloneTag>> GetReliantStandaloneFilterTagsAsync();
        Task<List<CMSReliantStandaloneWeightedTag>> GetReliantStandaloneWeightedTagsAsync();
        Task<List<ProductAccessory>> GetAccessoriesBySkusAsync(List<string> accessorySkuList);
        Task<List<ProductDaasService>> GetDaasServicesBySkusAsync(List<string> serviceSkuList);
        Task<ProductSku> GetSkuAsync(String sku);
        Task<List<CMSAnswer>> GetAnswerByLabels(List<string> answerLabels);
    }
}
