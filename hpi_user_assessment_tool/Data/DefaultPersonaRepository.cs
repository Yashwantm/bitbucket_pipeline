﻿using hpi_user_assessment_tool.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Utility;


namespace hpi_user_assessment_tool.Data
{
    public class DefaultPersonaRepository : IDefaultPersonaRepository
    {
        private readonly UserAssessmentDBContextContainer UATDb;

        public DefaultPersonaRepository()
        {
            this.UATDb = new UserAssessmentDBContextContainer();
        }

        public Task<List<CMSDefaultPersona>> GetAllDefaultPersonaAsync()
        {
            Logs.Info("DefaultPersonaRepository.GetAllDefaultPersonaAsync method has been called.");
            Task<List<CMSDefaultPersona>> obj = Task<List<CMSDefaultPersona>>.Run(() => UATDb.CMSDefaultPersonas.ToList<CMSDefaultPersona>());
           
            Logs.Info("The response of DefaultPersonaRepository.GetAllDefaultPersonaAsync method is, Response [List<CMSDefaultPersona>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<CMSDefaultPersona> GetDefaultPersonaByIdAsync(int id)
        {
            Logs.Info("DefaultPersonaRepository.GetDefaultPersonaByIdAsync method has been called having parameter[id: " + id + "]");
            Task<CMSDefaultPersona> obj = Task<CMSDefaultPersona>.Run(() => UATDb.CMSDefaultPersonas.FirstOrDefault(p => p.Id == id));
            
            Logs.Info("The response of DefaultPersonaRepository.GetDefaultPersonaByIdAsync method is, Response[CMSDefaultPersona, Id: " + ((obj == null) ? '-' : obj.Id) + "]");
            return obj;
        }
        public Task<CMSDefaultPersona> GetDefaultPersonaByProfileAnswerLabelAsync(string profileAnswerLabel)
        {
            Logs.Info("DefaultPersonaRepository.GetDefaultPersonaByProfileAnswerLabelAsync method has been called having parameter [profileAnswerLabel: " + profileAnswerLabel + "]");
            Task<CMSDefaultPersona> obj = Task<CMSDefaultPersona>.Run(() => UATDb.CMSDefaultPersonas.FirstOrDefault(p => p.profileAnswerLabel == profileAnswerLabel));
            
            Logs.Info("The response of DefaultPersonaRepository.GetDefaultPersonaByProfileAnswerLabelAsync method is, Response[CMSDefaultPersona, Id: " + ((obj == null) ? '-' : obj.Id) + "]");
            return obj;
        }
    }

    public interface IDefaultPersonaRepository
    {
        Task<List<CMSDefaultPersona>> GetAllDefaultPersonaAsync();
        Task<CMSDefaultPersona> GetDefaultPersonaByIdAsync(int id);
        Task<CMSDefaultPersona> GetDefaultPersonaByProfileAnswerLabelAsync(string profileAnswerLabel);
    }
}