﻿using hpi_user_assessment_tool.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Utility;


namespace hpi_user_assessment_tool.Data
{
    public class ValidationRepository : IValidationRepository
    {
        private readonly UserAssessmentDBContextContainer UATDb;

        public ValidationRepository()
        {
            this.UATDb = new UserAssessmentDBContextContainer();
        }

        public Task<List<CMSAnswer>> GetAllAnswersAsync()
        {
            Logs.Info("ValidationRepository.GetAllAnswersAsync method has been called");
            Task<List<CMSAnswer>> obj = Task<List<CMSAnswer>>.Run(() => UATDb.CMSAnswers.ToList<CMSAnswer>());
            Logs.Info("The response of ValidationRepository.GetAllAnswersAsync method is, Response [List<CMSAnswer>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<CMSAnswer>> GetAnswerByAnswersIdAsync(List<int> answerIds)
        {
            Logs.Info("ValidationRepository.GetAnswerByAnswersIdAsync method has been called having parameters [List<int> answerIds, Size: " + ((answerIds == null) ? '-' : answerIds.Count) + "]");
            Task<List<CMSAnswer>> obj = Task<List<CMSAnswer>>.Run(() => UATDb.CMSAnswers.Where(p => answerIds.Contains(p.Id)).ToList<CMSAnswer>());
            Logs.Info("The response of ValidationRepository.GetAnswerByAnswersIdAsync is, Response [List<CMSAnswer>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<CMSTag>> GetFilterTagsByAnswersIdAsync(List<int> answerIds)
        {
            Logs.Info("ValidationRepository.GetFilterTagsByAnswersIdAsync method has been called having parameters [List<int> answerIds, Size: " + ((answerIds == null) ? '-' : answerIds.Count) + "]");
            Task<List<CMSTag>> obj = Task<List<CMSTag>>.Run(() => UATDb.CMSTags.Where(p => answerIds.Contains(p.CMSAnswerId)).ToList<CMSTag>());
            Logs.Info("The response of ValidationRepository.GetFilterTagsByAnswersIdAsync is, Response [List<CMSTag>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<CMSWeightedTag>> GetWeightedTagsByAnswersIdAsync(List<int> answerIds)
        {
            Logs.Info("ValidationRepository.GetWeightedTagsByAnswersIdAsync method has been called having parameters [List<int> answerIds, Size: " + ((answerIds == null) ? '-' : answerIds.Count) + "]");
            Task<List<CMSWeightedTag>> obj = Task<List<CMSWeightedTag>>.Run(() => UATDb.CMSWeightedTags.Where(p => answerIds.Contains(p.CMSAnswerId)).ToList<CMSWeightedTag>());
            Logs.InfoFormat("The response of ValidationRepository.GetWeightedTagsByAnswersIdAsync is, Response [List<CMSWeightedTag>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }

        public Task<List<CMSTag>> GetFilterTagsByIdAsync(List<int> filterTagIds)
        {
            Logs.Info("ValidationRepository.GetFilterTagsByIdAsync method has been called having parameters [List<int> filterTagIds, Size: " + ((filterTagIds == null) ? '-' : filterTagIds.Count) + "]");
            Task<List<CMSTag>> obj = Task<List<CMSTag>>.Run(() => UATDb.CMSTags.Where(p => filterTagIds.Contains(p.Id)).ToList<CMSTag>());
            Logs.InfoFormat("The response of ValidationRepository.GetFilterTagsByIdAsync is, Response [List<CMSTag>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
        public Task<List<CMSWeightedTag>> GetWeightedTagsByIdAsync(List<int> weightedTagIds)
        {
            Logs.Info("ValidationRepository.GetWeightedTagsByIdAsync method has been called having parameters [List<int> weightedTagIds, Size: " + ((weightedTagIds == null) ? '-' : weightedTagIds.Count) + "]");
            Task<List<CMSWeightedTag>> obj = Task<List<CMSWeightedTag>>.Run(() => UATDb.CMSWeightedTags.Where(p => weightedTagIds.Contains(p.Id)).ToList<CMSWeightedTag>());
            Logs.Info("The response of ValidationRepository.GetWeightedTagsByIdAsync is, Response [List<CMSWeightedTag>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
    }

    public interface IValidationRepository
    {
        Task<List<CMSAnswer>> GetAllAnswersAsync();
        Task<List<CMSAnswer>> GetAnswerByAnswersIdAsync(List<int> answerIds);
        Task<List<CMSTag>> GetFilterTagsByAnswersIdAsync(List<int> answerIds);
        Task<List<CMSWeightedTag>> GetWeightedTagsByAnswersIdAsync(List<int> answerIds);
        Task<List<CMSTag>> GetFilterTagsByIdAsync(List<int> filterTagIds);
        Task<List<CMSWeightedTag>> GetWeightedTagsByIdAsync(List<int> weightedTagIds);
    }
}