﻿using hpi_user_assessment_tool.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Utility;


namespace hpi_user_assessment_tool.Data
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly UserAssessmentDBContextContainer UATDb;
        public QuestionRepository()
        {
            this.UATDb = new UserAssessmentDBContextContainer();
        }

        public Task<List<CMSQuestion>> GetAllQuestionAsync()
        {
            Logs.Info("QuestionRepository.GetAllQuestionAsync method has been called.");
            Task<List<CMSQuestion>> obj = Task<List<CMSQuestion>>.Run(() => UATDb.CMSQuestions.ToList<CMSQuestion>());

            Logs.Info("The response of QuestionRepository.GetAllQuestionAsync method is, Response [List<CMSQuestion> , Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }

        public Task<CMSQuestion> GetQuestionByIdAsync(int id)
        {
            Logs.Info("QuestionRepository.GetQuestionByIdAsync method has been called having parameter [id: " + id + "]");
            Task<CMSQuestion> obj = Task<CMSQuestion>.Run(() => UATDb.CMSQuestions.FirstOrDefault(p => p.Id == id));

            Logs.Info("The response of QuestionRepository.GetQuestionByIdAsync method is, Response [CMSQuestion , CMSQuestionId: " + ((obj == null) ? '-' : obj.Id) + "]");
            return obj;
        }

        public Task<Local> GetLocalAsync(string country, string language, string key)
        {
            Logs.Debug("QuestionRepository.GetLocalAsync method has been called having parameters [country=" + country + ", language=" + language + ", key=" + key + "]");
            Task<Local> obj = Task<Local>.Run(() => UATDb.Locals.Where(p => p.country == country && p.language == language && p.key == key)
              .AsEnumerable()
              .FirstOrDefault());

            Logs.Debug("The response of QuestionRepository.GetLocalAsync is, Response [local , localId: " + ((obj == null) ? '-' : obj.Id) + "]");
            return obj;
        }

        public Task<List<CMSConditionalQuestion>> GetAllConditionalQuestionAsync()
        {
            Logs.Info("QuestionRepository.GetAllConditionalQuestionAsync method has been called.");
            Task<List<CMSConditionalQuestion>> obj = Task<List<CMSConditionalQuestion>>.Run(() => UATDb.CMSConditionalQuestions.ToList<CMSConditionalQuestion>());
            Logs.Info("The response of QuestionRepository.GetAllConditionalQuestionAsync method is, Response [List<CMSConditionalQuestion> , Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");
            return obj;
        }
    }

    public interface IQuestionRepository
    {
        Task<List<CMSQuestion>> GetAllQuestionAsync();
        Task<CMSQuestion> GetQuestionByIdAsync(int id);
        Task<Local> GetLocalAsync(string country, string language, string key);
        Task<List<CMSConditionalQuestion>> GetAllConditionalQuestionAsync();
    }
}