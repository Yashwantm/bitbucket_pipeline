﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Utility;


namespace hpi_user_assessment_tool.Data
{
    public class CustomerProfileRepository : ICustomerProfileRepository
    {
        private UserAssessmentDBContextContainer UATDb;

        public CustomerProfileRepository()
        {
            UATDb = new UserAssessmentDBContextContainer();
        }

        public void CreateUserAsync(USRInfo usrInfo)
        {
            Logs.Info("CustomerProfileRepository.CreateUserAsync method has been called having parameter [USRInfo]");

            UATDb.USRInfoes.Add(usrInfo);
            UATDb.SaveChanges();
        }

        public void UpdateUserAsync(USRInfo usrInfo)
        {
            Logs.Info("CustomerProfileRepository.UpdateUserAsync method has been called having parameter [USRInfo]");
            USRInfo dbUSRInfo = UATDb.USRInfoes.FirstOrDefault(p => p.userId == usrInfo.userId);

            dbUSRInfo = usrInfo;
            UATDb.SaveChanges();
        }

        public Task<USRInfo> GetUserByUserIdAsync(string userId)
        {
            Logs.Info("CustomerProfileRepository.GetUserByIdAsync method has been called having parameter [userId: " + userId + "]");
            UATDb = new UserAssessmentDBContextContainer();
            Task<USRInfo> obj = Task<USRInfo>.Run(() => UATDb.USRInfoes.FirstOrDefault(p => p.userId == userId));

            Logs.Info("The response of CustomerProfileRepository.GetUserByIdAsync is, Response[USRInfo, Id: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Id) + "]");
            return obj;
        }

        public Task<USRCustomer> GetCustomerByIdAsync(int customerId)
        {
            Logs.Info("CustomerProfileRepository.GetCustomerByIdAsync method has been called having parameter [customerId: " + customerId + "]");
            Task<USRCustomer> obj = Task<USRCustomer>.Run(() =>
             UATDb.USRCustomers.FirstOrDefault(p => p.Id == customerId));

            Logs.Info("The response of CustomerProfileRepository.GetCustomerByIdAsync is, Response[ USRCustomer, customerId: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Id) + "]");
            return obj;
        }

        public bool IsProductSkuExist(string sku)
        {
            Logs.Info("CustomerProfileRepository.IsProductSkuExist method has been called having parameter [sku: " + sku + "]");
            bool isExist = false;
            ProductSku productSku = UATDb.ProductSkus.SingleOrDefault(p => p.sku == sku);
            if (productSku != null && !string.IsNullOrEmpty(productSku.obsolete_date) && productSku.obsolete_date != "NULL" && DateTime.Parse(productSku.obsolete_date) >= DateTime.Now.Date)
            {
                isExist = true;
            }
            Logs.Info("The response of CustomerProfileRepository.IsProductSkuExist is, Response[isExist: " + isExist + "]");
            return isExist;
        }

        public bool IsAccessorySkuExist(string sku, string category)
        {
            Logs.Info("CustomerProfileRepository.IsAccessorySkuExist method has been called having parameters [sku: " + sku + ", category: " + category + "]");
            bool isExist = false;
            ProductAccessory productAccessory = UATDb.ProductAccessories.SingleOrDefault(p => p.sku == sku);
            if (productAccessory != null)
            {
                if (category.Equals("service"))
                {
                    isExist = true;
                }
                else if (!string.IsNullOrEmpty(productAccessory.obsolete_date) && productAccessory.obsolete_date != "NULL" && DateTime.Parse(productAccessory.obsolete_date) >= DateTime.Now.Date)
                {
                    isExist = true;
                }
            }
            Logs.Info("The response of CustomerProfileRepository.IsAccessorySkuExist is, Response[isExist: " + isExist + "]");
            return isExist;
        }

        public bool IsDaasServiceSkuExist(string sku)
        {
            Logs.Info("CustomerProfileRepository.IsDaasServiceSkuExist method has been called having parameters [sku: " + sku + "]");
            bool isExist = false;
            ProductDaasService productDaasService = UATDb.ProductDaasServices.SingleOrDefault(p => p.sku == sku);
            if (productDaasService != null)
            {
                isExist = true;
            }
            Logs.Info("The response of CustomerProfileRepository.IsDaasServiceSkuExist is, Response[isExist: " + isExist + "]");
            return isExist;
        }

        public Task<List<CMSAllowedUser>> GetAllAllowedUsersAsync()
        {
            Logs.Info("CustomerProfileRepository.GetAllAllowedUsersAsync method has been called ");
            Task<List<CMSAllowedUser>> obj = Task<List<CMSAllowedUser>>.Run(() => UATDb.CMSAllowedUsers.ToList());

            Logs.Info("The response of CustomerProfileRepository.GetAllAllowedUsersAsync is, Response[ List<CMSAllowedUser>, Size: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Count) + "]");

            return obj;
        }

        public Task<CMSRestrictedAccess> GetRestrictedAccessAsync()
        {
            Logs.Info("CustomerProfileRepository.GetRestrictedAccessAsync method has been called ");
            Task<CMSRestrictedAccess> obj = Task<CMSRestrictedAccess>.Run(() => UATDb.CMSRestrictedAccesses.FirstOrDefault());

            Logs.Info("The response of CustomerProfileRepository.GetRestrictedAccessAsync is, Response[ CMSRestrictedAccess, Id: " + ((obj == null || obj.Result == null) ? '-' : obj.Result.Id) + "]");
            return obj;
        }

        public void SaveChanges()
        {
            UATDb.SaveChanges();
        }

        public void AddNewUser(USRInfo usrInfo)
        {
            UATDb.USRInfoes.Add(usrInfo);
        }

        public UserAssessmentDBContextContainer GetContext()
        {
            Logs.Info("CustomerProfileRepository.GetContext method has been called ");
            return UATDb;
        }

        public bool DeleteCustomerByIdAsync(USRCustomer customer)
        {
            bool isDeleted = false;
            try
            {
                Logs.Info("CustomerProfileRepository.DeleteCustomerByIdAsync method has been called having parameter [customer{customerName: " + ((customer == null) ? "-" : customer.name) + "}]");
                UATDb.USRCustomers.Remove(customer);
                SaveChanges();
                isDeleted = true;
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            Logs.InfoFormat("The response of CustomerProfileRepository.DeleteCustomerByIdAsync is, Response[isDeleted: " + isDeleted + "]");
            return isDeleted;
        }

        public bool DeleteProfileByIdAsync(USRProfile profile)
        {
            bool isDeleted = false;
            try
            {
                Logs.Info("CustomerProfileRepository.DeleteProfileByIdAsync method has been called having parameter [profile {profileName: " + ((profile == null) ? "-" : profile.name) + "}]");
                UATDb.USRSelectedProducts.Remove(UATDb.USRSelectedProducts.FirstOrDefault(p => p.Id == profile.USRSelectedProduct.Id));
                UATDb.USRProfiles.Remove(UATDb.USRProfiles.FirstOrDefault(p => p.Id == profile.Id));
                UATDb.SaveChanges();
                isDeleted = true;
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            Logs.InfoFormat("The response of CustomerProfileRepository.DeleteProfileByIdAsync is, Response[isDeleted: " + isDeleted + "]");
            return isDeleted;
        }

        public Task<List<USRInfoCustomerMapping>> GetUSRInfoCustomerMappingsByUserInfoPKAsync(int userInfoPK)
        {
            Task<List<USRInfoCustomerMapping>> obj = Task<List<USRInfoCustomerMapping>>.Run(() => UATDb.USRInfoCustomerMappings.Where(p => p.usrInfoPK == userInfoPK).ToList());
            return obj;
        }
        public Task<List<USRCustomer>> GetCustomerByIdsAsync(List<int> customerIdList)
        {
            Task<List<USRCustomer>> obj = Task<List<USRCustomer>>.Run(() => UATDb.USRCustomers.Where(p => customerIdList.Contains(p.Id)).ToList());
            return obj;
        }
        public bool DeleteUSRInfoCustomerMappingAsync(int usrInfoPK, int usrCustomerPK)
        {
            bool isDeleted = false;
            try
            {
                UATDb.USRInfoCustomerMappings.Remove(UATDb.USRInfoCustomerMappings.SingleOrDefault(p => p.usrInfoPK == usrInfoPK && p.usrCustomerPK == usrCustomerPK));
                SaveChanges();
                isDeleted = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return isDeleted;
        }
    }

    public interface ICustomerProfileRepository
    {
        Task<USRInfo> GetUserByUserIdAsync(string userId);

        Task<USRCustomer> GetCustomerByIdAsync(int customerId);

        void SaveChanges();

        void AddNewUser(USRInfo usrInfo);

        UserAssessmentDBContextContainer GetContext();
        bool IsProductSkuExist(string sku);
        bool IsAccessorySkuExist(string sku, string category);
        bool IsDaasServiceSkuExist(string sku);
        Task<List<CMSAllowedUser>> GetAllAllowedUsersAsync();
        Task<CMSRestrictedAccess> GetRestrictedAccessAsync();
        bool DeleteCustomerByIdAsync(USRCustomer customer);
        bool DeleteProfileByIdAsync(USRProfile profile);
        Task<List<USRCustomer>> GetCustomerByIdsAsync(List<int> customerIdList);
        Task<List<USRInfoCustomerMapping>> GetUSRInfoCustomerMappingsByUserInfoPKAsync(int userInfoPK);
        bool DeleteUSRInfoCustomerMappingAsync(int usrInfoPK, int usrCustomerPK);
    }
}
