﻿using System.Web;
using System.Web.Mvc;

namespace hpi_user_assessment_tool
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
