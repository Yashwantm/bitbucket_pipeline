﻿using Autofac;
using hpi_user_assessment_tool.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Autofac.Integration.WebApi;
using hpi_user_assessment_tool.Data;

namespace hpi_user_assessment_tool.App_Start
{
    public static class Bootstrapper
    {
        public static void Configure()
        {
            ConfigureWebApiContainer();
        }

        public static void ConfigureWebApiContainer()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            
            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);

            // Register Dependencies
            builder.RegisterType<DefaultPersonaService>().As<IDefaultPersonaService>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<DefaultPersonaRepository>().As<IDefaultPersonaRepository>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<QuestionService>().As<IQuestionService>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<QuestionRepository>().As<IQuestionRepository>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<CustomerProfileService>().As<ICustomerProfileService>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<CustomerProfileRepository>().As<ICustomerProfileRepository>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<RecommendationService>().As<IRecommendationService>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<RecommendationRepository>().As<IRecommendationRepository>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<ValidationService>().As<IValidationService>().AsImplementedInterfaces().InstancePerApiRequest();
            builder.RegisterType<ValidationRepository>().As<IValidationRepository>().AsImplementedInterfaces().InstancePerApiRequest();
            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}