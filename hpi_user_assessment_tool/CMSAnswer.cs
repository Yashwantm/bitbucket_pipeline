//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hpi_user_assessment_tool
{
    using System;
    using System.Collections.Generic;
    
    public partial class CMSAnswer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CMSAnswer()
        {
            this.CMSAnswerInsights = new HashSet<CMSAnswerInsight>();
            this.CMSTags = new HashSet<CMSTag>();
            this.CMSWeightedTags = new HashSet<CMSWeightedTag>();
            this.images = new HashSet<image>();
        }
    
        public int Id { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public string order { get; set; }
        public Nullable<bool> enabled { get; set; }
        public string localKey { get; set; }
        public int CMSQuestionId { get; set; }
    
        public virtual CMSQuestion CMSQuestion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CMSAnswerInsight> CMSAnswerInsights { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CMSTag> CMSTags { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CMSWeightedTag> CMSWeightedTags { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<image> images { get; set; }
    }
}
