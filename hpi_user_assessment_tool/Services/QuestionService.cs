﻿using hpi_user_assessment_tool.Data;
using hpi_user_assessment_tool.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;
using System.Web;
using hpi_user_assessment_tool.Utility;
using hpi_user_assessment_tool.Models.Response;

namespace hpi_user_assessment_tool.Services
{
    public class QuestionService : IQuestionService
    {
        private IQuestionRepository questionRepository;
        // TODO: Need to get the local information from the request
        string country = "us";
        string language = "en";
        public QuestionService(IQuestionRepository questionRepository)
        {
            this.questionRepository = questionRepository;
        }

        public GetAllQuestionsResponse GetAllQuestionAsync()
        {
            GetAllQuestionsResponse getAllQuestionsResponse = new GetAllQuestionsResponse();
            List<CMSQuestion> cmsQuestions = questionRepository.GetAllQuestionAsync().Result;
            ResourceManager rm = new ResourceManager("hpi_user_assessment_tool.UAT_resources",
                Assembly.GetExecutingAssembly());
            String ImageServerURL = rm.GetString("ImageServerURL", CultureInfo.CurrentCulture);
            List<CMSConditionalQuestion> cmsConditionalQuestionList = new List<CMSConditionalQuestion>();
            try
            {
                Logs.Info("QuestionService.GetAllQuestionAsync method has been called.");
                foreach (CMSQuestion cmsQuestionquestion in cmsQuestions)
                {
                    if (cmsQuestionquestion != null)
                    {
                        Local local = questionRepository.GetLocalAsync(country, language, cmsQuestionquestion.localKey).Result;
                        cmsQuestionquestion.localKey = local == null ? "" : local.value;

                        if (cmsQuestionquestion.CMSQuestionInsights != null)
                        {
                            foreach (CMSQuestionInsight cmsQuestionInsight in cmsQuestionquestion.CMSQuestionInsights)
                            {
                                Local localTemp = questionRepository.GetLocalAsync(country, language, cmsQuestionInsight.localKey).Result;
                                cmsQuestionInsight.localKey = localTemp == null ? "" : localTemp.value;
                            }
                        }

                        if (cmsQuestionquestion.CMSAnswers != null)
                        {
                            foreach (CMSAnswer cmsAnswer in cmsQuestionquestion.CMSAnswers)
                            {
                                Local localTemp = questionRepository.GetLocalAsync(country, language, cmsAnswer.localKey).Result;

                                cmsAnswer.localKey = localTemp == null ? "" : localTemp.value;
                                if (cmsAnswer.CMSAnswerInsights != null)
                                {
                                    foreach (CMSAnswerInsight cmsAnswerInsight in cmsAnswer.CMSAnswerInsights)
                                    {
                                        Local localTemp1 = questionRepository.GetLocalAsync(country, language, cmsAnswerInsight.localKey).Result;
                                        cmsAnswerInsight.localKey = localTemp1 == null ? "" : localTemp1.value;
                                    }
                                }

                                if (cmsAnswer.images != null)
                                {
                                    foreach (image objImage in cmsAnswer.images)
                                    {
                                        objImage.url = ImageServerURL + objImage.url;
                                    }
                                }

                                cmsAnswer.CMSTags.Clear();
                                cmsAnswer.CMSWeightedTags.Clear();
                            }
                        }

                    }
                }

                cmsConditionalQuestionList = questionRepository.GetAllConditionalQuestionAsync().Result;
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            Logs.Info("The response of QuestionService.GetAllQuestionAsync method is, Response [List<CMSQuestion>, Size: " + ((cmsQuestions == null) ? '-' : cmsQuestions.Count) + "] AND [List<CMSConditionalQuestion>, Size: " + ((cmsConditionalQuestionList == null) ? '-' : cmsConditionalQuestionList.Count) + "]");

            getAllQuestionsResponse.questions = cmsQuestions;
            getAllQuestionsResponse.conditionalQuestions = cmsConditionalQuestionList;

            return getAllQuestionsResponse;
        }

        public async Task<CMSQuestion> GetQuestionByIdAsync(int id)
        {
            Logs.Info("QuestionService.GetQuestionByIdAsync method has been called having parameter [id: " + id + "]");
            ResourceManager rm = new ResourceManager("hpi_user_assessment_tool.UAT_resources",
            Assembly.GetExecutingAssembly());
            String ImageServerURL = rm.GetString("ImageServerURL", CultureInfo.CurrentCulture);

            CMSQuestion cmsQuestionquestion = await questionRepository.GetQuestionByIdAsync(id);
            if (cmsQuestionquestion != null)
            {
                Local local = await questionRepository.GetLocalAsync(country, language, cmsQuestionquestion.localKey);
                cmsQuestionquestion.localKey = local == null ? "" : local.value;

                if (cmsQuestionquestion.CMSQuestionInsights != null)
                {
                    foreach (CMSQuestionInsight cmsQuestionInsight in cmsQuestionquestion.CMSQuestionInsights)
                    {
                        Local localTemp = await questionRepository.GetLocalAsync(country, language, cmsQuestionInsight.localKey);
                        cmsQuestionInsight.localKey = localTemp == null ? "" : localTemp.value;
                    }
                }

                if (cmsQuestionquestion.CMSAnswers != null)
                {
                    foreach (CMSAnswer cmsAnswer in cmsQuestionquestion.CMSAnswers)
                    {
                        Local localTemp = await questionRepository.GetLocalAsync(country, language, cmsAnswer.localKey);
                        cmsAnswer.localKey = localTemp == null ? "" : localTemp.value;
                        if (cmsAnswer.CMSAnswerInsights != null)
                        {
                            foreach (CMSAnswerInsight cmsAnswerInsight in cmsAnswer.CMSAnswerInsights)
                            {
                                Local localTemp1 = await questionRepository.GetLocalAsync(country, language, cmsAnswerInsight.localKey);
                                cmsAnswerInsight.localKey = localTemp1 == null ? "" : localTemp1.value;
                            }
                        }

                        if (cmsAnswer.images != null)
                        {
                            foreach (image objImage in cmsAnswer.images)
                            {
                                objImage.url = ImageServerURL + objImage.url;
                            }
                        }

                        cmsAnswer.CMSTags.Clear();
                        cmsAnswer.CMSWeightedTags.Clear();
                    }
                }

            }

            Logs.Info("The response of QuestionService.GetQuestionByIdAsync method is, Response [CMSQuestion, CMSQuestionId: " + ((cmsQuestionquestion == null) ? '-' : cmsQuestionquestion.Id) + "]");
            return cmsQuestionquestion;
        }
    }

    public interface IQuestionService
    {
        GetAllQuestionsResponse GetAllQuestionAsync();
        Task<CMSQuestion> GetQuestionByIdAsync(int id);
    }
}