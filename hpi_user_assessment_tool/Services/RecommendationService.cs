﻿using hpi_user_assessment_tool.Data;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Models.Process;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Resources;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using hpi_user_assessment_tool.Utility;

namespace hpi_user_assessment_tool.Services
{
    public class RecommendationService : IRecommendationService
    {
        private IRecommendationRepository recommendationRepository;

        public RecommendationService(IRecommendationRepository recommendationRepository)
        {
            this.recommendationRepository = recommendationRepository;
        }

        /// <summary>
        /// GetRecommendedSkusAsync will return the list of recommended sku's after applying the tags/rules on it
        /// </summary>
        /// <param name="recommendation"></param>
        /// <returns  RecommendationProductSkuDTOResponse></returns>
        public RecommendationProductSkuDTOResponse GetRecommendedSkusAsync(Recommendation recommendation)
        {
            //Logs.Info("RecommendationService.GetRecommendedSkusAsync method has been called having parameter [Recommendation]");
            List<CMSWeightedTag> weightedTagList = new List<CMSWeightedTag>();
            List<CMSAnswer> allDBRequestedCMSAnswers = new List<CMSAnswer>();
            List<CMSAnswer> nonFilterDBCMSAnswers = new List<CMSAnswer>();
            List<CMSAnswer> filterDBCMSAnswers = new List<CMSAnswer>();

            if (recommendation.answerIds != null && recommendation.answerIds.Count > 0)
            {
                allDBRequestedCMSAnswers = recommendationRepository.GetAnswerByIdsAsync(recommendation.answerIds).Result;

                List<CMSAnswer> allFilterDBCMSAnswers = recommendationRepository.GetAnswersByQuestionAssessmentStepAsync(Constants.FILTER_DEVICE_ASSESSMENT_LABEL);
                if (allFilterDBCMSAnswers != null && allFilterDBCMSAnswers.Count > 0)
                {
                    // Filter out the device filter answers
                    List<int> nonFilterAnswerIds = recommendation.answerIds.Except(allFilterDBCMSAnswers.Select(p => p.Id).ToList()).ToList();
                    nonFilterDBCMSAnswers = allDBRequestedCMSAnswers.Where(p => nonFilterAnswerIds.Contains(p.Id)).ToList();
                    filterDBCMSAnswers = allDBRequestedCMSAnswers.Except(nonFilterDBCMSAnswers).ToList();
                }

                if (nonFilterDBCMSAnswers.Count == 0)
                {
                    nonFilterDBCMSAnswers = allDBRequestedCMSAnswers;
                }
            }

            // Getting Standalone Tags
            includeStandaloneTags(nonFilterDBCMSAnswers, weightedTagList, Constants.ENTITY_TYPE_SKU, "");


            List<ProductSku> dbProductSkuList = new List<ProductSku>();
            List<WeightCalculation> orderedAVGWeightList = new List<WeightCalculation>();

            if (filterDBCMSAnswers.Count > 0)
            {
                string filterQuery = buildFilterQuery(filterDBCMSAnswers);
                // Getting recommendations with filter answers
                dbProductSkuList = GetRecommendedSku(nonFilterDBCMSAnswers, weightedTagList, recommendation, filterQuery);
            }
            else
            {
                // Getting recommendations without filter answers
                dbProductSkuList = GetRecommendedSku(nonFilterDBCMSAnswers, weightedTagList, recommendation, "");
            }

            // filtering out the obsolate dated skus
            dbProductSkuList = dbProductSkuList.Where(p => !string.IsNullOrEmpty(p.obsolete_date) && p.obsolete_date != "NULL" && DateTime.Parse(p.obsolete_date) >= DateTime.Now.Date).ToList();
            
            // Calculating the Weight for each Sku's
            orderedAVGWeightList = PopulateWeightMap(weightedTagList, dbProductSkuList, Constants.ENTITY_TYPE_SKU, recommendation);

            // Ordering orderedAVGWeightList/sortedAVGWeightList in 'order' assending order
            List<WeightCalculation> sortedAVGWeightList = new List<WeightCalculation>();
            sortedAVGWeightList = orderedAVGWeightList.Where(p => p.order > 0).OrderBy(p => p.order).ToList();
            sortedAVGWeightList.AddRange(orderedAVGWeightList.Where(p => p.order <= 0).ToList());

            orderedAVGWeightList = sortedAVGWeightList;

            List<string> productPmoidsList = dbProductSkuList.Select(p => p.pmoid).ToList<string>();

            //Getting the Product Features list for all filtered products
            List<ProductFeature> productsFeatureList = recommendationRepository.GetProductFeaturesByPmoidsAsync(productPmoidsList, recommendation.country, recommendation.language).Result;

            // Adding Product Features
            List<ProductSkuDTO> productSkuDTOList = new List<ProductSkuDTO>();
            foreach (ProductSku productSku in dbProductSkuList)
            {
                ProductSkuDTO productSkuDTO = new ProductSkuDTO(recommendation.debug);
                CopyProperties.Copy(productSku, productSkuDTO);
                WeightCalculation weightCalculation = orderedAVGWeightList.FirstOrDefault(p => p.sku == productSku.sku);
                if (weightCalculation != null)
                {                    
                    productSkuDTO.order = weightCalculation.order;
                    productSkuDTO.averageWeight = weightCalculation.averageWeight;
                    productSkuDTO.totalWeight = weightCalculation.totalWeight;
                    productSkuDTO.totalNumberofWeightedTag = weightCalculation.totalNumberOfWeightedTags;
                    productSkuDTO.matchedWeightedTags = weightCalculation.matchedCMSWeightedTagList;
                    productSkuDTO.matchedWeightedTagCount = weightCalculation.matchedCMSWeightedTagList.Count;
                    productSkuDTO.unmatchedWeightedTags = weightCalculation.unmatchedCMSWeightedTagList;
                }

                //Getting the Product Features list for particular product
                List<ProductFeature> productFeatureList = productsFeatureList.Where(p => p.pmoid == productSkuDTO.pmoid).ToList<ProductFeature>();

                if (productFeatureList != null)
                {
                    List<ProductFeatureDTO> productFeatureDTOList = new List<ProductFeatureDTO>();
                    List<ProductFeatureDTO> productFootNoteDTOList = new List<ProductFeatureDTO>();
                    List<string> productFeaturePriorityList = new List<string> { "ksp_01_suppt_01_short", "ksp_02_suppt_01_short", "ksp_03_suppt_01_short", "ksp_04_suppt_01_short", "ksp_01_headline_medium", "ksp_02_headline_medium", "ksp_03_headline_medium", "ksp_04_headline_medium" };
                    List<ProductFeature> productKSPList = productFeatureList.Where(p => p.elementName.StartsWith("ksp")).ToList();
                    List<ProductFeature> productFootNoteList = productFeatureList.Where(p => p.elementName.StartsWith("msgfootnote")).ToList();
                    List<string> matchedNumbers = new List<string>();

                    foreach (string featurePriority in productFeaturePriorityList)
                    {
                        if (productFeatureDTOList.Count == 3)
                            break;

                        foreach (ProductFeature productFeature in productKSPList)
                        {
                            if (featurePriority.Equals(productFeature.elementName))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                CopyProperties.Copy(productFeature, productFeatureDTO);
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeature.elementValue != null)
                                {
                                    List<string> matches =
                                    Regex.Matches(productFeature.elementValue.Replace(Environment.NewLine, ""), @"\[([^]]*)\]")
                                        .Cast<Match>()
                                        .Select(x => x.Groups[1].Value)
                                        .ToList();

                                    matches = matches.Distinct().ToList();

                                    foreach (string footNoteMatchedNumber in matches)
                                    {
                                        if (matchedNumbers.Contains(footNoteMatchedNumber))
                                            continue;
                                        ProductFeature productFootNote = productFootNoteList.FirstOrDefault(p => p.elementValue.StartsWith("[" + footNoteMatchedNumber + "]"));
                                        if (productFootNote != null)
                                        {
                                            ProductFeatureDTO productFeatureDTOFootNote = new ProductFeatureDTO();
                                            CopyProperties.Copy(productFootNote, productFeatureDTOFootNote);
                                            productFootNoteDTOList.Add(productFeatureDTOFootNote);
                                        }
                                        matchedNumbers.Add(footNoteMatchedNumber);
                                    }
                                }
                            }
                        }
                    }

                    productSkuDTO.productFeatureList = productFeatureDTOList;
                    productSkuDTO.productFootNoteList = productFootNoteDTOList;
                }
                productSkuDTOList.Add(productSkuDTO);
            }

            // Ordering productSkuDTOList/ sortedProductSkuDTOList in 'order' assending order
            List<ProductSkuDTO> sortedProductSkuDTOList = new List<ProductSkuDTO>();
            List<ProductSku> sortedProductSkuList = new List<ProductSku>();
            
            sortedProductSkuDTOList = productSkuDTOList.Where(p => p.order > 0).OrderBy(p => p.order).ToList();
            sortedProductSkuDTOList.AddRange(productSkuDTOList.Where(p => p.order <= 0).ToList());

            int order = 1;
            foreach (ProductSkuDTO productSkuDTO in sortedProductSkuDTOList)
            {
                productSkuDTO.order = order;
                order++;
            }

            sortedProductSkuDTOList = sortAllProductSkuByDistinctSeries(sortedProductSkuDTOList);

            RecommendationProductSkuDTOResponse response = new RecommendationProductSkuDTOResponse();
            response.totalCount = sortedProductSkuDTOList.Count;
            sortedProductSkuList = getRequiredRangeProductList(sortedProductSkuList, recommendation);
            sortedProductSkuDTOList = getRequiredRangeProductList(sortedProductSkuDTOList, recommendation);

            foreach(var sortedProductSkuDTO in sortedProductSkuDTOList)
            {
                foreach(var dbProductSku in dbProductSkuList)
                {
                    if(dbProductSku.sku == sortedProductSkuDTO.sku)
                    {
                        sortedProductSkuList.Add(dbProductSku);
                    }
                }
            }
            //Logs.InfoFormat("The response of RecommendationService.GetRecommendedSkusAsync method is, Response[List<ProductSKuDTO>, Size: " + ((sortedProductSkuDTOList == null) ? '-' : sortedProductSkuDTOList.Count) + "]");
            response.recommendations = sortedProductSkuDTOList;

            bool isMatched = false;
            bool isApplicable = false;
            string value = "";
            List<CMSAnswer> notApplicableFilterOptions = new List<CMSAnswer>();
            if (filterDBCMSAnswers != null)
            {
                foreach (var filter in filterDBCMSAnswers)
                {
                    foreach (var cmsTag in filter.CMSTags)
                    {
                        foreach (var cmsTagRule in cmsTag.CMSTagRules)
                        {
                            foreach (var product in sortedProductSkuList)
                            {
                                value = product.GetType().GetProperty(cmsTagRule.appliedOnColumn).GetValue(product, null).ToString();
                                
                                // Process Tags for this entity table
                                string criteria = cmsTagRule.criteria.Replace("'", "");
                                if (!string.IsNullOrEmpty(value))
                                {
                                    value = value.Replace("'", "");
                                }


                                if (cmsTagRule.condition.Equals("contains"))
                                {
                                    string[] criteriaArr = criteria.Split(',');
                                    foreach (string containsValue in criteriaArr)
                                    {
                                        if (!string.IsNullOrEmpty(value) && value.Contains(containsValue))
                                        {
                                            isMatched = true;
                                        }
                                    }
                                    if (!isMatched)
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("notContains"))
                                {
                                    string[] criteriaArr = criteria.Split(',');
                                    foreach (string containsValue in criteriaArr)
                                    {
                                        if (!string.IsNullOrEmpty(value) && !value.Contains(containsValue))
                                        {
                                            isMatched = true;
                                        }
                                    }
                                    if (!isMatched)
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("equals"))
                                {

                                    if (criteria.Equals(value))
                                    {
                                        isMatched = true;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("notEquals"))
                                {

                                    if (!criteria.Equals(value))
                                    {
                                        isMatched = true;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("minMaxRange"))
                                {
                                    string[] criteriaList = criteria.Split(',');

                                    if (string.IsNullOrEmpty(value))
                                    {
                                        value = "0";
                                    }

                                    if (float.Parse(value) >= float.Parse(criteriaList[0]) && float.Parse(value) <= float.Parse(criteriaList[1]))
                                    {
                                        isMatched = true;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("greaterThan"))
                                {
                                    if (string.IsNullOrEmpty(value))
                                    {
                                        value = "0";
                                    }

                                    if ((float.Parse(value) > float.Parse(criteria)))
                                    {
                                        isMatched = true;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("lessThan"))
                                {
                                    if (string.IsNullOrEmpty(value))
                                    {
                                        value = "0";
                                    }

                                    if ((float.Parse(value) < float.Parse(criteria)))
                                    {
                                        isMatched = true;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                                else if (cmsTagRule.condition.Equals("daysBetween") && !string.IsNullOrEmpty(value) && !value.Equals("NULL"))
                                {
                                    string[] criteriaList = criteria.Split(',');

                                    DateTime valueDateTime = Convert.ToDateTime(value);
                                    DateTime todayDateTime = DateTime.Now;
                                    double numberOfDays = (todayDateTime - valueDateTime).TotalDays;

                                    if (numberOfDays >= double.Parse(criteriaList[0]) && numberOfDays <= double.Parse(criteriaList[1]))
                                    {
                                        isMatched = true;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        break;
                                    }
                                }
                            }                                                
                        }
                        if (!isMatched)
                        {
                            isApplicable = false;
                        }
                        else
                        {
                            isApplicable = true;
                            break;
                        }
                    }
                    if (isApplicable == false)
                    {
                        notApplicableFilterOptions.Add(filter);
                    }                 
                }
            }
            response.notApplicableFilterOption = notApplicableFilterOptions;  
            return response;
        }

        /// <summary>
        /// GetRecommendedAccessoriesAsync will return the list of recommended accessories after applying the tags/rules on it
        /// </summary>
        /// <param name="recommendation">request object</param>
        /// <param name="accessoryType">docks/displays/services/accessories</param>
        /// <returns RecommendationProductAccessoryDTOResponse></returns>
        public RecommendationProductAccessoryDTOResponse GetRecommendedAccessoriesAsync(Recommendation recommendation, string accessoryType)
        {
            //Logs.InfoFormat("RecommendationService.GetRecommendedAccessoriesAsync method has been called having parameters [Recommendation, accessoryType: " + accessoryType + "]");
            List<CMSWeightedTag> weightedTagList = new List<CMSWeightedTag>();
            List<CMSAnswer> allDBRequestedCMSAnswers = new List<CMSAnswer>();
            List<CMSAnswer> nonFilterDBCMSAnswers = new List<CMSAnswer>();
            List<CMSAnswer> filterDBCMSAnswers = new List<CMSAnswer>();
            string productAccessoriesAlias = "pa";

            if (recommendation.answerIds != null && recommendation.answerIds.Count > 0)
            {
                allDBRequestedCMSAnswers = recommendationRepository.GetAnswerByIdsAsync(recommendation.answerIds).Result;

                string questionAssessmentStep = "";
                if (Constants.ACCESSORY_TYPE_ACCESSORY.Equals(accessoryType))
                {
                    questionAssessmentStep = Constants.FILTER_ACCESSORY_ASSESSMENT_LABEL;
                }
                else if (Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType))
                {
                    questionAssessmentStep = Constants.FILTER_DISPLAY_ASSESSMENT_LABEL;
                }
                else if (Constants.ACCESSORY_TYPE_DOCK.Equals(accessoryType))
                {
                    questionAssessmentStep = Constants.FILTER_DOCK_ASSESSMENT_LABEL;
                }
                else if (Constants.ACCESSORY_TYPE_SERVICE.Equals(accessoryType))
                {
                    questionAssessmentStep = Constants.FILTER_SERVICE_ASSESSMENT_LABEL;
                }

                List<CMSAnswer> allFilterDBCMSAnswers = recommendationRepository.GetAnswersByQuestionAssessmentStepAsync(questionAssessmentStep);
                if (allFilterDBCMSAnswers != null && allFilterDBCMSAnswers.Count > 0)
                {
                    // Filter out the accessories filter answers
                    List<int> nonFilterAnswerIds = recommendation.answerIds.Except(allFilterDBCMSAnswers.Select(p => p.Id).ToList()).ToList();
                    nonFilterDBCMSAnswers = allDBRequestedCMSAnswers.Where(p => nonFilterAnswerIds.Contains(p.Id)).ToList();
                    filterDBCMSAnswers = allDBRequestedCMSAnswers.Except(nonFilterDBCMSAnswers).ToList();
                }

                if (nonFilterDBCMSAnswers.Count == 0)
                {
                    nonFilterDBCMSAnswers = allDBRequestedCMSAnswers;
                }
            }

            // Getting Standalone Tags
            includeStandaloneTags(nonFilterDBCMSAnswers, weightedTagList, Constants.ENTITY_TYPE_ACCESSORY, accessoryType);

            // Getting Reliant Standalone Tags
            includeReliantStandaloneTags(recommendation, nonFilterDBCMSAnswers, weightedTagList, Constants.ENTITY_TYPE_ACCESSORY, accessoryType);

            List<ProductAccessory> dbProductAccessoryList = new List<ProductAccessory>();
            List<WeightCalculation> orderedAVGWeightList = new List<WeightCalculation>();

            // Apply Accessories Filters on the recommended Skus
            if (filterDBCMSAnswers.Count > 0)
            {
                string filterQuery = buildFilterQueryForAccessories(filterDBCMSAnswers, productAccessoriesAlias);
                dbProductAccessoryList = GetRecommendedAccessories(nonFilterDBCMSAnswers, weightedTagList, recommendation, filterQuery, accessoryType, productAccessoriesAlias);
            }
            else
            {
                dbProductAccessoryList = GetRecommendedAccessories(nonFilterDBCMSAnswers, weightedTagList, recommendation, "", accessoryType, productAccessoriesAlias);
            }

            // Calculating the Weight for each Accessories
            orderedAVGWeightList = PopulateWeightMap(weightedTagList, dbProductAccessoryList, accessoryType, recommendation);

            // Ordering orderedAVGWeightList/sortedAVGWeightList in 'order' assending order
            List<WeightCalculation> sortedAVGWeightList = new List<WeightCalculation>();
            sortedAVGWeightList = orderedAVGWeightList.Where(p => p.order > 0).OrderBy(p => p.order).ToList();
            sortedAVGWeightList.AddRange(orderedAVGWeightList.Where(p => p.order <= 0).ToList());

            orderedAVGWeightList = sortedAVGWeightList;


            List<string> productPmoidsList = dbProductAccessoryList.Select(p => p.pmoid).ToList<string>();

            //Getting the Product Features list for all filtered products
            List<ProductFeature> productsFeatureList = recommendationRepository.GetProductFeaturesByPmoidsAsync(productPmoidsList, recommendation.country, recommendation.language).Result;

            List<ProductAccessoryDTO> productAccessoryDTOList = new List<ProductAccessoryDTO>();
            foreach (ProductAccessory productAccessory in dbProductAccessoryList)
            {
                ProductAccessoryDTO productAccessoryDTO = new ProductAccessoryDTO(recommendation.debug, accessoryType);
                CopyProperties.Copy(productAccessory, productAccessoryDTO);
                WeightCalculation weightCalculation = orderedAVGWeightList.FirstOrDefault(p => p.sku == productAccessory.sku);
                if (weightCalculation != null)
                {
                    productAccessoryDTO.order = weightCalculation.order;
                    productAccessoryDTO.averageWeight = weightCalculation.averageWeight;
                    productAccessoryDTO.totalWeight = weightCalculation.totalWeight;
                    productAccessoryDTO.totalNumberofWeightedTag = weightCalculation.totalNumberOfWeightedTags;
                    productAccessoryDTO.matchedWeightedTags = weightCalculation.matchedCMSWeightedTagList;
                    productAccessoryDTO.unmatchedWeightedTags = weightCalculation.unmatchedCMSWeightedTagList;
                }

                //Getting the Product Features list for particular product
                List<ProductFeature> productFeatureList = productsFeatureList.Where(p => p.pmoid == productAccessoryDTO.pmoid).ToList<ProductFeature>();


                if (productFeatureList != null)
                {
                    List<ProductFeatureDTO> productFeatureDTOList = new List<ProductFeatureDTO>();
                    List<ProductFeatureDTO> productFootNoteDTOList = new List<ProductFeatureDTO>();
                    List<string> productFeaturePriorityList = new List<string> { "ksp_01_suppt_01_short", "ksp_02_suppt_01_short", "ksp_03_suppt_01_short", "ksp_04_suppt_01_short", "ksp_01_headline_medium", "ksp_02_headline_medium", "ksp_03_headline_medium", "ksp_04_headline_medium" };
                    List<ProductFeature> productKSPList = productFeatureList.Where(p => p.elementName.StartsWith("ksp")).ToList();
                    List<ProductFeature> productFootNoteList = productFeatureList.Where(p => p.elementName.StartsWith("msgfootnote")).ToList();
                    List<string> matchedNumbers = new List<string>();

                    if (Constants.ACCESSORY_TYPE_DOCK.Equals(accessoryType))
                    {
                        if (!string.IsNullOrEmpty(productAccessory.ioports))
                        {
                            ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                            productFeatureDTO.elementName = "ioports";
                            productFeatureDTO.elementValue = productAccessory.ioports;
                            productFeatureDTOList.Add(productFeatureDTO);
                        }
                    }
                    //else if (Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType))
                    //{
                    //    if (!string.IsNullOrEmpty(productAccessory.displaytype))
                    //    {
                    //        ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                    //        productFeatureDTO.elementName = "displaytype";
                    //        productFeatureDTO.elementValue = productAccessory.displaytype;
                    //        productFeatureDTOList.Add(productFeatureDTO);
                    //    }
                    //}

                    foreach (string featurePriority in productFeaturePriorityList)
                    {
                        if (productFeatureDTOList.Count == 3)
                            break;

                        if (Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType) && featurePriority.Equals("ksp_01_headline_medium"))
                        {
                            if (!string.IsNullOrEmpty(productAccessory.monitorfeat))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                productFeatureDTO.elementName = "monitorfeat";
                                productFeatureDTO.elementValue = productAccessory.monitorfeat;
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeatureDTOList.Count == 3)
                                    break;
                            }
                        }

                        foreach (ProductFeature productFeature in productKSPList)
                        {
                            if (featurePriority.Equals(productFeature.elementName))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                CopyProperties.Copy(productFeature, productFeatureDTO);
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeature.elementValue != null)
                                {
                                    List<string> matches =
                                    Regex.Matches(productFeature.elementValue.Replace(Environment.NewLine, ""), @"\[([^]]*)\]")
                                        .Cast<Match>()
                                        .Select(x => x.Groups[1].Value)
                                        .ToList();

                                    matches = matches.Distinct().ToList();

                                    foreach (string footNoteMatchedNumber in matches)
                                    {
                                        if (matchedNumbers.Contains(footNoteMatchedNumber))
                                            continue;
                                        ProductFeature productFootNote = productFootNoteList.FirstOrDefault(p => p.elementValue.StartsWith("[" + footNoteMatchedNumber + "]"));
                                        if (productFootNote != null)
                                        {
                                            ProductFeatureDTO productFeatureDTOFootNote = new ProductFeatureDTO();
                                            CopyProperties.Copy(productFootNote, productFeatureDTOFootNote);
                                            productFootNoteDTOList.Add(productFeatureDTOFootNote);
                                        }
                                        matchedNumbers.Add(footNoteMatchedNumber);
                                    }
                                }
                            }
                        }
                    }

                    productAccessoryDTO.productFeatureList = productFeatureDTOList;
                    productAccessoryDTO.productFootNoteList = productFootNoteDTOList;
                    // TODO: need to remove the ioports parameter from response once changes done from frontend side
                    productAccessoryDTO.ioports = "";
                }
                productAccessoryDTOList.Add(productAccessoryDTO);
            }

            // Ordering productAccessoryDTOList/sortedProductAccessoryDTOList in 'order' assending order
            List<ProductAccessoryDTO> sortedProductAccessoryDTOList = new List<ProductAccessoryDTO>();
            sortedProductAccessoryDTOList = productAccessoryDTOList.Where(p => p.order > 0).OrderBy(p => p.order).ToList();
            sortedProductAccessoryDTOList.AddRange(productAccessoryDTOList.Where(p => p.order <= 0).ToList());

            int order = 1;
            foreach (ProductAccessoryDTO accessorySkuDTO in sortedProductAccessoryDTOList)
            {
                accessorySkuDTO.order = order;
                order++;
            }

            // Sort the product accessories based on the distinct series
            /**
             * accessories : marketing_sub_category_pmoid
             * services    : smallSeriesPmoid
             * dock/display: bigSeriesPmoid
             **/
            if (Constants.ACCESSORY_TYPE_ACCESSORY.Equals(accessoryType))
            {
                sortedProductAccessoryDTOList = sortAllProductAccessoriesByDistinctSeries(sortedProductAccessoryDTOList, "marketing_sub_category_pmoid");
            }
            else if (Constants.ACCESSORY_TYPE_SERVICE.Equals(accessoryType))
            {
                sortedProductAccessoryDTOList = sortAllProductAccessoriesByDistinctSeries(sortedProductAccessoryDTOList, "smallSeriesPmoid");
            }
            else
            {
                sortedProductAccessoryDTOList = sortAllProductAccessoriesByDistinctSeries(sortedProductAccessoryDTOList, "bigSeriesPmoid");
            }

            RecommendationProductAccessoryDTOResponse response = new RecommendationProductAccessoryDTOResponse();
            response.totalCount = sortedProductAccessoryDTOList.Count;

            sortedProductAccessoryDTOList = getRequiredRangeProductList(sortedProductAccessoryDTOList, recommendation);
            //Logs.Info("The response of RecommendationService.GetRecommendedAccessoriesAsync is, Response[List<ProductAccessoryDTO>, Size: " + ((sortedProductAccessoryDTOList == null) ? '-' : sortedProductAccessoryDTOList.Count) + "]");
            response.recommendations = sortedProductAccessoryDTOList;

            return response;
        }

        /// <summary>
        /// GetRecommendedDaasServicesAsync will return the list of recommended daas services after applying the tags/rules on it
        /// </summary>
        /// <param name="recommendation">request object</param>
        /// <returns RecommendationProductDaasAccessoryDTOResponse></returns>
        public RecommendationProductDaasAccessoryDTOResponse GetRecommendedDaasServicesAsync(Recommendation recommendation)
        {
            //Logs.InfoFormat("RecommendationService.RecommendationProductDaasAccessoryDTOResponse method has been called having parameters [Recommendation]");
            List<CMSWeightedTag> weightedTagList = new List<CMSWeightedTag>();
            List<CMSAnswer> allDBRequestedCMSAnswers = new List<CMSAnswer>();
            List<CMSAnswer> nonFilterDBCMSAnswers = new List<CMSAnswer>();
            List<CMSAnswer> filterDBCMSAnswers = new List<CMSAnswer>();
            string productDaasAccessoriesAlias = "pda";
            ResourceManager rm = new ResourceManager("hpi_user_assessment_tool.UAT_resources", Assembly.GetExecutingAssembly());
            String ImageServerURL = rm.GetString("DaasServicesImageServerURL", CultureInfo.CurrentCulture);

            if (recommendation.answerIds != null && recommendation.answerIds.Count > 0)
            {
                allDBRequestedCMSAnswers = recommendationRepository.GetAnswerByIdsAsync(recommendation.answerIds).Result;

                List<CMSAnswer> allFilterDBCMSAnswers = recommendationRepository.GetAnswersByQuestionAssessmentStepAsync(Constants.FILTER_DAAS_SERVICE_ASSESSMENT_LABEL);
                if (allFilterDBCMSAnswers != null && allFilterDBCMSAnswers.Count > 0)
                {
                    // Filter out the accessories filter answers
                    List<int> nonFilterAnswerIds = recommendation.answerIds.Except(allFilterDBCMSAnswers.Select(p => p.Id).ToList()).ToList();
                    nonFilterDBCMSAnswers = allDBRequestedCMSAnswers.Where(p => nonFilterAnswerIds.Contains(p.Id)).ToList();
                    filterDBCMSAnswers = allDBRequestedCMSAnswers.Except(nonFilterDBCMSAnswers).ToList();
                }

                if (nonFilterDBCMSAnswers.Count == 0)
                {
                    nonFilterDBCMSAnswers = allDBRequestedCMSAnswers;
                }
            }

            // Getting Standalone Tags
            includeStandaloneTags(nonFilterDBCMSAnswers, weightedTagList, Constants.ENTITY_TYPE_DAAS_SERVICE, "");

            // Getting Reliant Standalone Tags
            includeReliantStandaloneTags(recommendation, nonFilterDBCMSAnswers, weightedTagList, Constants.ENTITY_TYPE_DAAS_SERVICE, "");

            List<ProductDaasService> dbProductAccessoryList = new List<ProductDaasService>();
            List<WeightCalculation> orderedAVGWeightList = new List<WeightCalculation>();

            // Apply Accessories Filters on the recommended Skus
            if (filterDBCMSAnswers.Count > 0)
            {
                string filterQuery = buildFilterQueryForAccessories(filterDBCMSAnswers, productDaasAccessoriesAlias);
                dbProductAccessoryList = GetRecommendedDaasAccessories(nonFilterDBCMSAnswers, weightedTagList, recommendation, filterQuery, productDaasAccessoriesAlias);
            }
            else
            {
                dbProductAccessoryList = GetRecommendedDaasAccessories(nonFilterDBCMSAnswers, weightedTagList, recommendation, "", productDaasAccessoriesAlias);
            }

            // Calculating the Weight for each Accessories
            orderedAVGWeightList = PopulateWeightMap(weightedTagList, dbProductAccessoryList, Constants.ENTITY_TYPE_DAAS_SERVICE, recommendation);

            // Ordering orderedAVGWeightList/sortedAVGWeightList in 'order' assending order
            List<WeightCalculation> sortedAVGWeightList = new List<WeightCalculation>();
            sortedAVGWeightList = orderedAVGWeightList.Where(p => p.order > 0).OrderBy(p => p.order).ToList();
            sortedAVGWeightList.AddRange(orderedAVGWeightList.Where(p => p.order <= 0).ToList());

            orderedAVGWeightList = sortedAVGWeightList;

            List<string> productPmoidsList = dbProductAccessoryList.Select(p => p.pmoid).ToList<string>();

            //Getting the Product Features list for all filtered products
            List<ProductDaasFeature> productsFeatureList = recommendationRepository.GetProductDaasFeaturesByPmoidsAsync(productPmoidsList, recommendation.country, recommendation.language).Result;

            List<ProductDaasServiceDTO> productAccessoryDTOList = new List<ProductDaasServiceDTO>();
            foreach (ProductDaasService productAccessory in dbProductAccessoryList)
            {
                ProductDaasServiceDTO productAccessoryDTO = new ProductDaasServiceDTO(recommendation.debug);
                CopyProperties.Copy(productAccessory, productAccessoryDTO);

                if (!string.IsNullOrEmpty(productAccessory.primaryImageUrl))
                {
                    productAccessoryDTO.primaryImageUrl = ImageServerURL + productAccessory.primaryImageUrl;
                }
                WeightCalculation weightCalculation = orderedAVGWeightList.FirstOrDefault(p => p.sku == productAccessory.sku);
                if (weightCalculation != null)
                {
                    productAccessoryDTO.order = weightCalculation.order;
                    productAccessoryDTO.averageWeight = weightCalculation.averageWeight;
                    productAccessoryDTO.totalWeight = weightCalculation.totalWeight;
                    productAccessoryDTO.totalNumberofWeightedTag = weightCalculation.totalNumberOfWeightedTags;
                    productAccessoryDTO.matchedWeightedTags = weightCalculation.matchedCMSWeightedTagList;
                    productAccessoryDTO.unmatchedWeightedTags = weightCalculation.unmatchedCMSWeightedTagList;
                }

                //Getting the Product Features list for particular product
                List<ProductDaasFeature> productFeatureList = productsFeatureList.Where(p => p.pmoid == productAccessoryDTO.pmoid).ToList<ProductDaasFeature>();


                if (productFeatureList != null)
                {
                    List<ProductFeatureDTO> productFeatureDTOList = new List<ProductFeatureDTO>();
                    List<ProductFeatureDTO> productFootNoteDTOList = new List<ProductFeatureDTO>();
                    List<string> productFeaturePriorityList = new List<string> { "ksp_01_suppt_01_short", "ksp_02_suppt_01_short", "ksp_03_suppt_01_short", "ksp_04_suppt_01_short", "ksp_01_headline_medium", "ksp_02_headline_medium", "ksp_03_headline_medium", "ksp_04_headline_medium" };
                    List<ProductDaasFeature> productKSPList = productFeatureList.Where(p => p.elementName.StartsWith("ksp")).ToList();
                    List<ProductDaasFeature> productFootNoteList = productFeatureList.Where(p => p.elementName.StartsWith("msgfootnote")).ToList();
                    List<string> matchedNumbers = new List<string>();

                    foreach (string featurePriority in productFeaturePriorityList)
                    {
                        if (productFeatureDTOList.Count == 3)
                            break;

                        foreach (ProductDaasFeature productFeature in productKSPList)
                        {
                            if (featurePriority.Equals(productFeature.elementName))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                CopyProperties.Copy(productFeature, productFeatureDTO);
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeature.elementValue != null)
                                {
                                    List<string> matches =
                                    Regex.Matches(productFeature.elementValue.Replace(Environment.NewLine, ""), @"\[([^]]*)\]")
                                        .Cast<Match>()
                                        .Select(x => x.Groups[1].Value)
                                        .ToList();

                                    matches = matches.Distinct().ToList();

                                    foreach (string footNoteMatchedNumber in matches)
                                    {
                                        if (matchedNumbers.Contains(footNoteMatchedNumber))
                                            continue;
                                        ProductDaasFeature productFootNote = productFootNoteList.FirstOrDefault(p => p.elementValue.StartsWith("[" + footNoteMatchedNumber + "]"));
                                        if (productFootNote != null)
                                        {
                                            ProductFeatureDTO productFeatureDTOFootNote = new ProductFeatureDTO();
                                            CopyProperties.Copy(productFootNote, productFeatureDTOFootNote);
                                            productFootNoteDTOList.Add(productFeatureDTOFootNote);
                                        }
                                        matchedNumbers.Add(footNoteMatchedNumber);
                                    }
                                }
                            }
                        }
                    }

                    productAccessoryDTO.productFeatureList = productFeatureDTOList;
                    productAccessoryDTO.productFootNoteList = productFootNoteDTOList;
                }
                productAccessoryDTOList.Add(productAccessoryDTO);
            }

            // Ordering productAccessoryDTOList/sortedProductAccessoryDTOList in 'order' assending order
            List<ProductDaasServiceDTO> sortedProductAccessoryDTOList = new List<ProductDaasServiceDTO>();
            sortedProductAccessoryDTOList = productAccessoryDTOList.Where(p => p.order > 0).OrderBy(p => p.order).ToList();
            sortedProductAccessoryDTOList.AddRange(productAccessoryDTOList.Where(p => p.order <= 0).ToList());

            int order = 1;
            foreach (ProductDaasServiceDTO accessorySkuDTO in sortedProductAccessoryDTOList)
            {
                accessorySkuDTO.order = order;
                order++;
            }

            // Sort the product daas services based on the distinct plan
            sortedProductAccessoryDTOList = sortAllProductDaasServicesByDistinctSeries(sortedProductAccessoryDTOList, "plan");

            RecommendationProductDaasAccessoryDTOResponse response = new RecommendationProductDaasAccessoryDTOResponse();
            response.totalCount = sortedProductAccessoryDTOList.Count;

            sortedProductAccessoryDTOList = getRequiredRangeProductList(sortedProductAccessoryDTOList, recommendation);
            //Logs.Info("The response of RecommendationService.RecommendationProductDaasAccessoryDTOResponse is, Response[List<ProductAccessoryDTO>, Size: " + ((sortedProductAccessoryDTOList == null) ? '-' : sortedProductAccessoryDTOList.Count) + "]");
            response.recommendations = sortedProductAccessoryDTOList;

            return response;
        }

        /// <summary>
        /// PopulateWeightMap is calculating the weight for each product, total weight/avg weight/applied tags
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="weightedTagList">use to calculate the weight</param>
        /// <param name="productList">Generic list of product can be included sku's and accessories</param>
        /// <param name="entityType">Sku/Accessory</param>
        /// <param name="recommendation">requested object</param>
        /// <returns List<WeightCalculation>></returns>
        public List<WeightCalculation> PopulateWeightMap<T>(List<CMSWeightedTag> weightedTagList, List<T> productList, string entityType, Recommendation recommendation)
        {
            List<WeightCalculation> orderedAVGWeightList = new List<WeightCalculation>();
            try
            {
                //Logs.Info("RecommendationService.PopulateWeightMap method has been called having parameters [weightedTagList {Size: " + ((weightedTagList == null) ? '-' : weightedTagList.Count) + "}, productList {Size: " + ((productList == null) ? '-' : productList.Count) + "}, entityType: " + entityType + ", recommendation ]");
                Dictionary<string, WeightCalculation> weightMap = new Dictionary<string, WeightCalculation>();
                List<ProductSkuAccessory> productSkuAccessoryList = new List<ProductSkuAccessory>();
                float maxRank = 0;
                if (entityType.Equals(Constants.APPLIED_ON_ENTITY_SERVICE))
                {
                    List<string> accessorySKUs = productList.Select(o => (string)o.GetType().GetProperty("sku").GetValue(o, null)).ToList();
                    productSkuAccessoryList = recommendationRepository.GetProductSkuAccessoryByAccessorySkusAsync(accessorySKUs, recommendation.pmoid.ToString()).Result;
                    if (productSkuAccessoryList.Count > 0)
                    {
                        maxRank = float.Parse(productSkuAccessoryList.Max(p => p.rank));
                    }
                }

                foreach (object product in productList)
                {
                    List<CMSWeightedTag> unappliedCMSWeightedTagList = new List<CMSWeightedTag>();
                    string sku = (string)product.GetType().GetProperty("sku").GetValue(product, null);
                    string pmoid = (string)product.GetType().GetProperty("pmoid").GetValue(product, null);
                    string smallSeriesPmoid = "";
                    try
                    {
                        smallSeriesPmoid = (string)product.GetType().GetProperty("smallSeriesPmoid").GetValue(product, null);
                    }
                    catch (Exception ex) { }

                    string bigSeriesPmoid = "";
                    string marketing_sub_category_pmoid = "";
                    try
                    {
                        marketing_sub_category_pmoid = (string)product.GetType().GetProperty("marketing_sub_category_pmoid").GetValue(product, null);
                        bigSeriesPmoid = (string)product.GetType().GetProperty("bigSeriesPmoid").GetValue(product, null);
                    }
                    catch (Exception ex) { }

                    foreach (CMSWeightedTag cmsWeightedTag in weightedTagList)
                    {
                        bool isMatched = false;
                        object linkedObject = null;
                        foreach (CMSWeightedTagRule cmsWeightedTagRule in cmsWeightedTag.CMSWeightedTagRules)
                        {
                            isMatched = false;
                            string value = "";
                            // checking if rule get matched for ProductEDWIndustryTrends table
                            if (cmsWeightedTag.appliedOnTable.Equals("ProductEDWIndustryTrends"))
                            {
                                string productSmallSeriesPmoid = (string)product.GetType().GetProperty("smallSeriesPmoid").GetValue(product, null);

                                // building where clause from weighted tags/rules
                                string whereCondition = buildWhereConditionForWeightedRules(cmsWeightedTag);
                                if (!string.IsNullOrEmpty(whereCondition))
                                {
                                    string query = "SELECT * FROM ProductEDWIndustryTrends WHERE CAP_seriesPmoid = '" + productSmallSeriesPmoid + "' AND (" + whereCondition + ")";
                                    ProductEDWIndustryTrend productEDWIndustryTrend = recommendationRepository.GetEDWIndustryTrendsBySeriesPmoid(query).Result;
                                    if (productEDWIndustryTrend != null)
                                    {
                                        isMatched = true;
                                        linkedObject = productEDWIndustryTrend;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        // breaking the loop for next iteration
                                        break;
                                    }
                                }
                                //}
                            }
                            else if (cmsWeightedTag.appliedOnTable.Equals("ProductSeriesFeatureMatrices"))
                            {
                                string productSmallSeriesPmoid = (string)product.GetType().GetProperty("smallSeriesPmoid").GetValue(product, null);

                                // building where clause from weighted tags/rules
                                string whereCondition = buildWhereConditionForWeightedRules(cmsWeightedTag);
                                if (!string.IsNullOrEmpty(whereCondition))
                                {
                                    string query = "SELECT * FROM ProductSeriesFeatureMatrices WHERE Series_PMOID = '" + productSmallSeriesPmoid + "' AND (" + whereCondition + ")";
                                    ProductSeriesFeatureMatrix productSeriesFeatureMatrix = recommendationRepository.GetSeriesFeatureMatrixBySeriesPmoid(query).Result;
                                    if (productSeriesFeatureMatrix != null)
                                    {
                                        isMatched = true;
                                        linkedObject = productSeriesFeatureMatrix;
                                    }
                                    else
                                    {
                                        isMatched = false;
                                        // breaking the loop for next iteration
                                        break;
                                    }
                                }
                                //}
                            }
                            else
                            {
                                try
                                {
                                    value = product.GetType().GetProperty(cmsWeightedTagRule.appliedOnColumn).GetValue(product, null).ToString();
                                }
                                catch (Exception ex)
                                {
                                    Logs.ErrorFormat(ex.Message, ex);
                                    Console.WriteLine(ex.Message);
                                }
                            }

                            // Process Tags for this entity table
                            string criteria = cmsWeightedTagRule.criteria.Replace("'", "");
                            if (!string.IsNullOrEmpty(value))
                            {
                                value = value.Replace("'", "");
                            }


                            if (cmsWeightedTagRule.condition.Equals("contains"))
                            {
                                string[] criteriaArr = criteria.Split(',');
                                foreach (string containsValue in criteriaArr)
                                {
                                    if (!string.IsNullOrEmpty(value) && value.Contains(containsValue))
                                    {
                                        isMatched = true;
                                        break;
                                    }
                                }
                                if (!isMatched)
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("notContains"))
                            {
                                string[] criteriaArr = criteria.Split(',');
                                foreach (string containsValue in criteriaArr)
                                {
                                    if (!string.IsNullOrEmpty(value) && !value.Contains(containsValue))
                                    {
                                        isMatched = true;
                                        break;
                                    }
                                }
                                if (!isMatched)
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("equals"))
                            {

                                if (criteria.Equals(value))
                                {
                                    isMatched = true;
                                }
                                else
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("notEquals"))
                            {

                                if (!criteria.Equals(value))
                                {
                                    isMatched = true;
                                }
                                else
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("minMaxRange"))
                            {
                                string[] criteriaList = criteria.Split(',');

                                if (string.IsNullOrEmpty(value))
                                {
                                    value = "0";
                                }

                                if (float.Parse(value) >= float.Parse(criteriaList[0]) && float.Parse(value) <= float.Parse(criteriaList[1]))
                                {
                                    isMatched = true;
                                }
                                else
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("greaterThan"))
                            {
                                if (string.IsNullOrEmpty(value))
                                {
                                    value = "0";
                                }

                                if ((float.Parse(value) > float.Parse(criteria)))
                                {
                                    isMatched = true;
                                }
                                else
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("lessThan"))
                            {
                                if (string.IsNullOrEmpty(value))
                                {
                                    value = "0";
                                }

                                if ((float.Parse(value) < float.Parse(criteria)))
                                {
                                    isMatched = true;
                                }
                                else
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                            else if (cmsWeightedTagRule.condition.Equals("daysBetween") && !string.IsNullOrEmpty(value) && !value.Equals("NULL"))
                            {
                                string[] criteriaList = criteria.Split(',');

                                DateTime valueDateTime = Convert.ToDateTime(value);
                                DateTime todayDateTime = DateTime.Now;
                                double numberOfDays = (todayDateTime - valueDateTime).TotalDays;

                                if (numberOfDays >= double.Parse(criteriaList[0]) && numberOfDays <= double.Parse(criteriaList[1]))
                                {
                                    isMatched = true;
                                }
                                else
                                {
                                    isMatched = false;
                                    // breaking the loop for next iteration
                                    break;
                                }
                            }
                        }// End of weightedtag rules iteration

                        if (isMatched)
                        {
                            WeightCalculation weightCalculationRules;
                            float weight = float.Parse(cmsWeightedTag.weight);
                            float inverseMultiplierValue = 1;
                            // calculting weight for multiplierColumn for ProductAccessories and ProductSkus
                            if (!string.IsNullOrEmpty(cmsWeightedTag.multiplierColumn) && (cmsWeightedTag.appliedOnTable.Equals("ProductSkus") || cmsWeightedTag.appliedOnTable.Equals("ProductAccessories")))
                            {
                                try
                                {
                                    string value = (string)product.GetType().GetProperty(cmsWeightedTag.multiplierColumn).GetValue(product, null);
                                    if (string.IsNullOrEmpty(value))
                                    {
                                        value = "0";
                                    }

                                    float multiplierWeightedValue = float.Parse(value);
                                    if ((bool)cmsWeightedTag.inverseMultiplier)
                                    {
                                        weight = float.Parse(cmsWeightedTag.weight) * (inverseMultiplierValue - multiplierWeightedValue);
                                    }
                                    else
                                    {
                                        weight = float.Parse(cmsWeightedTag.weight) * multiplierWeightedValue;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logs.ErrorFormat(ex.Message, ex);
                                    Console.WriteLine(ex.Message);
                                }

                            }
                            else if (!string.IsNullOrEmpty(cmsWeightedTag.multiplierColumn) && linkedObject != null)  // calculting weight for other realted tables
                            {
                                try
                                {
                                    string value = (string)linkedObject.GetType().GetProperty(cmsWeightedTag.multiplierColumn).GetValue(linkedObject, null);
                                    if (string.IsNullOrEmpty(value))
                                    {
                                        value = "0";
                                    }

                                    float multiplierWeightedValue = float.Parse(value);

                                    if ((bool)cmsWeightedTag.inverseMultiplier)
                                    {
                                        weight = float.Parse(cmsWeightedTag.weight) * (inverseMultiplierValue - multiplierWeightedValue);
                                    }
                                    else
                                    {
                                        weight = float.Parse(cmsWeightedTag.weight) * multiplierWeightedValue;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logs.ErrorFormat(ex.Message, ex);
                                    Console.WriteLine(ex.Message);
                                }
                            }
                            // If already exist in the map
                            if (weightMap.TryGetValue(sku, out weightCalculationRules))
                            {
                                weightCalculationRules.totalWeight = weightCalculationRules.totalWeight + weight;
                                weightCalculationRules.matchedCMSWeightedTagList.Add(cmsWeightedTag);
                                weightMap[sku] = weightCalculationRules;
                            }
                            else
                            {
                                weightCalculationRules = new WeightCalculation();
                                weightCalculationRules.sku = sku;
                                weightCalculationRules.pmoid = pmoid;
                                weightCalculationRules.smallSeriesPmoid = smallSeriesPmoid;
                                weightCalculationRules.bigSeriesPmoid = bigSeriesPmoid;
                                weightCalculationRules.marketing_sub_category_pmoid = marketing_sub_category_pmoid;
                                weightCalculationRules.totalWeight = weight;
                                weightCalculationRules.matchedCMSWeightedTagList.Add(cmsWeightedTag);
                                weightMap[sku] = weightCalculationRules;
                            }
                        }
                        else
                        {
                            unappliedCMSWeightedTagList.Add(cmsWeightedTag);
                        }
                    } // End of weighted tag list iteration

                    // Add weight 1 to the dock/display/service/accessory if matched with top recommended
                    if (!entityType.Equals(Constants.ENTITY_TYPE_SKU))
                    {
                        // recommendation.pmoid == ProductSku.pmoid
                        ProductSku productSku = recommendationRepository.GetProductSkuByPmoid(recommendation.pmoid.ToString()).Result;
                        // sku == ProductAccessories.sku
                        ProductSeriesTopRecAccessory productSeriesTopRecAccessory = recommendationRepository.GetSeriesTopRecAccessory(productSku.smallSeriesPmoid, sku).Result;

                        int weight = 1;
                        CMSWeightedTag topRecCMSWeightedTag = new CMSWeightedTag();
                        topRecCMSWeightedTag.name = "Default: Top Recommended accessories";
                        topRecCMSWeightedTag.description = "Added weight 1 to the accessory as its matched with top recommended accessories";
                        topRecCMSWeightedTag.weight = weight.ToString();
                        topRecCMSWeightedTag.appliedOnTable = "ProductSeriesTopRecAccessories";

                        if (productSeriesTopRecAccessory != null)
                        {
                            WeightCalculation weightCalculationRules;

                            // If already exist in the map
                            if (weightMap.TryGetValue(sku, out weightCalculationRules))
                            {
                                weightCalculationRules.totalWeight = weightCalculationRules.totalWeight + weight;
                                weightCalculationRules.matchedCMSWeightedTagList.Add(topRecCMSWeightedTag);
                                weightMap[sku] = weightCalculationRules;
                            }
                            else
                            {
                                weightCalculationRules = new WeightCalculation();
                                weightCalculationRules.sku = sku;
                                weightCalculationRules.pmoid = pmoid;
                                weightCalculationRules.smallSeriesPmoid = smallSeriesPmoid;
                                weightCalculationRules.bigSeriesPmoid = bigSeriesPmoid;
                                weightCalculationRules.marketing_sub_category_pmoid = marketing_sub_category_pmoid;
                                weightCalculationRules.totalWeight = weight;
                                weightCalculationRules.matchedCMSWeightedTagList.Add(topRecCMSWeightedTag);
                                weightMap[sku] = weightCalculationRules;
                            }
                        }
                        else
                        {
                            unappliedCMSWeightedTagList.Add(topRecCMSWeightedTag);
                        }
                    }

                    // Add weight 1*calculatedRank to the service if ranked in the productSkuAccessories table
                    if (entityType.Equals(Constants.APPLIED_ON_ENTITY_SERVICE))
                    {
                        // recommendation.pmoid == ProductSku.pmoid
                        // string devicePmoid = recommendation.pmoid;
                        // sku == ProductAccessories.sku
                        // float rank = recommendationRepository.getAccessoryRank(devicePmoid, sku);
                        float weight = 0;
                        float rank = float.Parse(productSkuAccessoryList.Where(c => c.accessorySKU == sku && c.sku_pmoid == recommendation.pmoid.ToString()).Select(p => p.rank).SingleOrDefault());
                        if (maxRank > 0)
                        {
                            weight = 1f * (1f - (rank / maxRank));
                        }

                        CMSWeightedTag topRecCMSWeightedTag = new CMSWeightedTag();
                        topRecCMSWeightedTag.name = "Default: Top ranked services";
                        topRecCMSWeightedTag.description = "Added weight 1 to the service as its matched with top recommended services by rank";
                        topRecCMSWeightedTag.weight = weight.ToString();
                        topRecCMSWeightedTag.appliedOnTable = "ProductSkuAccessories";

                        if (rank > 0)
                        {
                            WeightCalculation weightCalculationRules;

                            // If already exist in the map
                            if (weightMap.TryGetValue(sku, out weightCalculationRules))
                            {
                                weightCalculationRules.totalWeight = weightCalculationRules.totalWeight + weight;
                                weightCalculationRules.matchedCMSWeightedTagList.Add(topRecCMSWeightedTag);
                                weightMap[sku] = weightCalculationRules;
                            }
                            else
                            {
                                weightCalculationRules = new WeightCalculation();
                                weightCalculationRules.sku = sku;
                                weightCalculationRules.pmoid = pmoid;
                                weightCalculationRules.smallSeriesPmoid = smallSeriesPmoid;
                                weightCalculationRules.bigSeriesPmoid = bigSeriesPmoid;
                                weightCalculationRules.marketing_sub_category_pmoid = marketing_sub_category_pmoid;
                                weightCalculationRules.totalWeight = weight;
                                weightCalculationRules.matchedCMSWeightedTagList.Add(topRecCMSWeightedTag);
                                weightMap[sku] = weightCalculationRules;
                            }
                        }
                        else
                        {
                            unappliedCMSWeightedTagList.Add(topRecCMSWeightedTag);
                        }
                    }


                    // Calculating average weight
                    WeightCalculation weightCalculationTags;
                    if (weightMap.TryGetValue(sku, out weightCalculationTags))
                    {
                        int totalTags = weightedTagList.Count;
                        if (!entityType.Equals(Constants.ENTITY_TYPE_SKU))
                        {
                            // Adding one more count for the accessorie's top recommended default tag
                            totalTags = totalTags + 1;
                        }
                        if (entityType.Equals(Constants.ACCESSORY_TYPE_SERVICE))
                        {
                            totalTags = totalTags + 1;
                        }
                        if (weightCalculationTags.totalWeight > 0)
                        {
                            weightCalculationTags.averageWeight = weightCalculationTags.totalWeight / totalTags;
                        }
                        weightCalculationTags.totalNumberOfWeightedTags = totalTags;
                        weightCalculationTags.unmatchedCMSWeightedTagList.AddRange(unappliedCMSWeightedTagList.ToList());
                        weightMap[sku] = weightCalculationTags;
                    }
                    else
                    {
                        int totalTags = weightedTagList.Count;
                        if (!entityType.Equals(Constants.ENTITY_TYPE_SKU))
                        {
                            // Adding one more count for the accessorie's top recommended default tag
                            totalTags = totalTags + 1;
                        }
                        if (entityType.Equals(Constants.ACCESSORY_TYPE_SERVICE))
                        {
                            // Adding one more count for the service's rank default tag
                            totalTags = totalTags + 1;
                        }
                        WeightCalculation weightCalculationRules = new WeightCalculation();
                        weightCalculationRules.sku = sku;
                        weightCalculationRules.pmoid = pmoid;
                        weightCalculationRules.smallSeriesPmoid = smallSeriesPmoid;
                        weightCalculationRules.bigSeriesPmoid = bigSeriesPmoid;
                        weightCalculationRules.marketing_sub_category_pmoid = marketing_sub_category_pmoid;
                        weightCalculationRules.totalNumberOfWeightedTags = totalTags;
                        weightCalculationRules.unmatchedCMSWeightedTagList.AddRange(unappliedCMSWeightedTagList.ToList());
                        weightMap[sku] = weightCalculationRules;
                    }

                    List<WeightCalculation> avgWeightList = weightMap.Values.ToList();
                    orderedAVGWeightList = avgWeightList.OrderByDescending(x => x.averageWeight).ToList<WeightCalculation>();

                    int order = 1;
                    foreach (WeightCalculation weightCalculation in orderedAVGWeightList)
                    {
                        weightCalculation.order = order++;
                    }
                } // End of productlist iteration
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                Console.WriteLine(ex.Message);
            }
            //Logs.Info("The response of RecommendationService.PopulateWeightMap method is, Response[List<WeightCalculation>, Size: " + ((orderedAVGWeightList == null) ? '-' : orderedAVGWeightList.Count) + "]");
            return orderedAVGWeightList;
        }

        /// <summary>
        /// buildWhereConditionForWeightedRules is building where clause using given rules
        /// </summary>
        /// <param name="cmsWeightedTag"></param>
        /// <returns string>where condition clause</returns>
        public string buildWhereConditionForWeightedRules(CMSWeightedTag cmsWeightedTag)
        {
            //Logs.Info("RecommendationService.buildWhereConditionForWeightedRules method has been called having parameter[CMSWeightedTag]");
            String whereCondition = "";
            if (cmsWeightedTag.CMSWeightedTagRules != null)
            {
                foreach (CMSWeightedTagRule cmsWeightedTagRule in cmsWeightedTag.CMSWeightedTagRules)
                {
                    string condition = "";
                    if (cmsWeightedTagRule.condition.Equals("contains"))
                    {
                        string[] containsArr = cmsWeightedTagRule.criteria.Replace("'", "").Split(',');
                        foreach (string containsValue in containsArr)
                        {
                            if (string.IsNullOrEmpty(condition))
                            {
                                condition = cmsWeightedTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                            }
                            else
                            {
                                condition = condition + " OR " + cmsWeightedTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                            }
                        }
                    }
                    else if (cmsWeightedTagRule.condition.Equals("notContains"))
                    {
                        string[] containsArr = cmsWeightedTagRule.criteria.Replace("'", "").Split(',');
                        foreach (string containsValue in containsArr)
                        {
                            if (string.IsNullOrEmpty(condition))
                            {
                                condition = cmsWeightedTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                            }
                            else
                            {
                                condition = condition + " OR " + cmsWeightedTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                            }
                        }
                    }
                    else if (cmsWeightedTagRule.condition.Equals("equals"))
                    {
                        condition = cmsWeightedTagRule.appliedOnColumn + " = " + cmsWeightedTagRule.criteria;
                    }
                    else if (cmsWeightedTagRule.condition.Equals("notEquals"))
                    {
                        condition = cmsWeightedTagRule.appliedOnColumn + " != " + cmsWeightedTagRule.criteria;
                    }
                    else if (cmsWeightedTagRule.condition.Equals("minMaxRange"))
                    {
                        string[] range = cmsWeightedTagRule.criteria.Split(',');
                        condition = cmsWeightedTagRule.appliedOnColumn + " BETWEEN " + range[0] + " AND " + range[1];
                    }
                    else if (cmsWeightedTagRule.condition.Equals("greaterThan"))
                    {
                        condition = cmsWeightedTagRule.appliedOnColumn + " > " + cmsWeightedTagRule.criteria;
                    }
                    else if (cmsWeightedTagRule.condition.Equals("lessThan"))
                    {
                        condition = cmsWeightedTagRule.appliedOnColumn + " < " + cmsWeightedTagRule.criteria;
                    }

                    if (!string.IsNullOrEmpty(condition))
                    {
                        if (string.IsNullOrEmpty(whereCondition))
                        {
                            whereCondition = condition;
                            condition = "";
                        }
                        else
                        {
                            whereCondition = whereCondition + " AND " + condition;
                            condition = "";
                        }
                    }
                } // End of rules iteration
            }
            //Logs.InfoFormat("The response of RecommendationService.buildWhereConditionForWeightedRules method is WhereCondition: " + whereCondition);
            return whereCondition;
        }

        /// <summary>
        /// sortProductSkuByDistinctSeries is sorting the top product list based on smallSeriesPmoid
        /// </summary>
        /// <param name="sortedProductSkuDTOList"></param>
        /// <returns List<WeightCalculation>></returns>
        public List<WeightCalculation> sortTopProductSkuByDistinctSeries(List<WeightCalculation> sortedAVGWeightList)
        {
            //Logs.Info("RecommendationService.sortProductSkuByDistinctSeries method has been called having parameter[List<WeightCalculation>, Size: " + ((sortedAVGWeightList == null) ? '-' : sortedAVGWeightList.Count) + "]");
            int requiredNumberOfDistinctProduct = 3;

            if (sortedAVGWeightList.Count > requiredNumberOfDistinctProduct)
            {
                // Get the distinct smallSeriesPmoid SKU's
                List<WeightCalculation> distinctAVGWeightList = sortedAVGWeightList
                  .GroupBy(p => p.smallSeriesPmoid)
                  .Select(g => g.First())
                  .ToList();
                List<WeightCalculation> topRecommendedProducts = new List<WeightCalculation>();
                List<WeightCalculation> otherRecommendedProducts = new List<WeightCalculation>();

                // If distinct product count is greter then required distinct product
                if (distinctAVGWeightList.Count >= requiredNumberOfDistinctProduct)
                {
                    // Getting first top required distinct products
                    topRecommendedProducts = distinctAVGWeightList.GetRange(0, requiredNumberOfDistinctProduct);
                    // Getting other then top required distinct products
                    otherRecommendedProducts = sortedAVGWeightList.Where(p => !topRecommendedProducts.Any(p2 => p2.pmoid == p.pmoid)).ToList<WeightCalculation>();
                }
                else if (distinctAVGWeightList.Count > 0)
                {
                    topRecommendedProducts = distinctAVGWeightList;
                    otherRecommendedProducts = sortedAVGWeightList.Where(p => !topRecommendedProducts.Any(p2 => p2.pmoid == p.pmoid)).ToList<WeightCalculation>();
                }

                // TOP + other.order > 0 + other.order <= 0
                sortedAVGWeightList = topRecommendedProducts;
                sortedAVGWeightList.AddRange(otherRecommendedProducts.Where(p => p.order > 0).OrderBy(p => p.order).ToList());
                sortedAVGWeightList.AddRange(otherRecommendedProducts.Where(p => p.order <= 0).ToList());

                int order = 1;
                foreach (WeightCalculation weightCalculation in sortedAVGWeightList)
                {
                    weightCalculation.order = order;
                    order++;
                }
            }
            //Logs.Info("The response of RecommendationService.sortProductSkuByDistinctSeries is, Response[List<WeightCalculation>, Size: " + ((sortedAVGWeightList == null) ? '-' : sortedAVGWeightList.Count) + "]");

            return sortedAVGWeightList;
        }


        public List<ProductSkuDTO> sortAllProductSkuByDistinctSeries(List<ProductSkuDTO> productSkuDTOList)
        {
            List<ProductSkuDTO> sortedProductSkuDTOList = new List<ProductSkuDTO>();
            if (productSkuDTOList != null && productSkuDTOList.Count > 0)
            {
                while (true)
                {
                    List<ProductSkuDTO> distinctProductSkuDTOList = new List<ProductSkuDTO>();
                    foreach (ProductSkuDTO productSkuDTO in productSkuDTOList)
                    {
                        if (distinctProductSkuDTOList.SingleOrDefault(p => p.smallSeriesPmoid == productSkuDTO.smallSeriesPmoid) == null)
                        {
                            distinctProductSkuDTOList.Add(productSkuDTO);
                        }
                    }

                    sortedProductSkuDTOList.AddRange(distinctProductSkuDTOList);
                    productSkuDTOList = productSkuDTOList.Except(distinctProductSkuDTOList).ToList();
                    distinctProductSkuDTOList.Clear();

                    if (productSkuDTOList.Count == 0)
                    {
                        break;
                    }
                }

                int order = 1;
                foreach (ProductSkuDTO productSkuDTO in sortedProductSkuDTOList)
                {
                    productSkuDTO.order = order;
                    order++;
                }

                return sortedProductSkuDTOList;
            }

            return productSkuDTOList;

        }

        public List<ProductAccessoryDTO> sortAllProductAccessoriesByDistinctSeries(List<ProductAccessoryDTO> productAccessoryDTOList, string sortBy)
        {
            List<ProductAccessoryDTO> sortedProductAccessoryDTOList = new List<ProductAccessoryDTO>();
            if (productAccessoryDTOList != null && productAccessoryDTOList.Count > 0)
            {
                while (true)
                {
                    List<ProductAccessoryDTO> distinctProductAccessoryDTOList = new List<ProductAccessoryDTO>();
                    foreach (ProductAccessoryDTO productSkuDTO in productAccessoryDTOList)
                    {
                        if (distinctProductAccessoryDTOList.SingleOrDefault(p => (string)p.GetType().GetProperty(sortBy).GetValue(p, null) == (string)productSkuDTO.GetType().GetProperty(sortBy).GetValue(productSkuDTO, null)) == null)
                        {
                            distinctProductAccessoryDTOList.Add(productSkuDTO);
                        }
                    }

                    sortedProductAccessoryDTOList.AddRange(distinctProductAccessoryDTOList);
                    productAccessoryDTOList = productAccessoryDTOList.Except(distinctProductAccessoryDTOList).ToList();
                    distinctProductAccessoryDTOList.Clear();

                    if (productAccessoryDTOList.Count == 0)
                    {
                        break;
                    }
                }

                int order = 1;
                foreach (ProductAccessoryDTO productAccessoryDTO in sortedProductAccessoryDTOList)
                {
                    productAccessoryDTO.order = order;
                    order++;
                }

                return sortedProductAccessoryDTOList;
            }

            return productAccessoryDTOList;
        }

        public List<ProductDaasServiceDTO> sortAllProductDaasServicesByDistinctSeries(List<ProductDaasServiceDTO> productAccessoryDTOList, string sortBy)
        {
            List<ProductDaasServiceDTO> sortedProductAccessoryDTOList = new List<ProductDaasServiceDTO>();
            if (productAccessoryDTOList != null && productAccessoryDTOList.Count > 0)
            {
                while (true)
                {
                    List<ProductDaasServiceDTO> distinctProductAccessoryDTOList = new List<ProductDaasServiceDTO>();
                    foreach (ProductDaasServiceDTO productSkuDTO in productAccessoryDTOList)
                    {
                        if (distinctProductAccessoryDTOList.SingleOrDefault(p => (string)p.GetType().GetProperty(sortBy).GetValue(p, null) == (string)productSkuDTO.GetType().GetProperty(sortBy).GetValue(productSkuDTO, null)) == null)
                        {
                            distinctProductAccessoryDTOList.Add(productSkuDTO);
                        }
                    }

                    sortedProductAccessoryDTOList.AddRange(distinctProductAccessoryDTOList);
                    productAccessoryDTOList = productAccessoryDTOList.Except(distinctProductAccessoryDTOList).ToList();
                    distinctProductAccessoryDTOList.Clear();

                    if (productAccessoryDTOList.Count == 0)
                    {
                        break;
                    }
                }

                int order = 1;
                foreach (ProductDaasServiceDTO productAccessoryDTO in sortedProductAccessoryDTOList)
                {
                    productAccessoryDTO.order = order;
                    order++;
                }

                return sortedProductAccessoryDTOList;
            }

            return productAccessoryDTOList;
        }

        /// <summary>
        /// sortTopAccessoryByDistinctCategory is sorting the product list based on marketing_sub_category_pmoid/bigSeriesPmoid
        /// </summary>
        /// <param name="sortedProductAccessoryDTOList"></param>
        /// <param name="groupBy"></param>
        /// <returns></returns>
        public List<WeightCalculation> sortTopAccessoryByDistinctCategory(List<WeightCalculation> sortedProductAccessoryDTOList, string groupBy)
        {
            //Logs.Info("RecommendationService.sortTopAccessoryByDistinctCategory method has been called having parameter [List<WeightCalculation> {Size: " + ((sortedProductAccessoryDTOList == null) ? '-' : sortedProductAccessoryDTOList.Count) + "}, groupBy: " + groupBy + "]");
            int requiredNumberOfDistinctProduct = 3;

            if (sortedProductAccessoryDTOList.Count > requiredNumberOfDistinctProduct)
            {
                // Get the distinct marketing_sub_category_pmoid/bigSeriesPmoid Accessory
                List<WeightCalculation> distinctProductSkuDTOList = sortedProductAccessoryDTOList
                  .GroupBy(p => p.GetType().GetProperty(groupBy).GetValue(p, null))
                  .Select(g => g.First())
                  .ToList();

                List<WeightCalculation> topRecommendedProducts = new List<WeightCalculation>();
                List<WeightCalculation> otherRecommendedProducts = new List<WeightCalculation>();

                if (distinctProductSkuDTOList.Count >= requiredNumberOfDistinctProduct)
                {
                    topRecommendedProducts = distinctProductSkuDTOList.GetRange(0, requiredNumberOfDistinctProduct);
                    otherRecommendedProducts = sortedProductAccessoryDTOList.Where(p => !topRecommendedProducts.Any(p2 => p2.pmoid == p.pmoid)).ToList<WeightCalculation>();
                }
                else if (distinctProductSkuDTOList.Count > 0)
                {
                    topRecommendedProducts = distinctProductSkuDTOList;
                    otherRecommendedProducts = sortedProductAccessoryDTOList.Where(p => !topRecommendedProducts.Any(p2 => p2.pmoid == p.pmoid)).ToList<WeightCalculation>();
                }

                sortedProductAccessoryDTOList = topRecommendedProducts;
                sortedProductAccessoryDTOList.AddRange(otherRecommendedProducts.Where(p => p.order > 0).OrderBy(p => p.order).ToList());
                sortedProductAccessoryDTOList.AddRange(otherRecommendedProducts.Where(p => p.order <= 0).ToList());

                int order = 1;
                foreach (WeightCalculation productAccessoryDTO in sortedProductAccessoryDTOList)
                {
                    productAccessoryDTO.order = order;
                    order++;
                }
            }
            //Logs.Info("The response of RecommendationService.sortTopAccessoryByDistinctCategory method is, Response[List<WeightCalculation>, Size: " + ((sortedProductAccessoryDTOList == null) ? '-' : sortedProductAccessoryDTOList.Count) + "]");
            return sortedProductAccessoryDTOList;
        }

        /// <summary>
        /// getRequiredRangeProductList is fetching the requested range of products, by default returning whole list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sortedProductDTOList"></param>
        /// <param name="recommendation">requested object</param>
        /// <returns List<T>></returns>
        public List<T> getRequiredRangeProductList<T>(List<T> sortedProductDTOList, Recommendation recommendation)
        {
            //Logs.Info("RecommendationService.getRequiredRangeProductList method has been called having parameter[sortedProductDTOList {Size: " + ((sortedProductDTOList == null) ? '-' : sortedProductDTOList.Count) + "}, Recommendation]");
            if (recommendation.offSet < sortedProductDTOList.Count() && recommendation.limit > sortedProductDTOList.Count())
            {
                recommendation.limit = sortedProductDTOList.Count();
            }
            else if (recommendation.offSet >= sortedProductDTOList.Count())
            {
                return sortedProductDTOList;
            }
            else if (recommendation.offSet == 0 && recommendation.limit == 0)
            {
                return sortedProductDTOList;
            }
            sortedProductDTOList = sortedProductDTOList.GetRange(recommendation.offSet, recommendation.limit - recommendation.offSet);
            //Logs.Info("The response of RecommendationService.getRequiredRangeProductList method is, Response[List<T>, Size: " + ((sortedProductDTOList == null) ? '-' : sortedProductDTOList.Count) + "]");
            return sortedProductDTOList;
        }

        public List<int> GetAnswerByLabelsAsync(List<string> answerLabels)
        {
            //Logs.Info("RecommendationService.GetAnswerByLabelsAsync method has been called having parameter[List of answerLabels, Size: " + ((answerLabels == null) ? '-' : answerLabels.Count) + "}");
            return recommendationRepository.GetAnswerByLabelsAsync(answerLabels).Result;
        }

        public List<ProductSku> GetRecommendedSku(List<CMSAnswer> dbCMSAnswers, List<CMSWeightedTag> weightedTagList, Recommendation recommendation, string filterQuery)
        {
            //Logs.Info("RecommendationService.GetRecommendedSku method has been called having parameters[List<CMSAnswer> {Size: " + ((dbCMSAnswers == null) ? '-' : dbCMSAnswers.Count) + "}, List<CMSWeightedTag> {Size: " + ((weightedTagList == null) ? '-' : weightedTagList.Count) + "}, Recommendation, filterQuery: " + filterQuery + "]");
            string query = "SELECT * FROM ProductSkus";
            String whereClause = "";

            foreach (CMSAnswer cmsAnswer in dbCMSAnswers)
            {
                List<CMSTagRule> cmsTagRuleList = new List<CMSTagRule>();
                //TODO: Define the appliedOnTable names mapping to the constant or config file and retrive from there to compare
                if (cmsAnswer != null)
                {
                    if (cmsAnswer.CMSWeightedTags != null)
                    {
                        // Adding WeightedTags to the list
                        foreach (CMSWeightedTag cmsWeightedTag in cmsAnswer.CMSWeightedTags)
                        {
                            // Add all the Tags except accessories tags for sku
                            if (cmsWeightedTag.CMSWeightedTagRules != null && !cmsWeightedTag.appliedOnTable.Equals("ProductAccessories") && Constants.APPLIED_ON_ENTITY_DEVICE.Equals(cmsWeightedTag.appliedOnEntity))
                            {
                                weightedTagList.Add(cmsWeightedTag);
                            }
                        }
                    }

                    if (cmsAnswer.CMSTags != null)
                    {
                        string tagsWhereCondition = "";
                        foreach (CMSTag cmsTag in cmsAnswer.CMSTags)
                        {
                            if (cmsTag.CMSTagRules != null && cmsTag.appliedOnTable.Equals("ProductSkus") && Constants.APPLIED_ON_ENTITY_DEVICE.Equals(cmsTag.appliedOnEntity))
                            {
                                String rulesWhereCondition = "";
                                foreach (CMSTagRule cmsTagRule in cmsTag.CMSTagRules)
                                {
                                    string condition = "";
                                    if (cmsTagRule.condition.Equals("contains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("notContains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("equals"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " = " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("notEquals"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " != " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("minMaxRange"))
                                    {
                                        string[] range = cmsTagRule.criteria.Split(',');
                                        condition = cmsTagRule.appliedOnColumn + " BETWEEN " + range[0] + " AND " + range[1];
                                    }
                                    else if (cmsTagRule.condition.Equals("greaterThan"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " > " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("lessThan"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " < " + cmsTagRule.criteria;
                                    }

                                    // Adding AND Clause for RULES
                                    if (!string.IsNullOrEmpty(condition))
                                    {
                                        condition = " ( " + condition + " ) ";
                                        if (string.IsNullOrEmpty(rulesWhereCondition))
                                        {
                                            rulesWhereCondition = condition;
                                            condition = "";
                                        }
                                        else
                                        {
                                            rulesWhereCondition = rulesWhereCondition + " AND " + condition;
                                            condition = "";
                                        }
                                    }
                                }// End of Rules Iteration

                                // Adding OR Clause for TAGS
                                if (!string.IsNullOrEmpty(rulesWhereCondition))
                                {
                                    rulesWhereCondition = " ( " + rulesWhereCondition + " ) ";
                                    if (string.IsNullOrEmpty(tagsWhereCondition))
                                    {
                                        tagsWhereCondition = rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                    else
                                    {
                                        tagsWhereCondition = tagsWhereCondition + " OR " + rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                }
                            }
                        }// End of TAGS Iteration

                        // Adding AND Clause for ANSWERS
                        if (!string.IsNullOrEmpty(tagsWhereCondition))
                        {
                            if (string.IsNullOrEmpty(whereClause))
                            {
                                whereClause = "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                            else
                            {
                                whereClause = whereClause + " AND " + "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                        }
                    }
                }
            }// End of Answers Iteration

            if (!string.IsNullOrEmpty(whereClause))
            {
                if (!string.IsNullOrEmpty(filterQuery))
                {
                    query = query + " WHERE " + whereClause + " AND " + filterQuery + " AND ";
                }
                else
                {
                    query = query + " WHERE " + whereClause + " AND ";
                }
            }
            else if (!string.IsNullOrEmpty(filterQuery))
            {
                query = query + " WHERE " + filterQuery + " AND ";
            }
            else
            {
                query = query + " WHERE";
            }

            query = query + " country = '" + recommendation.country + "' AND language = '" + recommendation.language + "'";

            // Retrieving the recommendations
            List<ProductSku> dbProductSkuList = recommendationRepository.GetSkusAsync(query).Result;

            dbProductSkuList = dbProductSkuList.Where(p => !string.IsNullOrEmpty(p.obsolete_date) && p.obsolete_date != "NULL" && DateTime.Parse(p.obsolete_date) >= DateTime.Now.Date).ToList();
            //Logs.Info("The response of RecommendationService.GetRecommendedSku is, Response[List<ProductSku>, Size: " + ((dbProductSkuList == null) ? '-' : dbProductSkuList.Count) + "]");
            return dbProductSkuList;
        }

        public List<ProductAccessory> GetRecommendedAccessories(List<CMSAnswer> dbCMSAnswers, List<CMSWeightedTag> weightedTagList, Recommendation recommendation, string filterQuery, string accessoryType, string productAccessoriesAlias)
        {
            //Logs.Info("RecommendationService.GetRecommendedAccessories method has been called having parameters[List<CMSAnswer> {Size: " + ((dbCMSAnswers == null) ? '-' : dbCMSAnswers.Count) + "}, List<CMSWeightedTag> {Size: " + ((weightedTagList == null) ? '-' : weightedTagList.Count) + "}, Recommendation, filterQuery: " + filterQuery + "]");
            ResourceManager rm = new ResourceManager("hpi_user_assessment_tool.UAT_resources", Assembly.GetExecutingAssembly());

            string query = "SELECT * FROM ProductAccessories " + productAccessoriesAlias + " JOIN ProductSkuAccessories psa ON psa.accessorySKU = " + productAccessoriesAlias + ".sku";
            String whereClause = "";

            foreach (CMSAnswer cmsAnswer in dbCMSAnswers)
            {
                List<CMSTagRule> cmsTagRuleList = new List<CMSTagRule>();
                //TODO: Define the appliedOnTable names mapping to the constant or config file and retrive from there to compare
                if (cmsAnswer != null)
                {
                    if (cmsAnswer.CMSWeightedTags != null)
                    {
                        // Adding WeightedTags to the list
                        foreach (CMSWeightedTag cmsWeightedTag in cmsAnswer.CMSWeightedTags)
                        {
                            // Add all the Tags except sku tags for accessories
                            if (cmsWeightedTag.CMSWeightedTagRules != null && !cmsWeightedTag.appliedOnTable.Equals("ProductSkus") && (Constants.APPLIED_ON_ENTITY_ALLACCESSORIES.Equals(cmsWeightedTag.appliedOnEntity) || accessoryType.Equals(cmsWeightedTag.appliedOnEntity)))
                            {
                                weightedTagList.Add(cmsWeightedTag);
                            }
                        }
                    }

                    if (cmsAnswer.CMSTags != null)
                    {
                        string tagsWhereCondition = "";
                        foreach (CMSTag cmsTag in cmsAnswer.CMSTags)
                        {
                            if (cmsTag.CMSTagRules != null && cmsTag.appliedOnTable.Equals("ProductAccessories") && (Constants.APPLIED_ON_ENTITY_ALLACCESSORIES.Equals(cmsTag.appliedOnEntity) || accessoryType.Equals(cmsTag.appliedOnEntity)))
                            {
                                String rulesWhereCondition = "";
                                foreach (CMSTagRule cmsTagRule in cmsTag.CMSTagRules)
                                {
                                    string condition = "";
                                    if (cmsTagRule.condition.Equals("contains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("notContains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("equals"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " = " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("notEquals"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " != " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("minMaxRange"))
                                    {
                                        string[] range = cmsTagRule.criteria.Split(',');
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " BETWEEN " + range[0] + " AND " + range[1];
                                    }
                                    else if (cmsTagRule.condition.Equals("greaterThan"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " > " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("lessThan"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " < " + cmsTagRule.criteria;
                                    }

                                    // Adding AND Clause for RULES
                                    if (!string.IsNullOrEmpty(condition))
                                    {
                                        condition = " ( " + condition + " ) ";
                                        if (string.IsNullOrEmpty(rulesWhereCondition))
                                        {
                                            rulesWhereCondition = condition;
                                            condition = "";
                                        }
                                        else
                                        {
                                            rulesWhereCondition = rulesWhereCondition + " AND " + condition;
                                            condition = "";
                                        }
                                    }
                                }// End of Rules Iteration

                                // Adding OR Clause for TAGS
                                if (!string.IsNullOrEmpty(rulesWhereCondition))
                                {
                                    rulesWhereCondition = " ( " + rulesWhereCondition + " ) ";
                                    if (string.IsNullOrEmpty(tagsWhereCondition))
                                    {
                                        tagsWhereCondition = rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                    else
                                    {
                                        tagsWhereCondition = tagsWhereCondition + " OR " + rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                }
                            }
                        }// End of TAGS Iteration
                         // Adding AND Clause for ANSWERS
                        if (!string.IsNullOrEmpty(tagsWhereCondition))
                        {
                            if (string.IsNullOrEmpty(whereClause))
                            {
                                whereClause = "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                            else
                            {
                                whereClause = whereClause + " AND " + "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                        }
                    }
                }
            }// End of Answers Iteration

            if (!string.IsNullOrEmpty(whereClause))
            {
                if (!string.IsNullOrEmpty(filterQuery))
                {
                    query = query + " WHERE " + whereClause + " AND " + filterQuery + " AND ";
                }
                else
                {
                    query = query + " WHERE " + whereClause + " AND ";
                }
            }
            else if (!string.IsNullOrEmpty(filterQuery))
            {
                query = query + " WHERE " + filterQuery + " AND ";
            }
            else
            {
                query = query + " WHERE";
            }

            query = query + " country = '" + recommendation.country + "' AND language = '" + recommendation.language + "'";

            if (Constants.ACCESSORY_TYPE_ACCESSORY.Equals(accessoryType))
            {
                query = query + " AND psa.sku_pmoid=" + recommendation.pmoid + " AND " + productAccessoriesAlias + ".marketing_category_pmoid in(" + rm.GetString("Accessory_marketing_category_pmoid", CultureInfo.CurrentCulture) + ")";
            }
            else if (Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType))
            {
                query = query + " AND psa.sku_pmoid=" + recommendation.pmoid + " AND " + productAccessoriesAlias + ".marketing_category_pmoid in(" + rm.GetString("Display_marketing_category_pmoid", CultureInfo.CurrentCulture) + ")";
            }
            else if (Constants.ACCESSORY_TYPE_DOCK.Equals(accessoryType))
            {
                query = query + " AND psa.sku_pmoid=" + recommendation.pmoid + " AND " + productAccessoriesAlias + ".marketing_sub_category_pmoid in(" + rm.GetString("Dock_marketing_sub_category_pmoid", CultureInfo.CurrentCulture) + ")";
            }
            else if (Constants.ACCESSORY_TYPE_SERVICE.Equals(accessoryType))
            {
                query = query + " AND psa.sku_pmoid=" + recommendation.pmoid + " AND " + productAccessoriesAlias + ".marketing_category_pmoid in(" + rm.GetString("Service_marketing_category_pmoid", CultureInfo.CurrentCulture) + ")";
            }

            // Retrieving recommendations
            List<ProductAccessory> dbProductAccessoryList = recommendationRepository.GetAccessoriesAsync(query).Result;

            if (!Constants.ACCESSORY_TYPE_SERVICE.Equals(accessoryType))
            {
                dbProductAccessoryList = dbProductAccessoryList.Where(p => !string.IsNullOrEmpty(p.obsolete_date) && p.obsolete_date != "NULL" && DateTime.Parse(p.obsolete_date) >= DateTime.Now.Date).ToList();
            }

            //Logs.Info("The response of RecommendationService.GetRecommendedAccessories is, Response[List<ProductAccessory>, Size: " + ((dbProductAccessoryList == null) ? '-' : dbProductAccessoryList.Count) + "]");
            return dbProductAccessoryList;
        }

        public List<ProductDaasService> GetRecommendedDaasAccessories(List<CMSAnswer> dbCMSAnswers, List<CMSWeightedTag> weightedTagList, Recommendation recommendation, string filterQuery, string productAccessoriesAlias)
        {
            //Logs.Info("RecommendationService.GetRecommendedDaasAccessories method has been called having parameters[List<CMSAnswer> {Size: " + ((dbCMSAnswers == null) ? '-' : dbCMSAnswers.Count) + "}, List<CMSWeightedTag> {Size: " + ((weightedTagList == null) ? '-' : weightedTagList.Count) + "}, Recommendation, filterQuery: " + filterQuery + "]");
            ResourceManager rm = new ResourceManager("hpi_user_assessment_tool.UAT_resources", Assembly.GetExecutingAssembly());

            string query = "SELECT * FROM ProductDaasServices " + productAccessoriesAlias;
            String whereClause = "";

            foreach (CMSAnswer cmsAnswer in dbCMSAnswers)
            {
                List<CMSTagRule> cmsTagRuleList = new List<CMSTagRule>();
                //TODO: Define the appliedOnTable names mapping to the constant or config file and retrive from there to compare
                if (cmsAnswer != null)
                {
                    if (cmsAnswer.CMSWeightedTags != null)
                    {
                        // Adding WeightedTags to the list
                        foreach (CMSWeightedTag cmsWeightedTag in cmsAnswer.CMSWeightedTags)
                        {
                            // Add all the Tags except sku tags for daas accessories
                            if (cmsWeightedTag.CMSWeightedTagRules != null && cmsWeightedTag.appliedOnTable.Equals("ProductDaasServices") )
                            {
                                weightedTagList.Add(cmsWeightedTag);
                            }
                        }
                    }

                    if (cmsAnswer.CMSTags != null)
                    {
                        string tagsWhereCondition = "";
                        foreach (CMSTag cmsTag in cmsAnswer.CMSTags)
                        {
                            if (cmsTag.CMSTagRules != null && cmsTag.appliedOnTable.Equals("ProductDaasServices"))
                            {
                                String rulesWhereCondition = "";
                                foreach (CMSTagRule cmsTagRule in cmsTag.CMSTagRules)
                                {
                                    string condition = "";
                                    if (cmsTagRule.condition.Equals("contains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("notContains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("equals"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " = " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("notEquals"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " != " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("minMaxRange"))
                                    {
                                        string[] range = cmsTagRule.criteria.Split(',');
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " BETWEEN " + range[0] + " AND " + range[1];
                                    }
                                    else if (cmsTagRule.condition.Equals("greaterThan"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " > " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("lessThan"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " < " + cmsTagRule.criteria;
                                    }

                                    // Adding AND Clause for RULES
                                    if (!string.IsNullOrEmpty(condition))
                                    {
                                        condition = " ( " + condition + " ) ";
                                        if (string.IsNullOrEmpty(rulesWhereCondition))
                                        {
                                            rulesWhereCondition = condition;
                                            condition = "";
                                        }
                                        else
                                        {
                                            rulesWhereCondition = rulesWhereCondition + " AND " + condition;
                                            condition = "";
                                        }
                                    }
                                }// End of Rules Iteration

                                // Adding OR Clause for TAGS
                                if (!string.IsNullOrEmpty(rulesWhereCondition))
                                {
                                    rulesWhereCondition = " ( " + rulesWhereCondition + " ) ";
                                    if (string.IsNullOrEmpty(tagsWhereCondition))
                                    {
                                        tagsWhereCondition = rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                    else
                                    {
                                        tagsWhereCondition = tagsWhereCondition + " OR " + rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                }
                            }
                        }// End of TAGS Iteration
                         // Adding AND Clause for ANSWERS
                        if (!string.IsNullOrEmpty(tagsWhereCondition))
                        {
                            if (string.IsNullOrEmpty(whereClause))
                            {
                                whereClause = "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                            else
                            {
                                whereClause = whereClause + " AND " + "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                        }
                    }
                }
            }// End of Answers Iteration

            if (!string.IsNullOrEmpty(whereClause))
            {
                if (!string.IsNullOrEmpty(filterQuery))
                {
                    query = query + " WHERE " + whereClause + " AND " + filterQuery + " AND ";
                }
                else
                {
                    query = query + " WHERE " + whereClause + " AND ";
                }
            }
            else if (!string.IsNullOrEmpty(filterQuery))
            {
                query = query + " WHERE " + filterQuery + " AND ";
            }
            else
            {
                query = query + " WHERE";
            }

            query = query + " country = '" + recommendation.country + "' AND language = '" + recommendation.language + "'";

            // Retrieving recommendations
            List<ProductDaasService> dbProductAccessoryList = recommendationRepository.GetDaasServicesAsync(query).Result;

            //Logs.Info("The response of RecommendationService.GetRecommendedDaasAccessories is, Response[List<ProductDaasService>, Size: " + ((dbProductAccessoryList == null) ? '-' : dbProductAccessoryList.Count) + "]");
            return dbProductAccessoryList;
        }
        public string buildFilterQuery(List<CMSAnswer> filterDBCMSAnswers)
        {
            //Logs.Info("RecommendationService.buildFilterQuery method has been called having parameter[List of filterDBCMSAnswers, Size: " + ((filterDBCMSAnswers == null) ? '-' : filterDBCMSAnswers.Count) + "]");
            filterDBCMSAnswers = filterDBCMSAnswers.OrderBy(p => p.CMSQuestionId).ToList();

            string query = "";
            String whereClause = "";
            int lastQuestionId = 0;

            foreach (CMSAnswer cmsAnswer in filterDBCMSAnswers)
            {
                List<CMSTagRule> cmsTagRuleList = new List<CMSTagRule>();
                if (cmsAnswer != null)
                {
                    if (cmsAnswer.CMSTags != null)
                    {
                        string tagsWhereCondition = "";
                        foreach (CMSTag cmsTag in cmsAnswer.CMSTags)
                        {
                            if (cmsTag.CMSTagRules != null && cmsTag.appliedOnTable.Equals("ProductSkus") && Constants.APPLIED_ON_ENTITY_DEVICE.Equals(cmsTag.appliedOnEntity))
                            {
                                String rulesWhereCondition = "";
                                foreach (CMSTagRule cmsTagRule in cmsTag.CMSTagRules)
                                {
                                    string condition = "";
                                    if (cmsTagRule.condition.Equals("contains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("notContains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("equals"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " = " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("notEquals"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " != " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("minMaxRange"))
                                    {
                                        string[] range = cmsTagRule.criteria.Split(',');
                                        condition = cmsTagRule.appliedOnColumn + " BETWEEN " + range[0] + " AND " + range[1];
                                    }
                                    else if (cmsTagRule.condition.Equals("greaterThan"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " > " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("lessThan"))
                                    {
                                        condition = cmsTagRule.appliedOnColumn + " < " + cmsTagRule.criteria;
                                    }

                                    // Adding AND Clause for RULES
                                    if (!string.IsNullOrEmpty(condition))
                                    {
                                        condition = " ( " + condition + " ) ";
                                        if (string.IsNullOrEmpty(rulesWhereCondition))
                                        {
                                            rulesWhereCondition = condition;
                                            condition = "";
                                        }
                                        else
                                        {
                                            rulesWhereCondition = rulesWhereCondition + " AND " + condition;
                                            condition = "";
                                        }
                                    }
                                }// End of Rules Iteration

                                // Adding OR Clause for TAGS
                                if (!string.IsNullOrEmpty(rulesWhereCondition))
                                {
                                    rulesWhereCondition = " ( " + rulesWhereCondition + " ) ";
                                    if (string.IsNullOrEmpty(tagsWhereCondition))
                                    {
                                        tagsWhereCondition = rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                    else
                                    {
                                        tagsWhereCondition = tagsWhereCondition + " OR " + rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                }
                            }
                        }// End of TAGS Iteration

                        // Adding OR Clause for ANSWERS
                        if (!string.IsNullOrEmpty(tagsWhereCondition))
                        {
                            // If if first block
                            if (string.IsNullOrEmpty(whereClause))
                            {
                                lastQuestionId = cmsAnswer.CMSQuestionId;
                                whereClause = "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                            else
                            {
                                if (lastQuestionId == cmsAnswer.CMSQuestionId)
                                {
                                    whereClause = whereClause + " OR " + "(" + tagsWhereCondition + ")";
                                    tagsWhereCondition = "";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(whereClause))
                                    {
                                        whereClause = " ( " + whereClause + " ) ";
                                        if (string.IsNullOrEmpty(query))
                                        {
                                            query = whereClause;
                                            whereClause = "";
                                        }
                                        else
                                        {
                                            query = query + " AND " + whereClause;
                                            whereClause = "";
                                        }
                                    }
                                    whereClause = "(" + tagsWhereCondition + ")";
                                    tagsWhereCondition = "";
                                    lastQuestionId = cmsAnswer.CMSQuestionId;
                                }
                            }
                        }
                    }
                }
            }// End of Answers Iteration

            if (!string.IsNullOrEmpty(whereClause))
            {
                if (string.IsNullOrEmpty(query))
                {
                    query = whereClause;
                    whereClause = "";
                }
                else
                {
                    query = query + " AND " + whereClause;
                    whereClause = "";
                }
            }
            //Logs.Info("The response of RecommendationService.buildFilterQuery method is query:" + query);
            return query;
        }

        public string buildFilterQueryForAccessories(List<CMSAnswer> filterDBCMSAnswers, string productAccessoriesAlias)
        {
            //Logs.Info("RecommendationService.buildFilterQueryForAccessories method has been called having parameter[List of filterDBCMSAnswers, Size: " + ((filterDBCMSAnswers == null) ? '-' : filterDBCMSAnswers.Count) + "]");
            filterDBCMSAnswers = filterDBCMSAnswers.OrderBy(p => p.CMSQuestionId).ToList();

            string query = "";
            String whereClause = "";
            int lastQuestionId = 0;

            foreach (CMSAnswer cmsAnswer in filterDBCMSAnswers)
            {
                List<CMSTagRule> cmsTagRuleList = new List<CMSTagRule>();
                if (cmsAnswer != null)
                {
                    if (cmsAnswer.CMSTags != null)
                    {
                        string tagsWhereCondition = "";
                        foreach (CMSTag cmsTag in cmsAnswer.CMSTags)
                        {
                            if (cmsTag.CMSTagRules != null && cmsTag.appliedOnTable.Equals("ProductAccessories") && (Constants.APPLIED_ON_ENTITY_ALLACCESSORIES.Equals(cmsTag.appliedOnEntity) || Constants.APPLIED_ON_ENTITY_DOCK.Equals(cmsTag.appliedOnEntity) || Constants.APPLIED_ON_ENTITY_DISPLAY.Equals(cmsTag.appliedOnEntity) || Constants.APPLIED_ON_ENTITY_ACCESSORY.Equals(cmsTag.appliedOnEntity) || Constants.APPLIED_ON_ENTITY_SERVICE.Equals(cmsTag.appliedOnEntity)))
                            {
                                String rulesWhereCondition = "";
                                foreach (CMSTagRule cmsTagRule in cmsTag.CMSTagRules)
                                {
                                    string condition = "";
                                    if (cmsTagRule.condition.Equals("contains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("notContains"))
                                    {
                                        string[] containsArr = cmsTagRule.criteria.Replace("'", "").Split(',');
                                        foreach (string containsValue in containsArr)
                                        {
                                            if (string.IsNullOrEmpty(condition))
                                            {
                                                condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                            else
                                            {
                                                condition = condition + " OR " + productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " NOT LIKE '%" + containsValue + "%'";
                                            }
                                        }
                                    }
                                    else if (cmsTagRule.condition.Equals("equals"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " = " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("notEquals"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " != " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("minMaxRange"))
                                    {
                                        string[] range = cmsTagRule.criteria.Split(',');
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " BETWEEN " + range[0] + " AND " + range[1];
                                    }
                                    else if (cmsTagRule.condition.Equals("greaterThan"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " > " + cmsTagRule.criteria;
                                    }
                                    else if (cmsTagRule.condition.Equals("lessThan"))
                                    {
                                        condition = productAccessoriesAlias + "." + cmsTagRule.appliedOnColumn + " < " + cmsTagRule.criteria;
                                    }

                                    // Adding AND Clause for RULES
                                    if (!string.IsNullOrEmpty(condition))
                                    {
                                        condition = " ( " + condition + " ) ";
                                        if (string.IsNullOrEmpty(rulesWhereCondition))
                                        {
                                            rulesWhereCondition = condition;
                                            condition = "";
                                        }
                                        else
                                        {
                                            rulesWhereCondition = rulesWhereCondition + " AND " + condition;
                                            condition = "";
                                        }
                                    }
                                }// End of Rules Iteration

                                // Adding OR Clause for TAGS
                                if (!string.IsNullOrEmpty(rulesWhereCondition))
                                {
                                    rulesWhereCondition = " ( " + rulesWhereCondition + " ) ";
                                    if (string.IsNullOrEmpty(tagsWhereCondition))
                                    {
                                        tagsWhereCondition = rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                    else
                                    {
                                        tagsWhereCondition = tagsWhereCondition + " OR " + rulesWhereCondition;
                                        rulesWhereCondition = "";
                                    }
                                }
                            }
                        }// End of TAGS Iteration

                        // Adding OR Clause for ANSWERS
                        if (!string.IsNullOrEmpty(tagsWhereCondition))
                        {
                            // If if first block
                            if (string.IsNullOrEmpty(whereClause))
                            {
                                lastQuestionId = cmsAnswer.CMSQuestionId;
                                whereClause = "(" + tagsWhereCondition + ")";
                                tagsWhereCondition = "";
                            }
                            else
                            {
                                if (lastQuestionId == cmsAnswer.CMSQuestionId)
                                {
                                    whereClause = whereClause + " OR " + "(" + tagsWhereCondition + ")";
                                    tagsWhereCondition = "";
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(whereClause))
                                    {
                                        whereClause = " ( " + whereClause + " ) ";
                                        if (string.IsNullOrEmpty(query))
                                        {
                                            query = whereClause;
                                            whereClause = "";
                                        }
                                        else
                                        {
                                            query = query + " AND " + whereClause;
                                            whereClause = "";
                                        }
                                    }
                                    whereClause = "(" + tagsWhereCondition + ")";
                                    tagsWhereCondition = "";
                                    lastQuestionId = cmsAnswer.CMSQuestionId;
                                }
                            }
                        }
                    }
                }
            }// End of Answers Iteration

            if (!string.IsNullOrEmpty(whereClause))
            {
                if (string.IsNullOrEmpty(query))
                {
                    query = whereClause;
                    whereClause = "";
                }
                else
                {
                    query = query + " AND " + whereClause;
                    whereClause = "";
                }
            }
            //Logs.Info("The response of RecommendationService.buildFilterQueryForAccessories method is query:" + query);
            return query;
        }

        public void includeStandaloneTags(List<CMSAnswer> dbCMSAnswers, List<CMSWeightedTag> weightedTagList, string entityType, string accessoryType)
        {
            List<CMSStandaloneTag> cmsStandaloneTagList = recommendationRepository.GetStandaloneFilterTagsAsync().Result;
            List<CMSStandaloneWeightedTag> cmsStandaloneWeightedTagList = recommendationRepository.GetStandaloneWeightedTagsAsync().Result;
            List<string> usrSelectedAnswersLables = dbCMSAnswers.Select(p => p.label).ToList();
            if (cmsStandaloneTagList != null && cmsStandaloneTagList.Count > 0)
            {
                foreach (CMSStandaloneTag cmsStandaloneTag in cmsStandaloneTagList)
                {
                    if (cmsStandaloneTag.CMSStandaloneTagSelectedAnswers != null && cmsStandaloneTag.CMSStandaloneTagSelectedAnswers.Count > 0)
                    {
                        List<string> dbSelectedAnswersLables = cmsStandaloneTag.CMSStandaloneTagSelectedAnswers.Select(p => p.answerLabel).ToList();
                        var hasMatch = dbSelectedAnswersLables.Intersect(usrSelectedAnswersLables).Count() == dbSelectedAnswersLables.Count();
                        if (!hasMatch)
                        {
                            continue;
                        }
                    }

                    if (cmsStandaloneTag.CMSStandaloneTagNotSelectedAnswers != null && cmsStandaloneTag.CMSStandaloneTagNotSelectedAnswers.Count > 0)
                    {
                        List<string> dbNotSelectedAnswersLables = cmsStandaloneTag.CMSStandaloneTagNotSelectedAnswers.Select(p => p.answerLabel).ToList();
                        bool hasMatch = usrSelectedAnswersLables.Intersect(dbNotSelectedAnswersLables).Any();
                        if (hasMatch)
                        {
                            continue;
                        }
                    }

                    CMSAnswer cmsAnswer = new CMSAnswer();
                    List<CMSTag> cmsTags = new List<CMSTag>();
                    CMSTag cmsTag = new CMSTag();

                    CopyProperties.Copy(cmsStandaloneTag, cmsTag);
                    List<CMSTagRule> cmsTagRules = new List<CMSTagRule>();
                    if (cmsStandaloneTag.CMSStandaloneTagRules != null && cmsStandaloneTag.CMSStandaloneTagRules.Count > 0)
                    {
                        foreach (CMSStandaloneTagRule cmsStandaloneTagRule in cmsStandaloneTag.CMSStandaloneTagRules)
                        {
                            CMSTagRule cmsTagRule = new CMSTagRule();
                            CopyProperties.Copy(cmsStandaloneTagRule, cmsTagRule);
                            cmsTagRules.Add(cmsTagRule);
                        }
                    }

                    cmsTag.CMSTagRules = cmsTagRules;
                    cmsTags.Add(cmsTag);
                    cmsAnswer.CMSTags = cmsTags;
                    dbCMSAnswers.Add(cmsAnswer);
                }

            }

            if (cmsStandaloneWeightedTagList != null && cmsStandaloneWeightedTagList.Count > 0)
            {
                foreach (CMSStandaloneWeightedTag cmsStandaloneWeightedTag in cmsStandaloneWeightedTagList)
                {
                    if (cmsStandaloneWeightedTag.CMSStandaloneWeightedTagSelectedAnswers != null && cmsStandaloneWeightedTag.CMSStandaloneWeightedTagSelectedAnswers.Count > 0)
                    {
                        List<string> dbSelectedAnswersLables = cmsStandaloneWeightedTag.CMSStandaloneWeightedTagSelectedAnswers.Select(p => p.answerLabel).ToList();
                        var hasMatch = dbSelectedAnswersLables.Intersect(usrSelectedAnswersLables).Count() == dbSelectedAnswersLables.Count();
                        if (!hasMatch)
                        {
                            continue;
                        }
                    }

                    if (cmsStandaloneWeightedTag.CMSStandaloneWeightedTagNotSelectedAnswers != null && cmsStandaloneWeightedTag.CMSStandaloneWeightedTagNotSelectedAnswers.Count > 0)
                    {
                        List<string> dbNotSelectedAnswersLables = cmsStandaloneWeightedTag.CMSStandaloneWeightedTagNotSelectedAnswers.Select(p => p.answerLabel).ToList();
                        bool hasMatch = usrSelectedAnswersLables.Intersect(dbNotSelectedAnswersLables).Any();
                        if (hasMatch)
                        {
                            continue;
                        }
                    }

                    bool isApplicable = false;
                    if (Constants.ENTITY_TYPE_SKU.Equals(entityType) && cmsStandaloneWeightedTag.CMSStandaloneWeightedTagRules != null && !cmsStandaloneWeightedTag.appliedOnTable.Equals("ProductAccessories") && Constants.APPLIED_ON_ENTITY_DEVICE.Equals(cmsStandaloneWeightedTag.appliedOnEntity))
                    {
                        isApplicable = true;
                    }
                    else if (Constants.ENTITY_TYPE_ACCESSORY.Equals(entityType) && cmsStandaloneWeightedTag.CMSStandaloneWeightedTagRules != null && !cmsStandaloneWeightedTag.appliedOnTable.Equals("ProductSkus") && (Constants.APPLIED_ON_ENTITY_ALLACCESSORIES.Equals(cmsStandaloneWeightedTag.appliedOnEntity) || accessoryType.Equals(cmsStandaloneWeightedTag.appliedOnEntity)))
                    {
                        isApplicable = true;
                    }
                    else if(Constants.ENTITY_TYPE_DAAS_SERVICE.Equals(entityType) && cmsStandaloneWeightedTag.CMSStandaloneWeightedTagRules != null && !cmsStandaloneWeightedTag.appliedOnTable.Equals("ProductDaasServices") && Constants.APPLIED_ON_ENTITY_DAAS_SERVICE.Equals(cmsStandaloneWeightedTag.appliedOnEntity))
                    {
                        isApplicable = true;
                    }

                    if (isApplicable)
                    {
                        CMSWeightedTag cmsWeightedTag = new CMSWeightedTag();

                        CopyProperties.Copy(cmsStandaloneWeightedTag, cmsWeightedTag);
                        List<CMSWeightedTagRule> cmsWeightedTagRules = new List<CMSWeightedTagRule>();
                        if (cmsStandaloneWeightedTag.CMSStandaloneWeightedTagRules != null && cmsStandaloneWeightedTag.CMSStandaloneWeightedTagRules.Count > 0)
                        {
                            foreach (CMSStandaloneWeightedTagRule cmsStandaloneWeightedTagRule in cmsStandaloneWeightedTag.CMSStandaloneWeightedTagRules)
                            {
                                CMSWeightedTagRule cmsWeightedTagRule = new CMSWeightedTagRule();
                                CopyProperties.Copy(cmsStandaloneWeightedTagRule, cmsWeightedTagRule);
                                cmsWeightedTagRules.Add(cmsWeightedTagRule);
                            }
                        }

                        cmsWeightedTag.CMSWeightedTagRules = cmsWeightedTagRules;
                        weightedTagList.Add(cmsWeightedTag);
                    }
                }
            }
        }

        public void includeReliantStandaloneTags(Recommendation recommendation, List<CMSAnswer> dbCMSAnswers, List<CMSWeightedTag> weightedTagList, string entityType, string accessoryType)
        {
            List<CMSReliantStandaloneTag> cmsReliantStandaloneTagList = recommendationRepository.GetReliantStandaloneFilterTagsAsync().Result;
            List<CMSReliantStandaloneWeightedTag> cmsReliantStandaloneWeightedTagList = recommendationRepository.GetReliantStandaloneWeightedTagsAsync().Result;
            ProductSku productSku = recommendationRepository.GetProductSkuByPmoid(recommendation.pmoid.ToString()).Result;

            if (productSku != null && cmsReliantStandaloneTagList != null && cmsReliantStandaloneTagList.Count > 0)
            {
                foreach (CMSReliantStandaloneTag cmsReliantStandaloneTag in cmsReliantStandaloneTagList)
                {
                    if (validateSelectedProductForReliantTags(productSku, cmsReliantStandaloneTag))
                    {
                        CMSAnswer cmsAnswer = new CMSAnswer();
                        List<CMSTag> cmsTags = new List<CMSTag>();
                        CMSTag cmsTag = new CMSTag();

                        CopyProperties.Copy(cmsReliantStandaloneTag, cmsTag);
                        List<CMSTagRule> cmsTagRules = new List<CMSTagRule>();
                        if (cmsReliantStandaloneTag.CMSReliantStandaloneTagRules != null && cmsReliantStandaloneTag.CMSReliantStandaloneTagRules.Count > 0)
                        {
                            foreach (CMSReliantStandaloneTagRule cmsReliantStandaloneTagRule in cmsReliantStandaloneTag.CMSReliantStandaloneTagRules)
                            {
                                CMSTagRule cmsTagRule = new CMSTagRule();
                                CopyProperties.Copy(cmsReliantStandaloneTagRule, cmsTagRule);
                                cmsTagRules.Add(cmsTagRule);
                            }
                        }

                        cmsTag.CMSTagRules = cmsTagRules;
                        cmsTags.Add(cmsTag);
                        cmsAnswer.CMSTags = cmsTags;
                        dbCMSAnswers.Add(cmsAnswer);
                    }
                }
            }

            if (productSku != null && cmsReliantStandaloneWeightedTagList != null && cmsReliantStandaloneWeightedTagList.Count > 0)
            {
                foreach (CMSReliantStandaloneWeightedTag cmsReliantStandaloneWeightedTag in cmsReliantStandaloneWeightedTagList)
                {
                    bool isApplicable = false;
                    if (Constants.ENTITY_TYPE_SKU.Equals(entityType) && cmsReliantStandaloneWeightedTag.CMSReliantStandaloneWeightedTagRules != null && !cmsReliantStandaloneWeightedTag.appliedOnTable.Equals("ProductAccessories") && Constants.APPLIED_ON_ENTITY_DEVICE.Equals(cmsReliantStandaloneWeightedTag.appliedOnEntity))
                    {
                        isApplicable = true;
                    }
                    else if (Constants.ENTITY_TYPE_ACCESSORY.Equals(entityType) && cmsReliantStandaloneWeightedTag.CMSReliantStandaloneWeightedTagRules != null && !cmsReliantStandaloneWeightedTag.appliedOnTable.Equals("ProductSkus") && (Constants.APPLIED_ON_ENTITY_ALLACCESSORIES.Equals(cmsReliantStandaloneWeightedTag.appliedOnEntity) || accessoryType.Equals(cmsReliantStandaloneWeightedTag.appliedOnEntity)))
                    {
                        isApplicable = true;
                    }
                    else if (Constants.ENTITY_TYPE_DAAS_SERVICE.Equals(entityType) && cmsReliantStandaloneWeightedTag.CMSReliantStandaloneWeightedTagRules != null && !cmsReliantStandaloneWeightedTag.appliedOnTable.Equals("ProductDaasServices") && Constants.APPLIED_ON_ENTITY_DAAS_SERVICE.Equals(cmsReliantStandaloneWeightedTag.appliedOnEntity))
                    {
                        isApplicable = true;
                    }

                    if (isApplicable)
                    {
                        if (validateSelectedProductForReliantTags(productSku, cmsReliantStandaloneWeightedTag))
                        {
                            CMSWeightedTag cmsWeightedTag = new CMSWeightedTag();

                            CopyProperties.Copy(cmsReliantStandaloneWeightedTag, cmsWeightedTag);
                            List<CMSWeightedTagRule> cmsWeightedTagRules = new List<CMSWeightedTagRule>();
                            if (cmsReliantStandaloneWeightedTag.CMSReliantStandaloneWeightedTagRules != null && cmsReliantStandaloneWeightedTag.CMSReliantStandaloneWeightedTagRules.Count > 0)
                            {
                                foreach (CMSReliantStandaloneWeightedTagRule cmsReliantStandaloneWeightedTagRule in cmsReliantStandaloneWeightedTag.CMSReliantStandaloneWeightedTagRules)
                                {
                                    CMSWeightedTagRule cmsWeightedTagRule = new CMSWeightedTagRule();
                                    CopyProperties.Copy(cmsReliantStandaloneWeightedTagRule, cmsWeightedTagRule);
                                    cmsWeightedTagRules.Add(cmsWeightedTagRule);
                                }
                            }

                            cmsWeightedTag.CMSWeightedTagRules = cmsWeightedTagRules;
                            weightedTagList.Add(cmsWeightedTag);
                        }
                    }
                }
            }
        }

        public bool validateSelectedProductForReliantTags(ProductSku productSku, Object cmsReliantStandaloneTag)
        {
            string deviceAppliedOnTable = (string)cmsReliantStandaloneTag.GetType().GetProperty("deviceAppliedOnTable").GetValue(cmsReliantStandaloneTag, null);
            string deviceAppliedOnColumn = (string)cmsReliantStandaloneTag.GetType().GetProperty("deviceAppliedOnColumn").GetValue(cmsReliantStandaloneTag, null);
            string condition = (string)cmsReliantStandaloneTag.GetType().GetProperty("deviceCondition").GetValue(cmsReliantStandaloneTag, null);
            string criteria = (string)cmsReliantStandaloneTag.GetType().GetProperty("deviceCriteria").GetValue(cmsReliantStandaloneTag, null);
            criteria = criteria.Replace("'", "");

            string value = (string)productSku.GetType().GetProperty(deviceAppliedOnColumn).GetValue(productSku, null);
            bool isMatched = false;

            if (condition.Equals("contains"))
            {
                string[] criteriaArr = criteria.Split(',');
                foreach (string containsValue in criteriaArr)
                {
                    if (!string.IsNullOrEmpty(value) && value.Contains(containsValue))
                    {
                        isMatched = true;
                    }
                }
            }
            else if (condition.Equals("notContains"))
            {
                string[] criteriaArr = criteria.Split(',');
                foreach (string containsValue in criteriaArr)
                {
                    if (!string.IsNullOrEmpty(value) && !value.Contains(containsValue))
                    {
                        isMatched = true;
                    }
                }
            }
            else if (condition.Equals("equals"))
            {
                if (criteria.Equals(value))
                {
                    isMatched = true;
                }
            }
            else if (condition.Equals("notEquals"))
            {
                if (!criteria.Equals(value))
                {
                    isMatched = true;
                }
            }
            else if (condition.Equals("minMaxRange"))
            {
                string[] criteriaList = criteria.Split(',');

                if (string.IsNullOrEmpty(value))
                {
                    value = "0";
                }

                if (float.Parse(value) >= float.Parse(criteriaList[0]) && float.Parse(value) <= float.Parse(criteriaList[1]))
                {
                    isMatched = true;
                }
            }
            else if (condition.Equals("greaterThan"))
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = "0";
                }

                if ((float.Parse(value) > float.Parse(criteria)))
                {
                    isMatched = true;
                }
            }
            else if (condition.Equals("lessThan"))
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = "0";
                }

                if ((float.Parse(value) < float.Parse(criteria)))
                {
                    isMatched = true;
                }
            }
            else if (condition.Equals("daysBetween") && !string.IsNullOrEmpty(value) && !value.Equals("NULL"))
            {
                string[] criteriaList = criteria.Split(',');

                DateTime valueDateTime = Convert.ToDateTime(value);
                DateTime todayDateTime = DateTime.Now;
                double numberOfDays = (todayDateTime - valueDateTime).TotalDays;

                if (numberOfDays >= double.Parse(criteriaList[0]) && numberOfDays <= double.Parse(criteriaList[1]))
                {
                    isMatched = true;
                }
            }

            return isMatched;
        }
    }

    public interface IRecommendationService
    {
        RecommendationProductSkuDTOResponse GetRecommendedSkusAsync(Recommendation recommendation);
        RecommendationProductAccessoryDTOResponse GetRecommendedAccessoriesAsync(Recommendation recommendation, string accessoryType);
        RecommendationProductDaasAccessoryDTOResponse GetRecommendedDaasServicesAsync(Recommendation recommendation);
        List<int> GetAnswerByLabelsAsync(List<string> answerLabels);
    }
}
