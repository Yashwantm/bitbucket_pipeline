﻿using hpi_user_assessment_tool.Data;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using hpi_user_assessment_tool.Utility;

namespace hpi_user_assessment_tool.Services
{
    public class ValidationService : IValidationService
    {
        private IValidationRepository validationRepository;
        private IRecommendationService recommendationService;
        public ValidationService(IValidationRepository validationRepository, IRecommendationService recommendationService)
        {
            this.validationRepository = validationRepository;
            this.recommendationService = recommendationService;
        }

        public List<AnswerTagsDTO> GetAllTags()
        {
            Logs.Info("ValidationService.GetAllTags method has been called. ");
            List<CMSAnswer> cmsAnswerList = validationRepository.GetAllAnswersAsync().Result;
            List<AnswerTagsDTO> answerTagDTOList = new List<AnswerTagsDTO>();
            foreach (CMSAnswer cmsAnswer in cmsAnswerList)
            {
                if (cmsAnswer.CMSTags.ToList().Count == 0 && cmsAnswer.CMSWeightedTags.ToList().Count == 0)
                {
                    continue;
                }
                AnswerTagsDTO answerTagsDTO = new AnswerTagsDTO();
                answerTagsDTO.answerId = cmsAnswer.Id;
                answerTagsDTO.answerLabel = cmsAnswer.label;
                answerTagsDTO.answerDescription = cmsAnswer.description;
                answerTagsDTO.filterTagList = cmsAnswer.CMSTags.ToList();
                answerTagsDTO.weightedTagList = cmsAnswer.CMSWeightedTags.ToList();
                answerTagDTOList.Add(answerTagsDTO);
            }
            Logs.InfoFormat("The response of ValidationService.GetAllTags method is, Response [List<AnswerTagsDTO>, Size: " + ((answerTagDTOList == null) ? '-' : answerTagDTOList.Count) + "]");
            return answerTagDTOList;
        }
        public List<AnswerTagsDTO> GetTagsByAnswersId(List<int> answerIds)
        {
            Logs.Info("ValidationService.GetAllTags method has been called having parameters [List<int> answerIds, Size: " + ((answerIds == null) ? '-' : answerIds.Count) + "]");
            List<CMSAnswer> cmsAnswerList = validationRepository.GetAnswerByAnswersIdAsync(answerIds).Result;
            List<AnswerTagsDTO> answerTagDTOList = new List<AnswerTagsDTO>();
            foreach (CMSAnswer cmsAnswer in cmsAnswerList)
            {
                AnswerTagsDTO answerTagsDTO = new AnswerTagsDTO();
                answerTagsDTO.answerId = cmsAnswer.Id;
                answerTagsDTO.answerLabel = cmsAnswer.label;
                answerTagsDTO.answerDescription = cmsAnswer.description;
                answerTagsDTO.filterTagList = cmsAnswer.CMSTags.ToList();
                answerTagsDTO.weightedTagList = cmsAnswer.CMSWeightedTags.ToList();
                answerTagDTOList.Add(answerTagsDTO);
            }
            Logs.InfoFormat("The response of ValidationService.GetAllTags method is, Response [List<AnswerTagsDTO>, Size: " + ((answerTagDTOList == null) ? '-' : answerTagDTOList.Count) + "]");
            return answerTagDTOList;
        }

        public Task<List<CMSTag>> GetFilterTagsByIdAsync(List<int> filterTagIds)
        {
            Logs.Info("ValidationService.GetFilterTagsByIdAsync method has been called having parameters [List<int> filterTagIds, Size: " + ((filterTagIds == null) ? '-' : filterTagIds.Count) + "]");
            return validationRepository.GetFilterTagsByIdAsync(filterTagIds);
        }
        public Task<List<CMSWeightedTag>> GetWeightedTagsByIdAsync(List<int> weightedTagIds)
        {
            Logs.Info("ValidationService.GetWeightedTagsByIdAsync method has been called having parameters [List<int> weightedTagIds, Size: " + ((weightedTagIds == null) ? '-' : weightedTagIds.Count) + "]");
            return validationRepository.GetWeightedTagsByIdAsync(weightedTagIds);
        }


        public List<ProductSkuScenarioResponseDTO> GetRecommendedSkusScenarios(RecommendationScenarios recommendationScenarios)
        {
            Logs.Info("ValidationService.GetRecommendedSkusScenarios service has been called having  parameters [recommendationScenarios]");
            if (string.IsNullOrEmpty(recommendationScenarios.country))
            {
                recommendationScenarios.country = Constants.DEFAULT_COUNTRY;
            }
            if (string.IsNullOrEmpty(recommendationScenarios.language))
            {
                recommendationScenarios.language = Constants.DEFAULT_LANGUAGE;
            }

            List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = new List<ProductSkuScenarioResponseDTO>();
            if (recommendationScenarios.scenarios != null)
            {
                // Iterating each scenario
                foreach (Scenario scenario in recommendationScenarios.scenarios)
                {
                    ProductSkuScenarioResponseDTO productSkuScenarioResponseDTO = new ProductSkuScenarioResponseDTO();
                    productSkuScenarioResponseDTO.id = scenario.id;
                    productSkuScenarioResponseDTO.answers = scenario.answers;

                    if (scenario.answers != null && scenario.answers.Count > 0)
                    {
                        // Getting answerIds by passing answerIds
                        List<int> answerIds = recommendationService.GetAnswerByLabelsAsync(scenario.answers);

                        // Creating recommendation instance
                        Recommendation recommendation = new Recommendation();
                        recommendation.debug = true;
                        recommendation.answerIds = answerIds;
                        recommendation.countOnly = false;
                        recommendation.country = recommendationScenarios.country;
                        recommendation.language = recommendationScenarios.language;
                        recommendation.limit = recommendationScenarios.limit;
                        recommendation.offSet = recommendationScenarios.offSet;

                        // Getting the RecommendedSkus
                        List<ProductSkuDTO> productSkuList = recommendationService.GetRecommendedSkusAsync(recommendation).recommendations;
                        if (productSkuList != null && productSkuList.Count > 0)
                        {
                            List<ProductSkuScenarioDTO> productSkuScenarioDTODTOList = new List<ProductSkuScenarioDTO>();
                            // Copying properties into response dto
                            foreach (ProductSkuDTO productSkuDTOObj in productSkuList)
                            {
                                ProductSkuScenarioDTO productSkuScenarioDTO = new ProductSkuScenarioDTO();
                                CopyProperties.Copy(productSkuDTOObj, productSkuScenarioDTO);
                                productSkuScenarioDTODTOList.Add(productSkuScenarioDTO);
                            }
                            productSkuScenarioResponseDTO.recommendations = productSkuScenarioDTODTOList;
                        }
                    }
                    productSkuScenarioResponseDTOList.Add(productSkuScenarioResponseDTO);
                }
            }
            Logs.Info("The response of ValidationService.GetRecommendedSkusScenarios method is, Response [List<ProductSkuScenarioResponseDTO> , Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
            return productSkuScenarioResponseDTOList;
        }

        public List<ProductSkuScenarioResponseDTO> GetRecommendedAccessoriesScenarios(RecommendationScenarios recommendationScenarios, string accessoryType)
        {
            Logs.Info("ValidationService.GetRecommendedAccessoriesScenarios service has been called having parameters [recommendationScenarios, accessoryType: " + accessoryType + "]");
            if (string.IsNullOrEmpty(recommendationScenarios.country))
            {
                recommendationScenarios.country = Constants.DEFAULT_COUNTRY;
            }
            if (string.IsNullOrEmpty(recommendationScenarios.language))
            {
                recommendationScenarios.language = Constants.DEFAULT_LANGUAGE;
            }

            List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = new List<ProductSkuScenarioResponseDTO>();
            if (recommendationScenarios.scenarios != null)
            {
                // Iterating each scenario
                foreach (Scenario scenario in recommendationScenarios.scenarios)
                {
                    ProductSkuScenarioResponseDTO productSkuScenarioResponseDTO = new ProductSkuScenarioResponseDTO();
                    productSkuScenarioResponseDTO.id = scenario.id;
                    productSkuScenarioResponseDTO.answers = scenario.answers;

                    if (scenario.answers != null && scenario.answers.Count > 0 && !string.IsNullOrEmpty(scenario.pmoid))
                    {
                        // Getting answerIds by passing answerIds
                        List<int> answerIds = recommendationService.GetAnswerByLabelsAsync(scenario.answers);

                        // Creating recommendation instance
                        Recommendation recommendation = new Recommendation();
                        recommendation.debug = true;
                        recommendation.answerIds = answerIds;
                        recommendation.countOnly = false;
                        recommendation.country = recommendationScenarios.country;
                        recommendation.language = recommendationScenarios.language;
                        recommendation.limit = recommendationScenarios.limit;
                        recommendation.offSet = recommendationScenarios.offSet;
                        recommendation.pmoid = int.Parse(scenario.pmoid);

                        // Getting the RecommendedAccessories
                        List<ProductAccessoryDTO> productAccessoryList = recommendationService.GetRecommendedAccessoriesAsync(recommendation, accessoryType).recommendations;

                        // List <ProductSkuDTO> productSkuList = recommendationService.GetRecommendedSkusAsync(recommendation);
                        if (productAccessoryList != null && productAccessoryList.Count > 0)
                        {
                            List<ProductSkuScenarioDTO> productSkuScenarioDTODTOList = new List<ProductSkuScenarioDTO>();
                            // Copying properties into response dto
                            foreach (ProductAccessoryDTO productAccessoryDTO in productAccessoryList)
                            {
                                ProductSkuScenarioDTO productSkuScenarioDTO = new ProductSkuScenarioDTO();
                                CopyProperties.Copy(productAccessoryDTO, productSkuScenarioDTO);
                                productSkuScenarioDTODTOList.Add(productSkuScenarioDTO);
                            }
                            productSkuScenarioResponseDTO.recommendations = productSkuScenarioDTODTOList;
                        }
                    }
                    productSkuScenarioResponseDTOList.Add(productSkuScenarioResponseDTO);
                }
            }
            Logs.InfoFormat("The response of ValidationService.GetRecommendedAccessoriesScenarios method is, Response [List<ProductSkuScenarioResponseDTO>, Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
            return productSkuScenarioResponseDTOList;
        }
    }

    public interface IValidationService
    {
        List<AnswerTagsDTO> GetAllTags();
        List<AnswerTagsDTO> GetTagsByAnswersId(List<int> answerIds);
        Task<List<CMSTag>> GetFilterTagsByIdAsync(List<int> filterTagIds);
        Task<List<CMSWeightedTag>> GetWeightedTagsByIdAsync(List<int> weightedTagIds);
        List<ProductSkuScenarioResponseDTO> GetRecommendedSkusScenarios(RecommendationScenarios recommendationScenarios);
        List<ProductSkuScenarioResponseDTO> GetRecommendedAccessoriesScenarios(RecommendationScenarios recommendationScenarios, string accessoryType);

    }
}