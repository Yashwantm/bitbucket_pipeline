﻿using hpi_user_assessment_tool.Data;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Models.Response;
using hpi_user_assessment_tool.Utility;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace hpi_user_assessment_tool.Services
{
    public class CustomerProfileService : ICustomerProfileService
    {
        private ICustomerProfileRepository customerProfilesRepository;
        private IRecommendationRepository recommendationRepository;

        public CustomerProfileService(ICustomerProfileRepository customerProfilesRepository)
        {
            this.customerProfilesRepository = customerProfilesRepository;
        }

        public CustomerProfileService(ICustomerProfileRepository customerProfilesRepository, IRecommendationRepository recommendationRepository)
        {
            this.customerProfilesRepository = customerProfilesRepository;
            this.recommendationRepository = recommendationRepository;
        }

        public async Task<AddCustomerResponse> AddCustomer(USRInfo usrInfo, USRInfoRequest usrInfoRequest)
        {
            Logs.Info("CustomerProfileService.AddCustomer method having parameter[ USRInfo]");
            USRInfo dbUSRInfo = await customerProfilesRepository.GetUserByUserIdAsync(usrInfo.userId);
            bool isNewCustomer = false;
            // Requested Objects
            var customerObj = usrInfoRequest.Customers.FirstOrDefault();
            var profileObj = customerObj.Profiles.FirstOrDefault(x => x.Id == 0);
            List<USRCustomer> dbUSRCustomerList = new List<USRCustomer>();

            // Adding/Updating UserInfo
            if (dbUSRInfo != null) //User Exist
            {
                usrInfo.Id = dbUSRInfo.Id;

                if (string.IsNullOrEmpty(dbUSRInfo.emailid))
                {
                    dbUSRInfo.emailid = usrInfo.emailid;
                }
            }
            else
            {
                // Creating New User with details
                customerProfilesRepository.GetContext().USRInfoes.Add(usrInfo);
            }

            // Get all Customers which mapped with USR's in Mapping table
            if (usrInfo.Id != 0)
            {
                dbUSRCustomerList = getUserCustomerList(usrInfo.Id);
            }

            var dbMatchedCustomer = dbUSRCustomerList.Where(p => string.Equals(p.name, customerObj.name, StringComparison.CurrentCultureIgnoreCase)).AsEnumerable().FirstOrDefault();
            var dbMatchedProfile = dbMatchedCustomer != null ? dbMatchedCustomer.Profiles.Where(p => string.Equals(p.name, profileObj.name, StringComparison.CurrentCultureIgnoreCase)).AsEnumerable().FirstOrDefault() : null;
            profileObj.insertTimestamp = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz");
            if (customerObj.Id != 0) // User can select existing Customer and try to save the New profile for it
            {
                // Need to check for the Profile Name if its already existing for the Customer
                if (dbMatchedProfile == null)
                {
                    dbMatchedCustomer.industry = customerObj.industry;
                    dbMatchedCustomer.size = customerObj.size;
                    dbMatchedCustomer.Profiles.Add(profileObj);
                    customerProfilesRepository.SaveChanges();
                }
                else
                {
                    //ERROR: Profile Name already exist for this Customer
                    Logs.Error("Profile Name '" + dbMatchedProfile.name + "' already exist for this Customer");
                    throw new Exception(string.Format("Profile Name '{0}' already exist for this Customer", dbMatchedProfile.name));
                }
            }
            else // User can enter new Customer and try to save the New profile for it, For this need to match if CustomerName is already exist
            {
                // Need to check for the Customer Name existance
                if (dbMatchedCustomer == null)
                {
                    customerProfilesRepository.GetContext().USRCustomers.Add(customerObj);
                    isNewCustomer = true;
                    customerProfilesRepository.SaveChanges();
                }
                else
                {
                    //ERROR: Customer Name already exist for this User
                    Logs.Error("Customer Name= '" + dbMatchedCustomer.name + "' already exist for this User");
                    throw new Exception(string.Format("Customer Name '{0}' already exist for this User", dbMatchedCustomer.name));
                }
            }

            // Add mapping in the USRInfoCustomerMapping table
            if (isNewCustomer)
            {
                USRInfoCustomerMapping usrInfoCustomerMapping = new USRInfoCustomerMapping();
                usrInfoCustomerMapping.usrCustomerPK = customerObj.Id;
                usrInfoCustomerMapping.usrInfoPK = usrInfo.Id;

                customerProfilesRepository.GetContext().USRInfoCustomerMappings.Add(usrInfoCustomerMapping);
                customerProfilesRepository.SaveChanges();
            }

            AddCustomerResponse addCustomerAPIResponse = new AddCustomerResponse();
            List<USRCustomer> usrCustomerList = getUserCustomerList(usrInfo.Id);
            addCustomerAPIResponse.Customers = usrCustomerList;
            addCustomerAPIResponse.CustomerId = customerObj.Id;
            addCustomerAPIResponse.ProfileId = profileObj.Id;
            Logs.ProfileLogs("Add", profileObj.SelectedAccessories, profileObj.USRSelectedProduct, profileObj.ProfileAnswers);
            return addCustomerAPIResponse;
        }

        public async Task<AddCustomerResponse> UpdateCustomer(USRInfo usrInfo, USRInfoRequest usrInfoRequest)
        {
            Logs.Info("CustomerProfileService.UpdateCustomer method having parameter[ USRInfo]");
            USRInfo dbUSRInfo = await customerProfilesRepository.GetUserByUserIdAsync(usrInfo.userId);

            var customerObj = usrInfoRequest.Customers.FirstOrDefault();
            var profileObj = customerObj.Profiles.FirstOrDefault(x => x.Id != 0);
            List<USRCustomer> dbUSRCustomerList = new List<USRCustomer>();

            if (dbUSRInfo != null)
            {
                dbUSRCustomerList = getUserCustomerList(dbUSRInfo.Id);

                var dbMatchedCustomer = dbUSRCustomerList.Where(p => p.Id == customerObj.Id).AsEnumerable().FirstOrDefault();
                var dbMatchedProfile = dbMatchedCustomer != null ? dbMatchedCustomer.Profiles.Where(p => p.Id == profileObj.Id).AsEnumerable().FirstOrDefault() : null;
                var dbMatchedCustomerByName = dbUSRCustomerList.Where(p => string.Equals(p.name, customerObj.name, StringComparison.CurrentCultureIgnoreCase)).AsEnumerable().FirstOrDefault();
                var dbMatchedProfileByName = dbMatchedCustomer != null ? dbMatchedCustomer.Profiles.Where(p => string.Equals(p.name, profileObj.name, StringComparison.CurrentCultureIgnoreCase)).AsEnumerable().FirstOrDefault() : null;

                if (string.IsNullOrEmpty(dbUSRInfo.emailid))
                {
                    dbUSRInfo.emailid = usrInfo.emailid;
                }

                if (dbMatchedCustomer != null)
                {
                    // Need to check for the Customer Name if New Customer name provided and its already existing
                    if (!string.Equals(dbMatchedCustomer.name, customerObj.name, StringComparison.CurrentCultureIgnoreCase) && dbMatchedCustomerByName != null)
                    {
                        //ERROR: Profile Name already exist for this Customer
                        Logs.Error("Updated Customer Name '" + customerObj.name + "' already exist");
                        throw new Exception(string.Format("Updated Customer Name '{0}' already exist", customerObj.name));
                    }
                    else
                    {
                        if (dbMatchedProfile != null)
                        {
                            if (!string.Equals(dbMatchedProfile.name, profileObj.name, StringComparison.CurrentCultureIgnoreCase) && dbMatchedProfileByName != null)
                            {
                                //ERROR: Profile Name already exist
                                Logs.Error("Updated Profile Name '" + profileObj.name + "' already exist");
                                throw new Exception(string.Format("Updated Profile Name '{0}' already exist", profileObj.name));
                            }
                            else
                            {
                                dbMatchedCustomer.name = customerObj.name;
                                dbMatchedCustomer.industry = customerObj.industry;
                                dbMatchedCustomer.size = customerObj.size;
                                dbMatchedProfile.name = profileObj.name;
                                dbMatchedProfile.defaultPersonaLabel = profileObj.defaultPersonaLabel;
                                dbMatchedProfile.updateTimestamp = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz");
                                // updating ProfileAnswers and there options
                                foreach (var profileAnswer in dbMatchedProfile.ProfileAnswers.ToList())
                                {
                                    customerProfilesRepository.GetContext().USRProfileAnswerOptions.RemoveRange(profileAnswer.USRProfileAnswerOptions);
                                    customerProfilesRepository.GetContext().USRProfileAnswers.Remove(profileAnswer);
                                }

                                dbMatchedProfile.ProfileAnswers = profileObj.ProfileAnswers;

                                // updating selectedAccessories and there SKU's
                                foreach (var selectedAccessory in dbMatchedProfile.SelectedAccessories.ToList())
                                {
                                    customerProfilesRepository.GetContext().USRSelectedAccessorySkus.RemoveRange(selectedAccessory.USRSelectedAccessoriesSkus);
                                    customerProfilesRepository.GetContext().USRSelectedAccessories.Remove(selectedAccessory);
                                }
                                dbMatchedProfile.SelectedAccessories = profileObj.SelectedAccessories;

                                // updating selectedProduct
                                dbMatchedProfile.USRSelectedProduct.sku = profileObj.USRSelectedProduct.sku;
                                dbMatchedProfile.USRSelectedProduct.itemId = profileObj.USRSelectedProduct.itemId;
                                dbMatchedProfile.USRSelectedProduct.Name = profileObj.USRSelectedProduct.Name;
                                dbMatchedProfile.USRSelectedProduct.primaryImageUrl = profileObj.USRSelectedProduct.primaryImageUrl;

                                customerProfilesRepository.SaveChanges();
                            }
                        }
                        else
                        {
                            //ERROR: Profile not exist
                            Logs.Error("The following Error has occured: Profile not exist");
                            throw new Exception("Profile not exist");
                        }
                    }
                }
                else
                {
                    //ERROR: Customer not exist
                    Logs.Error("The following Error has occured: Customer not exist");
                    throw new Exception("Customer not exist");
                }
            }
            else
            {
                //ERROR: User not exist
                Logs.Error("The following Error has occured: User not exist");
                throw new Exception("User not exist");
            }

            AddCustomerResponse addCustomerAPIResponse = new AddCustomerResponse();
            List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
            addCustomerAPIResponse.Customers = usrCustomerList;
            addCustomerAPIResponse.CustomerId = customerObj.Id;
            addCustomerAPIResponse.ProfileId = profileObj.Id;
            Logs.Info("The response of CustomerProfileService.UpdateCustomer method is updated USRInfo");
            Logs.ProfileLogs("Update", profileObj.SelectedAccessories, profileObj.USRSelectedProduct, profileObj.ProfileAnswers);
            return addCustomerAPIResponse;
        }

        public List<USRCustomer> GetAllCustomerAsync(string userId)
        {
            Logs.Info("CustomerProfileService.GetAllCustomerAsync method having parameter[userId: " + userId + "]");
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userId).Result;
            if (dbUSRInfo != null)
            {
                return getUserCustomerList(dbUSRInfo.Id);
            }
            return null;
        }

        public USRCustomer GetCustomerByIdAsync(int customerId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.GetCustomerByIdAsync method having parameters [customerId: " + customerId + ", userInfo]");
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                if (usrCustomerList != null && usrCustomerList.Count > 0)
                {
                    Logs.Info("The response of CustomerProfileService.GetCustomerByIdAsync method is USRCustomer");
                    return usrCustomerList.SingleOrDefault(p => p.Id == customerId);
                }
            }
            return null;
        }
        public string DeleteCustomerByIdAsync(int customerId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.DeleteCustomerByIdAsync method having parameters [customerId: " + customerId + ", userInfo]");
            string response = "";
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                USRCustomer customer = usrCustomerList.SingleOrDefault(p => p.Id == customerId);
                if (customer != null)
                {
                    customerProfilesRepository.DeleteUSRInfoCustomerMappingAsync(dbUSRInfo.Id, customer.Id);
                    bool isDeleted = customerProfilesRepository.DeleteCustomerByIdAsync(customer);
                    if (!isDeleted)
                    {
                        response = Constants.API_RESPONSE_FAILURE_MSG;
                    }
                    else
                    {
                        response = Constants.API_RESPONSE_SUCCESS_MSG;
                    }
                }
                else
                {
                    response = Constants.API_RESPONSE_CUSTOMER_DELETE_FAILURE_MSG;
                }
            }
            Logs.Info("The response of CustomerProfileService.DeleteCustomerByIdAsync method is: " + response);
            return response;
        }

        public string DeleteProfileByIdAsync(int profileId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.DeleteProfileByIdAsync method having parameters [profileId: " + profileId + ", userInfo]");
            string response = "";
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                foreach (USRCustomer usrCustomer in usrCustomerList)
                {
                    List<USRProfile> usrProfileList = usrCustomer.Profiles.ToList();
                    if (usrProfileList != null && usrProfileList.Count > 0)
                    {
                        USRProfile usrProfile = usrProfileList.SingleOrDefault(p => p.Id == profileId);
                        if (usrProfile != null)
                        {
                            bool isDeleted = customerProfilesRepository.DeleteProfileByIdAsync(usrProfile);
                            if (!isDeleted)
                            {
                                response = Constants.API_RESPONSE_FAILURE_MSG;
                            }
                            else
                            {
                                response = Constants.API_RESPONSE_SUCCESS_MSG;
                            }
                        }
                        else
                        {
                            response = Constants.API_RESPONSE_PROFILE_DELETE_FAILURE_MSG;
                        }
                    }
                }
            }
            Logs.Info("The response of CustomerProfileService.DeleteProfileByIdAsync method is: " + response);
            return response;
        }

        public List<USRProfile> GetAllProfileAsync(int customerId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.GetAllProfileAsync service has been called having parameters [customerId: " + customerId + ", userInfo]");
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                USRCustomer usrCustomer = usrCustomerList.SingleOrDefault(p => p.Id == customerId);
                if (usrCustomer != null)
                {
                    return usrCustomer.Profiles.ToList();
                }
            }
            return null;
        }

        public USRProfile GetProfileByIdAsync(int profileId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.GetProfileByIdAsync service has been called having parameters [profileId: " + profileId + ", userInfo]");
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                foreach (USRCustomer usrCustomer in usrCustomerList)
                {
                    List<USRProfile> usrProfileList = usrCustomer.Profiles.ToList();
                    if (usrProfileList != null && usrProfileList.Count > 0)
                    {
                        USRProfile usrProfile = usrProfileList.SingleOrDefault(p => p.Id == profileId);
                        if (usrProfile != null)
                        {
                            Logs.Info("The response of CustomerProfileService.GetProfileByIdAsync method is usrProfile");
                            return usrProfile;
                        }
                    }
                }
            }
            return null;
        }

        public USRProfileResponse ValidateProfileByIdAsync(int profileId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.ValidateProfileByIdAsync service has been called having parameters [profileId: " + profileId + ", userInfo]");
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                foreach (USRCustomer usrCustomer in usrCustomerList)
                {
                    List<USRProfile> usrProfileList = usrCustomer.Profiles.ToList();
                    if (usrProfileList != null && usrProfileList.Count > 0)
                    {
                        USRProfile usrProfile = usrProfileList.SingleOrDefault(p => p.Id == profileId);
                        if (usrProfile != null)
                        {
                            // Validating the requested profile
                            USRProfileResponse usrProfileResponse = new USRProfileResponse();
                            CopyProperties.Copy(usrProfile, usrProfileResponse);

                            USRSelectedProductResponse usrSelectedProductResponse = new USRSelectedProductResponse();
                            CopyProperties.Copy(usrProfile.USRSelectedProduct, usrSelectedProductResponse);

                            // Validating selected Product
                            if (customerProfilesRepository.IsProductSkuExist(usrProfile.USRSelectedProduct.sku))
                            {
                                usrSelectedProductResponse.isExist = true;

                                foreach (USRSelectedAccessory accessory in usrProfile.SelectedAccessories)
                                {
                                    USRSelectedAccessoryResponse usrSelectedAccessoryResponse = new USRSelectedAccessoryResponse();
                                    CopyProperties.Copy(accessory, usrSelectedAccessoryResponse);

                                    foreach (USRSelectedAccessorySku accessorySku in accessory.USRSelectedAccessoriesSkus)
                                    {
                                        USRSelectedAccessorySkuResponse usrSelectedAccessorySkuResponse = new USRSelectedAccessorySkuResponse();
                                        CopyProperties.Copy(accessorySku, usrSelectedAccessorySkuResponse);

                                        if (Constants.ASSESSMENT_ACCESSORY_TYPE_DAAS_SERVICE.Equals(accessory.category) && customerProfilesRepository.IsDaasServiceSkuExist(accessorySku.sku))
                                        {
                                            usrSelectedAccessorySkuResponse.isExist = true;
                                        }
                                        else if (customerProfilesRepository.IsAccessorySkuExist(accessorySku.sku, accessory.category))
                                        {
                                            usrSelectedAccessorySkuResponse.isExist = true;
                                        }
                                        usrSelectedAccessoryResponse.USRSelectedAccessoriesSkus.Add(usrSelectedAccessorySkuResponse);
                                    }
                                    usrProfileResponse.SelectedAccessories.Add(usrSelectedAccessoryResponse);
                                }
                            }

                            usrProfileResponse.USRSelectedProduct = usrSelectedProductResponse;

                            return usrProfileResponse;
                        }
                    }
                }
            }
            Logs.Info("The response of CustomerProfileService.ValidateProfileByIdAsync method is usrProfileResponse of Id: " + profileId);
            return null;
        }

        public List<CMSAllowedUser> GetAllAllowedUsers()
        {
            return customerProfilesRepository.GetAllAllowedUsersAsync().Result;
        }

        public CMSRestrictedAccess GetRestrictedAccess()
        {
            Logs.Info("CustomerProfileService.GetRestrictedAccess method has been called");
            return customerProfilesRepository.GetRestrictedAccessAsync().Result;
        }

        public HttpResponseMessage createResponseMsg(HttpStatusCode httpStatusCode, string content, string reasonPhrase)
        {
            Logs.Info("CustomerProfileService.createResponseMsg method has been called  having parameters [httpStatusCode =" + httpStatusCode + ", content= " + content + " and reasonPhrase= " + reasonPhrase + "]");
            var resp = new HttpResponseMessage(httpStatusCode)
            {
                Content = new StringContent(content),
                ReasonPhrase = reasonPhrase
            };
            Logs.Info("The response of CustomerProfileService.createResponseMsg is: " + resp);
            return resp;
        }

        public List<USRCustomer> getUserCustomerList(int userIdPK)
        {
            List<USRCustomer> dbUSRCustomerList = new List<USRCustomer>();
            List<USRInfoCustomerMapping> USRInfoCustomerMappingList = customerProfilesRepository.GetUSRInfoCustomerMappingsByUserInfoPKAsync(userIdPK).Result;
            if (USRInfoCustomerMappingList != null && USRInfoCustomerMappingList.Count > 0)
            {
                List<int> userCustomerIdList = USRInfoCustomerMappingList.Select(p => p.usrCustomerPK.Value).ToList();
                if (userCustomerIdList != null && userCustomerIdList.Count > 0)
                {
                    dbUSRCustomerList = customerProfilesRepository.GetCustomerByIdsAsync(userCustomerIdList).Result;
                }
            }
            return dbUSRCustomerList;
        }

        public HttpResponseMessage DownloadExcelProfileByIdAsync(int profileId, UserInfo userInfo)
        {
            Logs.Info("CustomerProfileService.DownloadExcelProfileByIdAsync method having parameters [profileId: " + profileId + ", userInfo]");
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            USRInfo dbUSRInfo = customerProfilesRepository.GetUserByUserIdAsync(userInfo.userId).Result;
            if (dbUSRInfo != null)
            {
                List<USRCustomer> usrCustomerList = getUserCustomerList(dbUSRInfo.Id);
                foreach (USRCustomer usrCustomer in usrCustomerList)
                {
                    List<USRProfile> usrProfileList = usrCustomer.Profiles.ToList();
                    if (usrProfileList != null && usrProfileList.Count > 0)
                    {
                        USRProfile usrProfile = usrProfileList.SingleOrDefault(p => p.Id == profileId);
                        if (usrProfile != null)
                        {
                            int rowNumber = 1;
                            ExcelPackage ExcelPkg = new ExcelPackage();
                            ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("Sheet1");
                            wsSheet1.Cells[rowNumber, 1].Value = "Date";
                            wsSheet1.Cells[rowNumber, 2].Value = DateTime.Now.ToString("yyyy-MM-dd");
                            ++rowNumber;
                            wsSheet1.Cells[++rowNumber, 1].Value = "Customer";
                            wsSheet1.Cells[rowNumber, 2].Value = usrCustomer.name;
                            //string FileRootPath = "http://bit.ly/2grgfVq";
                            //using (ExcelRange Rng = wsSheet1.Cells[rowNumber, 7, rowNumber, 12])
                            //{
                            //    Rng.Merge = true;
                            //    Rng.Formula = "HYPERLINK(\"" + FileRootPath + "\",\"Please leave suggestions in feedback: " + FileRootPath + "\")";
                            //    Rng.Style.Font.Bold = false;
                            //    Rng.Style.Font.Italic = true;
                            //    Color backgroundColorHex = ColorTranslator.FromHtml("#f2f2f2");
                            //    Rng.Style.Font.Color.SetColor(Color.Red);
                            //    Rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            //    Rng.Style.Fill.BackgroundColor.SetColor(backgroundColorHex);
                            //}

                            wsSheet1.Cells[++rowNumber, 1].Value = "Profile";
                            wsSheet1.Cells[rowNumber, 2].Value = usrProfile.name;
                            ++rowNumber;
                            wsSheet1.Cells[++rowNumber, 2].Value = "Quantity";
                            wsSheet1.Cells[rowNumber, 3].Value = "SKU";
                            wsSheet1.Cells[rowNumber, 4].Value = "Name";

                            if (usrProfile.USRSelectedProduct != null)
                            {
                                wsSheet1.Cells[++rowNumber, 1].Value = "Device";
                                // wsSheet1.Cells[rowNumber, 2].Value = "1";
                                wsSheet1.Cells[rowNumber, 3].Value = usrProfile.USRSelectedProduct.sku;
                                wsSheet1.Cells[rowNumber, 4].Value = usrProfile.USRSelectedProduct.Name;
                            }

                            if (usrProfile.SelectedAccessories != null && usrProfile.SelectedAccessories.Count > 0)
                            {
                                foreach (USRSelectedAccessory usrSelectedAccessory in usrProfile.SelectedAccessories)
                                {
                                    string accessoryType = "";
                                    if (Constants.ASSESSMENT_ACCESSORY_TYPE_DOCK.Equals(usrSelectedAccessory.category))
                                    {
                                        accessoryType = "Dock";
                                    }
                                    else if (Constants.ASSESSMENT_ACCESSORY_TYPE_DISPLAY.Equals(usrSelectedAccessory.category))
                                    {
                                        accessoryType = "Display";
                                    }
                                    else if (Constants.ASSESSMENT_ACCESSORY_TYPE_ACCESSORY.Equals(usrSelectedAccessory.category))
                                    {
                                        accessoryType = "Accessory";
                                    }
                                    else if (Constants.ASSESSMENT_ACCESSORY_TYPE_SERVICE.Equals(usrSelectedAccessory.category))
                                    {
                                        accessoryType = "Service";
                                    }
                                    else if (Constants.ASSESSMENT_ACCESSORY_TYPE_DAAS_SERVICE.Equals(usrSelectedAccessory.category))
                                    {
                                        accessoryType = "Daas-Service";
                                    }

                                    if (usrSelectedAccessory.USRSelectedAccessoriesSkus != null && usrSelectedAccessory.USRSelectedAccessoriesSkus.Count > 0)
                                    {
                                        foreach (USRSelectedAccessorySku usrSelectedAccessorySku in usrSelectedAccessory.USRSelectedAccessoriesSkus)
                                        {

                                            wsSheet1.Cells[++rowNumber, 1].Value = accessoryType;
                                            // wsSheet1.Cells[rowNumber, 2].Value = "";
                                            wsSheet1.Cells[rowNumber, 3].Value = usrSelectedAccessorySku.sku;
                                            wsSheet1.Cells[rowNumber, 4].Value = usrSelectedAccessorySku.Name;
                                        }
                                    }
                                }
                            }

                            wsSheet1.Protection.IsProtected = false;
                            wsSheet1.Protection.AllowSelectLockedCells = false;

                            //string fileName = usrProfile.name;
                            //foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                            //{
                            //    fileName = fileName.Replace(c, '_');
                            //}

                            response.Content = new ByteArrayContent(ExcelPkg.GetAsByteArray());
                            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                            //response.Content.Headers.ContentDisposition.FileName = fileName + "_Assessment.xlsx";
                            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                            return response;
                        }
                    }
                }
            }
            Logs.Info("The response of CustomerProfileService.DownloadExcelProfileByIdAsync method is: " + response);
            return null;
        }

        public ProductSkuDTO GetSelectedProductDetails(string sku)
        {
            //Getting product having specified sku
            ProductSkuDTO productSkuDTO = new ProductSkuDTO(false);
            ProductSku productSku = recommendationRepository.GetSkuAsync(sku).Result;
            CopyProperties.Copy(productSku, productSkuDTO);

            //Getting the Product Features list for selected product
            List<ProductFeature> productFeatureList = recommendationRepository.GetProductFeaturesByPmoidAsync(productSkuDTO.pmoid, productSkuDTO.country, productSkuDTO.language).Result;

            // Adding Product Features
            if (productFeatureList != null)
            {
                List<ProductFeatureDTO> productFeatureDTOList = new List<ProductFeatureDTO>();
                List<ProductFeatureDTO> productFootNoteDTOList = new List<ProductFeatureDTO>();
                List<string> productFeaturePriorityList = new List<string> { "ksp_01_suppt_01_short", "ksp_02_suppt_01_short", "ksp_03_suppt_01_short", "ksp_04_suppt_01_short", "ksp_01_headline_medium", "ksp_02_headline_medium", "ksp_03_headline_medium", "ksp_04_headline_medium" };
                List<ProductFeature> productKSPList = productFeatureList.Where(p => p.elementName.StartsWith("ksp")).ToList();
                List<ProductFeature> productFootNoteList = productFeatureList.Where(p => p.elementName.StartsWith("msgfootnote")).ToList();
                List<string> matchedNumbers = new List<string>();

                foreach (string featurePriority in productFeaturePriorityList)
                {
                    if (productFeatureDTOList.Count == 3)
                        break;

                    foreach (ProductFeature productFeature in productKSPList)
                    {
                        if (featurePriority.Equals(productFeature.elementName))
                        {
                            ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                            CopyProperties.Copy(productFeature, productFeatureDTO);
                            productFeatureDTOList.Add(productFeatureDTO);
                            if (productFeature.elementValue != null)
                            {
                                List<string> matches =
                                Regex.Matches(productFeature.elementValue.Replace(Environment.NewLine, ""), @"\[([^]]*)\]")
                                    .Cast<Match>()
                                    .Select(x => x.Groups[1].Value)
                                    .ToList();

                                matches = matches.Distinct().ToList();

                                foreach (string footNoteMatchedNumber in matches)
                                {
                                    if (matchedNumbers.Contains(footNoteMatchedNumber))
                                        continue;
                                    ProductFeature productFootNote = productFootNoteList.FirstOrDefault(p => p.elementValue.StartsWith("[" + footNoteMatchedNumber + "]"));
                                    if (productFootNote != null)
                                    {
                                        ProductFeatureDTO productFeatureDTOFootNote = new ProductFeatureDTO();
                                        CopyProperties.Copy(productFootNote, productFeatureDTOFootNote);
                                        productFootNoteDTOList.Add(productFeatureDTOFootNote);
                                    }
                                    matchedNumbers.Add(footNoteMatchedNumber);
                                }
                            }
                        }
                    }
                }

                productSkuDTO.productFeatureList = productFeatureDTOList;
                productSkuDTO.productFootNoteList = productFootNoteDTOList;
            }

            return productSkuDTO;
        }

        public List<ProductAccessoryDTO> GetAccessoriesDetails(List<string> accessorySkuList, string accessoryType)
        {
            //Getting accessories having specified Skus

            List<ProductAccessory> productAccessoryList = recommendationRepository.GetAccessoriesBySkusAsync(accessorySkuList).Result;


            List<ProductAccessoryDTO> productAccessoryDTOList = new List<ProductAccessoryDTO>();


            foreach (ProductAccessory productAccessory in productAccessoryList)
            {
                ProductAccessoryDTO productAccessoryDTO = new ProductAccessoryDTO(false, accessoryType);
                CopyProperties.Copy(productAccessory, productAccessoryDTO);

                //Getting the Product Features list for particular product
                List<ProductFeature> productFeatureList = recommendationRepository.GetProductFeaturesByPmoidAsync(productAccessory.pmoid, productAccessory.country, productAccessory.language).Result;

                if (productFeatureList != null)
                {
                    List<ProductFeatureDTO> productFeatureDTOList = new List<ProductFeatureDTO>();
                    List<ProductFeatureDTO> productFootNoteDTOList = new List<ProductFeatureDTO>();
                    List<string> productFeaturePriorityList = new List<string> { "ksp_01_suppt_01_short", "ksp_02_suppt_01_short", "ksp_03_suppt_01_short", "ksp_04_suppt_01_short", "ksp_01_headline_medium", "ksp_02_headline_medium", "ksp_03_headline_medium", "ksp_04_headline_medium" };
                    List<ProductFeature> productKSPList = productFeatureList.Where(p => p.elementName.StartsWith("ksp")).ToList();
                    List<ProductFeature> productFootNoteList = productFeatureList.Where(p => p.elementName.StartsWith("msgfootnote")).ToList();
                    List<string> matchedNumbers = new List<string>();

                    if (Constants.ACCESSORY_TYPE_DOCK.Equals(accessoryType))
                    {
                        if (!string.IsNullOrEmpty(productAccessory.ioports))
                        {
                            ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                            productFeatureDTO.elementName = "ioports";
                            productFeatureDTO.elementValue = productAccessory.ioports;
                            productFeatureDTOList.Add(productFeatureDTO);
                        }
                    }
                    else if (Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType))
                    {
                        if (!string.IsNullOrEmpty(productAccessory.displaytype))
                        {
                            ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                            productFeatureDTO.elementName = "displaytype";
                            productFeatureDTO.elementValue = productAccessory.displaytype;
                            productFeatureDTOList.Add(productFeatureDTO);
                        }
                    }

                    foreach (string featurePriority in productFeaturePriorityList)
                    {
                        if (productFeatureDTOList.Count == 3)
                            break;

                        if (Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType) && featurePriority.Equals("ksp_01_headline_medium"))
                        {
                            if (!string.IsNullOrEmpty(productAccessory.monitorfeat))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                productFeatureDTO.elementName = "monitorfeat";
                                productFeatureDTO.elementValue = productAccessory.monitorfeat;
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeatureDTOList.Count == 3)
                                    break;
                            }
                        }

                        foreach (ProductFeature productFeature in productKSPList)
                        {
                            if (featurePriority.Equals(productFeature.elementName))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                CopyProperties.Copy(productFeature, productFeatureDTO);
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeature.elementValue != null)
                                {
                                    List<string> matches =
                                    Regex.Matches(productFeature.elementValue.Replace(Environment.NewLine, ""), @"\[([^]]*)\]")
                                        .Cast<Match>()
                                        .Select(x => x.Groups[1].Value)
                                        .ToList();

                                    matches = matches.Distinct().ToList();

                                    foreach (string footNoteMatchedNumber in matches)
                                    {
                                        if (matchedNumbers.Contains(footNoteMatchedNumber))
                                            continue;
                                        ProductFeature productFootNote = productFootNoteList.FirstOrDefault(p => p.elementValue.StartsWith("[" + footNoteMatchedNumber + "]"));
                                        if (productFootNote != null)
                                        {
                                            ProductFeatureDTO productFeatureDTOFootNote = new ProductFeatureDTO();
                                            CopyProperties.Copy(productFootNote, productFeatureDTOFootNote);
                                            productFootNoteDTOList.Add(productFeatureDTOFootNote);
                                        }
                                        matchedNumbers.Add(footNoteMatchedNumber);
                                    }
                                }
                            }
                        }
                    }

                    productAccessoryDTO.productFeatureList = productFeatureDTOList;
                    productAccessoryDTO.productFootNoteList = productFootNoteDTOList;
                }
                productAccessoryDTOList.Add(productAccessoryDTO);
            }
            return productAccessoryDTOList;
        }

        public List<ProductDaasServiceDTO> GetDaasServicesDetails(List<string> accessorySkuList)
        {
            //Getting accessories having specified Skus
            
            List<ProductDaasService> productDaasServiceList = recommendationRepository.GetDaasServicesBySkusAsync(accessorySkuList).Result;


            List<ProductDaasServiceDTO> productAccessoryDTOList = new List<ProductDaasServiceDTO>();


            foreach (ProductDaasService productDaasService in productDaasServiceList)
            {
                ProductDaasServiceDTO productDaasServiceDTO = new ProductDaasServiceDTO(false);
                CopyProperties.Copy(productDaasService, productDaasServiceDTO);

                //Getting the Product Features list for particular product
                List<ProductDaasFeature> productDaasFeatureList = recommendationRepository.GetProductDaasFeaturesByPmoidAsync(productDaasService.pmoid, productDaasService.country, productDaasService.language).Result;

                if (productDaasFeatureList != null)
                {
                    List<ProductFeatureDTO> productFeatureDTOList = new List<ProductFeatureDTO>();
                    List<ProductFeatureDTO> productFootNoteDTOList = new List<ProductFeatureDTO>();
                    List<string> productFeaturePriorityList = new List<string> { "ksp_01_suppt_01_short", "ksp_02_suppt_01_short", "ksp_03_suppt_01_short", "ksp_04_suppt_01_short", "ksp_01_headline_medium", "ksp_02_headline_medium", "ksp_03_headline_medium", "ksp_04_headline_medium" };
                    List<ProductDaasFeature> productKSPList = productDaasFeatureList.Where(p => p.elementName.StartsWith("ksp")).ToList();
                    List<ProductDaasFeature> productFootNoteList = productDaasFeatureList.Where(p => p.elementName.StartsWith("msgfootnote")).ToList();
                    List<string> matchedNumbers = new List<string>();

                    foreach (string featurePriority in productFeaturePriorityList)
                    {
                        if (productFeatureDTOList.Count == 3)
                            break;

                        foreach (ProductDaasFeature productFeature in productKSPList)
                        {
                            if (featurePriority.Equals(productFeature.elementName))
                            {
                                ProductFeatureDTO productFeatureDTO = new ProductFeatureDTO();
                                CopyProperties.Copy(productFeature, productFeatureDTO);
                                productFeatureDTOList.Add(productFeatureDTO);
                                if (productFeature.elementValue != null)
                                {
                                    List<string> matches =
                                    Regex.Matches(productFeature.elementValue.Replace(Environment.NewLine, ""), @"\[([^]]*)\]")
                                        .Cast<Match>()
                                        .Select(x => x.Groups[1].Value)
                                        .ToList();

                                    matches = matches.Distinct().ToList();

                                    foreach (string footNoteMatchedNumber in matches)
                                    {
                                        if (matchedNumbers.Contains(footNoteMatchedNumber))
                                            continue;
                                        ProductDaasFeature productFootNote = productFootNoteList.FirstOrDefault(p => p.elementValue.StartsWith("[" + footNoteMatchedNumber + "]"));
                                        if (productFootNote != null)
                                        {
                                            ProductFeatureDTO productFeatureDTOFootNote = new ProductFeatureDTO();
                                            CopyProperties.Copy(productFootNote, productFeatureDTOFootNote);
                                            productFootNoteDTOList.Add(productFeatureDTOFootNote);
                                        }
                                        matchedNumbers.Add(footNoteMatchedNumber);
                                    }
                                }
                            }
                        }
                    }

                    productDaasServiceDTO.productFeatureList = productFeatureDTOList;
                    productDaasServiceDTO.productFootNoteList = productFootNoteDTOList;
                }

                productAccessoryDTOList.Add(productDaasServiceDTO);
            }
            return productAccessoryDTOList;
        }
    }

    public interface ICustomerProfileService
    {
        Task<AddCustomerResponse> AddCustomer(USRInfo usrInfo, USRInfoRequest usrInfoRequest);
        Task<AddCustomerResponse> UpdateCustomer(USRInfo usrInfo, USRInfoRequest usrInfoRequest);
        List<USRCustomer> GetAllCustomerAsync(string userId);
        USRCustomer GetCustomerByIdAsync(int customerId, UserInfo userInfo);
        List<USRProfile> GetAllProfileAsync(int customerId, UserInfo userInfo);
        USRProfile GetProfileByIdAsync(int profileId, UserInfo userInfo);
        USRProfileResponse ValidateProfileByIdAsync(int profileId, UserInfo userInfo);
        List<CMSAllowedUser> GetAllAllowedUsers();
        CMSRestrictedAccess GetRestrictedAccess();
        string DeleteCustomerByIdAsync(int customerId, UserInfo userInfo);
        string DeleteProfileByIdAsync(int profileId, UserInfo userInfo);
        HttpResponseMessage DownloadExcelProfileByIdAsync(int profileId, UserInfo userInfo);
        List<ProductAccessoryDTO> GetAccessoriesDetails(List<string> accessorySkuList, string accessoryType);
        List<ProductDaasServiceDTO> GetDaasServicesDetails(List<string> accessorySkuList);
        ProductSkuDTO GetSelectedProductDetails(string sku);
    }
}
