﻿using hpi_user_assessment_tool.Data;
using hpi_user_assessment_tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using hpi_user_assessment_tool.Utility;

namespace hpi_user_assessment_tool.Services
{
    public class DefaultPersonaService : IDefaultPersonaService
    {
        private IDefaultPersonaRepository defaultPersonaRepository;

        public DefaultPersonaService(IDefaultPersonaRepository defaultPersonaRepository)
        {
            this.defaultPersonaRepository = defaultPersonaRepository;
        }

        public Task<List<CMSDefaultPersona>> GetAllDefaultPersonaAsync()
        {
            Logs.Info("DefaultPersonaService.GetAllDefaultPersonaAsync method has been called ");
            return defaultPersonaRepository.GetAllDefaultPersonaAsync();
        }
        public Task<CMSDefaultPersona> GetDefaultPersonaByIdAsync(int id)
        {
            Logs.Info("DefaultPersonaService.GetDefaultPersonaByIdAsync method has been called having parameter [id: " + id + "]");
            return defaultPersonaRepository.GetDefaultPersonaByIdAsync(id);
        }
        public Task<CMSDefaultPersona> GetDefaultPersonaByProfileAnswerLabelAsync(string profileAnswerLabel)
        {
            Logs.Info("DefaultPersonaService.GetDefaultPersonaByProfileAnswerLabelAsync method has been called having parameter [profileAnswerLabel: " + profileAnswerLabel + "]");
            return defaultPersonaRepository.GetDefaultPersonaByProfileAnswerLabelAsync(profileAnswerLabel);
        }
    }

    public interface IDefaultPersonaService
    {
        Task<List<CMSDefaultPersona>> GetAllDefaultPersonaAsync();
        Task<CMSDefaultPersona> GetDefaultPersonaByIdAsync(int id);
        Task<CMSDefaultPersona> GetDefaultPersonaByProfileAnswerLabelAsync(string profileAnswerLabel);
    }
}