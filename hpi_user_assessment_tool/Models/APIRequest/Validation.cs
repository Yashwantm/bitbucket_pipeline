﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.APIRequest
{
    public class Validation
    {
        public List<int> answerIds { get; set; }
        public List<int> filterTagIds { get; set; }
        public List<int> weightedTagIds { get; set; }

    }

    public class TagsValidationValidator : FluentValidation.AbstractValidator<Validation>
    {
        public TagsValidationValidator()
        {
            RuleFor(x => x.answerIds).Must(p => p != null).WithMessage("'answerIds' should not be empty");
        }
        public string validateRequest(Validation validation)
        {
            ValidationResult results = Validate(validation);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }

    public class FilterTagsValidationValidator : FluentValidation.AbstractValidator<Validation>
    {
        public FilterTagsValidationValidator()
        {
            RuleFor(x => x.filterTagIds).Must(p => p != null).WithMessage("'filterTagIds' should not be empty");
        }
        public string validateRequest(Validation validation)
        {
            ValidationResult results = Validate(validation);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }

    public class WeightedTagsValidationValidator : FluentValidation.AbstractValidator<Validation>
    {
        public WeightedTagsValidationValidator()
        {
            RuleFor(x => x.weightedTagIds).Must(p => p != null).WithMessage("'weightedTagIds' should not be empty");
        }
        public string validateRequest(Validation validation)
        {
            ValidationResult results = Validate(validation);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }
}