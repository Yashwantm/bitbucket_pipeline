﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.APIRequest
{
    public class Recommendation
    {
        public bool countOnly { get; set; }
        public int pmoid { get; set; }
        public string country { get; set; }
        public string language { get; set; }
        public List<int> answerIds { get; set; }
        public int offSet { get; set; }
        public int limit { get; set; }
        public bool debug { get; set; }

    }

    public class SKURecommendationValidator : FluentValidation.AbstractValidator<Recommendation>
    {
        public SKURecommendationValidator()
        {
            RuleFor(recommendation => recommendation.country).NotEmpty();
            RuleFor(recommendation => recommendation.language).NotEmpty();
        }


        public string validateRequest(Recommendation recommendation)
        {
            ValidationResult results = Validate(recommendation);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }

    public class AccessoryRecommendationValidator : FluentValidation.AbstractValidator<Recommendation>
    {
        public AccessoryRecommendationValidator()
        {
            RuleFor(recommendation => recommendation.country).NotEmpty();
            RuleFor(recommendation => recommendation.language).NotEmpty();
            RuleFor(recommendation => recommendation.pmoid).NotEmpty();
        }

        public string validateRequest(Recommendation recommendation)
        {
            ValidationResult results = Validate(recommendation);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }
}