﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.APIRequest
{
    public class RecommendationScenarios
    {
        public bool countOnly { get; set; }
        public int pmoid { get; set; }
        public string country { get; set; }
        public string language { get; set; }
        public List<Scenario> scenarios { get; set; }
        public int offSet { get; set; }
        public int limit { get; set; }
        public bool debug { get; set; }

    }

    public class SKURecommendationScenariosValidator : FluentValidation.AbstractValidator<RecommendationScenarios>
    {
        public SKURecommendationScenariosValidator()
        {
        }


        public string validateRequest(RecommendationScenarios recommendationScenarios)
        {
            ValidationResult results = Validate(recommendationScenarios);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }

    public class AccessoryRecommendationScenariosValidator : FluentValidation.AbstractValidator<RecommendationScenarios>
    {
        public AccessoryRecommendationScenariosValidator()
        {
        }

        public string validateRequest(RecommendationScenarios recommendationScenarios)
        {
            ValidationResult results = Validate(recommendationScenarios);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }
}