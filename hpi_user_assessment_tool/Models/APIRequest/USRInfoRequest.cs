﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class USRInfoRequest
    {
        public virtual ICollection<USRCustomer> Customers { get; set; }
    }
}