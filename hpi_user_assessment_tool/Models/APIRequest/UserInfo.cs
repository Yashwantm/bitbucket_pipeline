﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Services;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.APIRequest
{
    public class UserInfo
    {
        public string userId { get; set; }
        public string emailId { get; set; }
    }

    public class UserInfoValidator : FluentValidation.AbstractValidator<UserInfo>
    {
        private readonly ICustomerProfileService customerProfileService;

        public UserInfoValidator(ICustomerProfileService customerProfileService)
        {
            RuleFor(userInfo => userInfo.userId).NotEmpty();
            RuleFor(userInfo => userInfo.emailId).NotEmpty();
            RuleFor(userInfo => userInfo.userId).MaximumLength(50);
            RuleFor(userInfo => userInfo.emailId).EmailAddress();

            this.customerProfileService = customerProfileService;
        }

        public string validateRequest(UserInfo userInfo)
        {
            ValidationResult results = Validate(userInfo);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                //if restrict user is set as true then validate the email address with allowed user list
                CMSRestrictedAccess cmsRestrictedAccess = customerProfileService.GetRestrictedAccess();
                if ((bool)cmsRestrictedAccess.isRestrictedAccess)
                {
                    List<CMSAllowedUser> cmsAllowedUserList = customerProfileService.GetAllAllowedUsers();
                    CMSAllowedUser cmsAllowedUser = cmsAllowedUserList.SingleOrDefault(p => string.Equals(p.emailAddress, userInfo.emailId, StringComparison.CurrentCultureIgnoreCase));
                    if (cmsAllowedUser == null)
                    {
                        return Constants.UNAUTHORIZED_ACCESS_MSG;
                    }
                }
                return "";
            }
        }
    }
}