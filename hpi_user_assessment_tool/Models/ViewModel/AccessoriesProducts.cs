﻿using hpi_user_assessment_tool.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.ViewModel
{
    public class AccessoriesProducts
    {
        public string customerName { get; set; }
        public USRProfile profile { get; set; }
        public ProductSkuDTO SelectedProduct { get; set; }
        public List<ProductAccessoryDTO> AccessoryList { get; set; }
        public List<ProductDaasServiceDTO> productDaasServiceDTO { get; set; }
        public Dictionary<string, string> selectedAnswerText { get; set; }
    }
}