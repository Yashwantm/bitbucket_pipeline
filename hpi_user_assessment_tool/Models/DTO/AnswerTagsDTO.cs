﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class AnswerTagsDTO
    {
        public int answerId { get; set; }
        public string answerLabel { get; set; }
        public string answerDescription { get; set; }

        public List<CMSTag> filterTagList { get; set; }

        public List<CMSWeightedTag> weightedTagList { get; set; }
    }
}