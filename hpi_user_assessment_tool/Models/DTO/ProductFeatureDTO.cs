﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class ProductFeatureDTO
    {
        public string elementName { get; set; }
        public string elementValue { get; set; }
    }
}