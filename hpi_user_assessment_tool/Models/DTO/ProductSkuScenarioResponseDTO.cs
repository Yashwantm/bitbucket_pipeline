﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class ProductSkuScenarioResponseDTO
    {
        public int id { get; set; }
        public List<string> answers { get; set; }
        public List<ProductSkuScenarioDTO> recommendations { get; set; }
    }
}