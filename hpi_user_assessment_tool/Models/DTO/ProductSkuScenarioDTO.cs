﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class ProductSkuScenarioDTO
    {
        public int Id { get; set; }
        public string sku { get; set; }
        public string bigSeries { get; set; }
        public string smallSeries { get; set; }
        public string Name { get; set; }
        public int order { get; set; }
        public float averageWeight { get; set; }
        public float totalWeight { get; set; }
        public int totalNumberofWeightedTag { get; set; }
        public int matchedWeightedTagCount { get; set; }
        
    }
}