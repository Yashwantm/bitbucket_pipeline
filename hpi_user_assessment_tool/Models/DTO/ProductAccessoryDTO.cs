﻿using hpi_user_assessment_tool.Models.Constant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class ProductAccessoryDTO
    {
        public ProductAccessoryDTO(bool debug, string accessoryType)
        {
            this.debug = debug;
            this.accessoryType = accessoryType;
        }

        [JsonIgnore]
        public bool debug { get; set; }
        [JsonIgnore]
        public string accessoryType { get; set; }
        public int Id { get; set; }
        public string country { get; set; }
        public string language { get; set; }
        public string pmoid { get; set; }
        public string sku { get; set; }
        public string bigSeries { get; set; }
        public string smallSeries { get; set; }
        public string smallSeriesPmoid { get; set; }
        public string primaryImageUrl { get; set; }
        public string marketing_sub_category_pmoid { get; set; }
        public string bigSeriesPmoid { get; set; }
        public string name { get; set; }
        public int order { get; set; }
        public List<CMSWeightedTag> matchedWeightedTags { get; set; }
        public bool ShouldSerializematchedWeightedTags()
        {
            return (debug == true);
        }
        public List<CMSWeightedTag> unmatchedWeightedTags { get; set; }
        public bool ShouldSerializeunmatchedWeightedTags()
        {
            return (debug == true);
        }
        public float averageWeight { get; set; }
        public bool ShouldSerializeaverageWeight()
        {
            return (debug == true);
        }

        public string ioports { get; set; }
        public bool ShouldSerializeioports()
        {
            return (Constants.ACCESSORY_TYPE_DOCK.Equals(accessoryType) || Constants.ACCESSORY_TYPE_DISPLAY.Equals(accessoryType));
        }

        public float totalWeight { get; set; }
        public bool ShouldSerializetotalWeight()
        {
            return (debug == true);
        }
        public int totalNumberofWeightedTag { get; set; }
        public bool ShouldSerializetotalNumberofWeightedTag()
        {
            return (debug == true);
        }

        public string datasheet { get; set; }
        public string quickspecs { get; set; }

        public List<ProductFeatureDTO> productFeatureList { get; set; }
        public List<ProductFeatureDTO> productFootNoteList { get; set; }
    }
}