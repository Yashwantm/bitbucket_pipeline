﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Constant
{
    public class Constants
    {
        public const string ACCESSORY_TYPE_DOCK = "Dock";
        public const string ACCESSORY_TYPE_DISPLAY = "Display";
        public const string ACCESSORY_TYPE_ACCESSORY = "Accessory";
        public const string ACCESSORY_TYPE_SERVICE = "Service";
        public const string ENTITY_TYPE_DAAS_SERVICE = "DaasService";
        public const string ENTITY_TYPE_SKU = "Sku";
        public const string ENTITY_TYPE_ACCESSORY = "Accessory";
        public const string DEFAULT_COUNTRY = "us";
        public const string DEFAULT_LANGUAGE = "en";
        public const string JWT_TOKEN_COOKIE_NAME = "devicewisetoken";

        public const string UNAUTHORIZED_ACCESS_MSG = "You are not authorized to access this resource";
        public const string API_RESPONSE_SUCCESS_MSG = "Success";
        public const string API_RESPONSE_FAILURE_MSG = "Failure";
        public const string API_RESPONSE_CUSTOMER_DELETE_FAILURE_MSG = "Customer not exist or you are not authorized person to delete it";
        public const string API_RESPONSE_PROFILE_DELETE_FAILURE_MSG = "Profile not exist or you are not authorized person to delete it";


        public const string APPLIED_ON_ENTITY_DEVICE = "Device";
        public const string APPLIED_ON_ENTITY_ACCESSORY = "Accessory";
        public const string APPLIED_ON_ENTITY_DOCK = "Dock";
        public const string APPLIED_ON_ENTITY_DISPLAY = "Display";
        public const string APPLIED_ON_ENTITY_SERVICE = "Service";
        public const string APPLIED_ON_ENTITY_ALLACCESSORIES = "All-Accessories";
        public const string APPLIED_ON_ENTITY_DAAS_SERVICE = "Daas-Service";

        public const string FILTER_DEVICE_ASSESSMENT_LABEL = "assessment_deviceRecomendation";
        public const string FILTER_DOCK_ASSESSMENT_LABEL = "assessment_dockRecommendation";
        public const string FILTER_DISPLAY_ASSESSMENT_LABEL = "assessment_displayRecommendation";
        public const string FILTER_ACCESSORY_ASSESSMENT_LABEL = "assessment_accessoryRecommendation";
        public const string FILTER_SERVICE_ASSESSMENT_LABEL = "assessment_servicesRecommendation";
        public const string FILTER_DAAS_SERVICE_ASSESSMENT_LABEL = "assessment_daasServicesRecommendation";

        // Saved Assessment accessories types
        public const string ASSESSMENT_ACCESSORY_TYPE_DOCK = "dock";
        public const string ASSESSMENT_ACCESSORY_TYPE_DISPLAY = "display";
        public const string ASSESSMENT_ACCESSORY_TYPE_ACCESSORY = "accessories";
        public const string ASSESSMENT_ACCESSORY_TYPE_SERVICE = "service";
        public const string ASSESSMENT_ACCESSORY_TYPE_DAAS_SERVICE = "daas-service";
    }
}