﻿using hpi_user_assessment_tool.Models.Constant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace hpi_user_assessment_tool.Models.Response
{
    public class WrappingHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            return BuildApiResponse(request, response);
        }

        private static HttpResponseMessage BuildApiResponse(HttpRequestMessage request, HttpResponseMessage response)
        {
            object content;
            string errorMessage = null;

            if (response.TryGetContentValue(out content) && !response.IsSuccessStatusCode)
            {
                HttpError error = content as HttpError;
                if (error != null)
                {
                    content = null;
                    if (Constants.UNAUTHORIZED_ACCESS_MSG.Equals(error.ExceptionMessage) || Constants.UNAUTHORIZED_ACCESS_MSG.Equals(error.Message))
                    {
                        errorMessage = Constants.UNAUTHORIZED_ACCESS_MSG;
                        response.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                    }
                    else
                    {
                        errorMessage = error.Message;
                        errorMessage = string.Concat(errorMessage, error.ExceptionMessage, error.StackTrace);
                    }
                }
            }

            if (response.Content.GetType() != typeof(ByteArrayContent) && response.Content.GetType() != typeof(System.Web.Mvc.FileStreamResult))
            {
                var newResponse = request.CreateResponse(response.StatusCode, new ApiResponse(response.StatusCode, content, errorMessage));

                foreach (var header in response.Headers)
                {
                    newResponse.Headers.Add(header.Key, header.Value);
                }

                return newResponse;
            }

            return response;
        }
    }
}