﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Response
{
    public class GetAllQuestionsResponse
    {
        public List<CMSQuestion> questions { get; set; }
        public List<CMSConditionalQuestion> conditionalQuestions { get; set; }
    }
}