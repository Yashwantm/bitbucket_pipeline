﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Response
{
    public class USRSelectedAccessoryResponse
    {
        public USRSelectedAccessoryResponse()
        {
            this.USRSelectedAccessoriesSkus = new HashSet<USRSelectedAccessorySkuResponse>();
        }

        public int Id { get; set; }
        public string category { get; set; }
        public int ProfileId { get; set; }
        public virtual ICollection<USRSelectedAccessorySkuResponse> USRSelectedAccessoriesSkus { get; set; }
    }
}