﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Response
{
    public class USRProfileResponse
    {
        public USRProfileResponse()
        {
            this.ProfileAnswers = new HashSet<USRProfileAnswer>();
            this.SelectedAccessories = new HashSet<USRSelectedAccessoryResponse>();
        }

        public int Id { get; set; }
        public string name { get; set; }
        public string defaultPersonaLabel { get; set; }
        public int CustomerId { get; set; }

        public ICollection<USRProfileAnswer> ProfileAnswers { get; set; }
        public ICollection<USRSelectedAccessoryResponse> SelectedAccessories { get; set; }
        public USRSelectedProductResponse USRSelectedProduct { get; set; }
    }
}