﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Response
{
    public class USRSelectedProductResponse
    {
        public int Id { get; set; }
        public string sku { get; set; }
        public string itemId { get; set; }
        public string Name { get; set; }
        public string primaryImageUrl { get; set; }
        public bool isExist { get; set; }
    }
}