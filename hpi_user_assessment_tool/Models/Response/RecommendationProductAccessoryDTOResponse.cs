﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class RecommendationProductDaasAccessoryDTOResponse
    {
        public int totalCount { get; set; }
        public List<ProductDaasServiceDTO> recommendations { get; set; }
    }
}