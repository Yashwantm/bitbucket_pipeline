﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.DTO
{
    public class RecommendationProductAccessoryDTOResponse
    {
        public int totalCount { get; set; }
        public List<ProductAccessoryDTO> recommendations { get; set; }
    }
}