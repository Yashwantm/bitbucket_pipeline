﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Response
{
    public class AddCustomerResponse
    {
        public int CustomerId { get; set; }
        public int ProfileId { get; set; }
        public List<USRCustomer> Customers { get; set; }
    }
}