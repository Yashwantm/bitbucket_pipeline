﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Process
{
    public class WeightCalculation
    {
        public WeightCalculation()
        {
            averageWeight = 0;
            totalNumberOfWeightedTags = 0;
            matchedCMSWeightedTagList = new List<CMSWeightedTag>();
            unmatchedCMSWeightedTagList = new List<CMSWeightedTag>();
        }

        public string sku { get; set; }
        public float totalWeight { get; set; }
        public int totalNumberOfWeightedTags { get; set; }
        public float averageWeight { get; set; }
        public int order { get; set; }
        public string pmoid { get; set; }
        public string smallSeriesPmoid { get; set; }
        public string bigSeriesPmoid { get; set; }
        public string marketing_sub_category_pmoid { get; set; }
        public List<CMSWeightedTag> matchedCMSWeightedTagList { get;  set;}
        public List<CMSWeightedTag> unmatchedCMSWeightedTagList { get; set; }
        
    }
}