﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Validation
{
    public class USRInfoValidator : FluentValidation.AbstractValidator<USRInfoRequest>
    {
        public USRInfoValidator()
        {
        }
        public string validateRequest(USRInfoRequest usrInfoRequest)
        {
            ValidationResult results = Validate(usrInfoRequest);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                if (usrInfoRequest.Customers != null && usrInfoRequest.Customers.Count > 0)
                {
                    foreach (USRCustomer usrCustomer in usrInfoRequest.Customers)
                    {
                        USRCustomerValidator usrCustomerValidator = new USRCustomerValidator();
                        ValidationResult usrCustomerValidatorResults = usrCustomerValidator.Validate(usrCustomer);
                        if (!usrCustomerValidatorResults.IsValid)
                        {
                            return ValidatorUtil.getErrorMSG(usrCustomerValidatorResults);
                        }
                        else
                        {
                            if (usrCustomer.Profiles != null && usrCustomer.Profiles.Count > 0)
                            {
                                foreach (USRProfile usrProfile in usrCustomer.Profiles)
                                {
                                    USRProfileValidator usrProfileValidator = new USRProfileValidator();
                                    ValidationResult usrProfileValidatorResults = usrProfileValidator.Validate(usrProfile);
                                    if (!usrProfileValidatorResults.IsValid)
                                    {
                                        return ValidatorUtil.getErrorMSG(usrProfileValidatorResults);
                                    }
                                }
                            }
                        }
                    }
                }
                return "";
            }
        }
    }
}