﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Validation
{
    public class USRProfileValidator : FluentValidation.AbstractValidator<USRProfile>
    {
        public USRProfileValidator()
        {
            RuleFor(userInfo => userInfo.name).NotEmpty();
            RuleFor(userInfo => userInfo.defaultPersonaLabel).NotEmpty();
        }
        public string validateRequest(USRProfile usrProfile)
        {
            ValidationResult results = Validate(usrProfile);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }
}