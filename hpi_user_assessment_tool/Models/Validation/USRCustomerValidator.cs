﻿using FluentValidation;
using FluentValidation.Results;
using hpi_user_assessment_tool.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models.Validation
{
    public class USRCustomerValidator : FluentValidation.AbstractValidator<USRCustomer>
    {
        public USRCustomerValidator()
        {
            RuleFor(userInfo => userInfo.name).NotEmpty();
            RuleFor(userInfo => userInfo.size).NotEmpty();
            RuleFor(userInfo => userInfo.industry).NotEmpty();
        }
        public string validateRequest(USRCustomer usrCustomer)
        {
            ValidationResult results = Validate(usrCustomer);
            if (!results.IsValid)
            {
                return ValidatorUtil.getErrorMSG(results);
            }
            else
            {
                return "";
            }
        }
    }
}