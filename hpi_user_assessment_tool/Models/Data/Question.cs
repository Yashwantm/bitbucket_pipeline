﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class Question
    {
        public string id { get; set; }
        public string description { get; set; }
        public string label { get; set; }
        public int order { get; set; }
        public string imageURL { get; set; }
        public string displayType { get; set; }
        public List<LocalValue> text { get; set; }
        public AssessmentStep assessmentStep { get; set; }
        public List<Insight> insights { get; set; }
        public List<Answer> answers { get; set; }
    }
}