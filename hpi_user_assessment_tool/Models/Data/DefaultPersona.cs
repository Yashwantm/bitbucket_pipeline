﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class DefaultPersona
    {
        public string id { get; set; }
        public int profileAnswerId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string countryCode { get; set; }
        public List<int> weightedTags { get; set; }
        public List<DefaultPersonaAnswer> answers { get; set; }
    }
}