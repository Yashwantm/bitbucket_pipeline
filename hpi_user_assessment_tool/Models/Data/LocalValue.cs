﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class LocalValue
    {
        public string country { get; set; }
        public string lang { get; set; }
        public string value { get; set; }
    }
}