﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class SelectedProduct
    {
        public string SKU { get; set; }
        public string itemId { get; set; }
    }
}