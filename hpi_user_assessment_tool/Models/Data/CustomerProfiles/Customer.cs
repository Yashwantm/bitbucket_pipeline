﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class Customer
    {
        public string id { get; set; }
        public string name { get; set; }
        public string size { get; set; }
        public string industry { get; set; }
        public List<Profile> profiles { get; set; }
    }
}