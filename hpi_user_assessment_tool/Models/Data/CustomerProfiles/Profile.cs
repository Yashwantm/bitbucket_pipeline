﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class Profile
    {
        public string id { get; set; }
        public string name { get; set; }
        public int defaultPersonaId { get; set; }
        public List<ProfileAnswer> answers { get; set; }
        public SelectedProduct selectedProduct { get; set; }
        public List<SelectedAccessories> selectedAccessories { get; set; }
    }
}