﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class SelectedAccessories
    {
        public string category { get; set; }
        public string SKU { get; set; }
    }
}