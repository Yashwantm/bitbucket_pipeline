﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class UserProfile
    {
        public string id { get; set; }
        public string userId { get; set; }
        public List<Customer> customers { get; set; }
        public string _self { get; set; }
    }
}
