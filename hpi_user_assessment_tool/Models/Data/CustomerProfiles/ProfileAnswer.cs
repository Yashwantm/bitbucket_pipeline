﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class ProfileAnswer
    {
        public string questionId { get; set; }
        public string answerId { get; set; }
    }
}