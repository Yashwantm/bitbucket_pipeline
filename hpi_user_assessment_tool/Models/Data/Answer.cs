﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class Answer
    {
        public string id { get; set; }
        public string label { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public bool enabled { get; set; }
        public string imageURL { get; set; }
        public List<LocalValue> text { get; set; }
        public List<int> tags { get; set; }
        public List<int> weightedTags { get; set; }
        public List<Insight> insights { get; set; }
    }
}