﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Models
{
    public class AssessmentStep
    {
        public string id { get; set; }
        public string label { get; set; }
        public string description { get; set; }
    }
}