//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace hpi_user_assessment_tool
{
    using System;
    using System.Collections.Generic;
    
    public partial class CMSReliantStandaloneWeightedTag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CMSReliantStandaloneWeightedTag()
        {
            this.CMSReliantStandaloneWeightedTagRules = new HashSet<CMSReliantStandaloneWeightedTagRule>();
        }
    
        public int Id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string weight { get; set; }
        public string label { get; set; }
        public string appliedOnTable { get; set; }
        public string multiplierColumn { get; set; }
        public Nullable<bool> inverseMultiplier { get; set; }
        public string appliedOnEntity { get; set; }
        public string deviceAppliedOnTable { get; set; }
        public string deviceAppliedOnColumn { get; set; }
        public string deviceCondition { get; set; }
        public string deviceCriteria { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CMSReliantStandaloneWeightedTagRule> CMSReliantStandaloneWeightedTagRules { get; set; }
    }
}
