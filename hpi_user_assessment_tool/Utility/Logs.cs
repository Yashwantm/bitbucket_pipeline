﻿using hpi_user_assessment_tool.Utility.Appender;
using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Utility
{
    public class Logs
    {

        private static readonly ILog log = log4net.LogManager.GetLogger("TraceAppender");
        // private static readonly ILog logE = log4net.LogManager.GetLogger("RollingFileEventAppender");
        private AzureAppendBlobAppender _appender;

        public static void Info(string message)
        {
            log.Info(message);
        }

        public static void InfoFormat(string message)
        {
            log.InfoFormat(message);
        }

        public static void Debug(string message)
        {
            log.Debug(message);
        }

        public static void Error(string message)
        {
            log.Error(message);
        }

        public static void ErrorFormat(string message, Exception ex)
        {
            log.ErrorFormat("The following Error has occured: {0}, {1}", message, ex);
        }

        public static void ProfileLogs(string actionType, ICollection<USRSelectedAccessory> selectedAccessories, USRSelectedProduct selectedProduct, ICollection<USRProfileAnswer> profileAnswers)
        {
            string answerLabels = "";
            string selectedDockSkus = "";
            string selectedDisplaySkus = "";
            string selectedAccessoriesSkus = "";
            string selectedServiceSkus = "";
            if (profileAnswers != null && profileAnswers.Count > 0)
            {
                foreach (var profileAnswer in profileAnswers)
                {
                    if (profileAnswer != null && profileAnswer.USRProfileAnswerOptions != null && profileAnswer.USRProfileAnswerOptions.Count > 0)
                    {
                        foreach (var usrProfileAnswerOptions in profileAnswer.USRProfileAnswerOptions)
                        {
                            if (usrProfileAnswerOptions != null && usrProfileAnswerOptions.answerLabel != null)
                            {
                                if (!string.IsNullOrEmpty(answerLabels))
                                {
                                    answerLabels = answerLabels + ",";
                                }

                                answerLabels = answerLabels + usrProfileAnswerOptions.answerLabel;
                            }
                        }
                    }
                }
            }

            answerLabels = (answerLabels.Split(',').Count() > 1) ? "\"" + answerLabels + "\"" : answerLabels;

            if (selectedAccessories != null && selectedAccessories.Count > 0)
            {
                foreach (var selectedAccessory in selectedAccessories)
                {
                    if (selectedAccessory != null && selectedAccessory.USRSelectedAccessoriesSkus != null && selectedAccessory.USRSelectedAccessoriesSkus.Count > 0)
                    {
                        foreach (var usrSelectedAccessorySku in selectedAccessory.USRSelectedAccessoriesSkus)
                        {
                            if (usrSelectedAccessorySku != null && usrSelectedAccessorySku.sku != null)
                            {
                                if ("dock".Equals(selectedAccessory.category))
                                {
                                    if (!string.IsNullOrEmpty(selectedDockSkus))
                                    {
                                        selectedDockSkus = selectedDockSkus + ",";
                                    }

                                    selectedDockSkus = selectedDockSkus + usrSelectedAccessorySku.sku;
                                }
                                else if ("display".Equals(selectedAccessory.category))
                                {
                                    if (!string.IsNullOrEmpty(selectedDisplaySkus))
                                    {
                                        selectedDisplaySkus = selectedDisplaySkus + ",";
                                    }

                                    selectedDisplaySkus = selectedDisplaySkus + usrSelectedAccessorySku.sku;
                                }
                                else if ("accessories".Equals(selectedAccessory.category))
                                {
                                    if (!string.IsNullOrEmpty(selectedAccessoriesSkus))
                                    {
                                        selectedAccessoriesSkus = selectedAccessoriesSkus + ",";
                                    }

                                    selectedAccessoriesSkus = selectedAccessoriesSkus + usrSelectedAccessorySku.sku;
                                }
                                else if ("service".Equals(selectedAccessory.category))
                                {
                                    if (!string.IsNullOrEmpty(selectedServiceSkus))
                                    {
                                        selectedServiceSkus = selectedServiceSkus + ",";
                                    }

                                    selectedServiceSkus = selectedServiceSkus + usrSelectedAccessorySku.sku;
                                }
                            }
                        }
                    }
                }
            }

            selectedDockSkus = (selectedDockSkus.Split(',').Count() > 1) ? "\"" + selectedDockSkus + "\"" : selectedDockSkus;
            selectedDisplaySkus = (selectedDisplaySkus.Split(',').Count() > 1) ? "\"" + selectedDisplaySkus + "\"" : selectedDisplaySkus;
            selectedAccessoriesSkus = (selectedAccessoriesSkus.Split(',').Count() > 1) ? "\"" + selectedAccessoriesSkus + "\"" : selectedAccessoriesSkus;
            selectedServiceSkus = (selectedServiceSkus.Split(',').Count() > 1) ? "\"" + selectedServiceSkus + "\"" : selectedServiceSkus;
            string dateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffffffzzz");
            string eventLogStr = dateTime+","+actionType + "," + answerLabels + "," + selectedProduct.sku + "," + selectedDockSkus + "," + selectedDisplaySkus + "," + selectedAccessoriesSkus + "," + selectedServiceSkus;
            try
            {
                Logs logs = new Logs();
                logs.Initialize(eventLogStr);
                logs.Blob_Appender();
            }
            catch (Exception ex)
            {
                Logs.Error("Exception occured while writing event logs to Server, [Error: " + ex.Message + ", eventLogStr: " + eventLogStr + ", exception: " + ex + "]");
            }

            // logE.Info();
        }

        public void Initialize(string eventLogStr)
        {
            _appender = new AzureAppendBlobAppender()
            {
                ConnectionString = ConfigurationManager.AppSettings["AzureEventLog_connectionString"],
                ContainerName = ConfigurationManager.AppSettings["AzureEventLog_containerName"],
                DirectoryName = "eventLogs",
                EventLogString = eventLogStr
            };
            _appender.ActivateOptions();
        }

        public void Blob_Appender()
        {
            var @event = MakeEvent();
            LoggingEvent[] events = new LoggingEvent[] { @event };
            _appender.SendBufferExe(events);
            //_appender.DoAppend(@event);
        }

        private static LoggingEvent MakeEvent()
        {
            return new LoggingEvent(
                new LoggingEventData
                {
                    TimeStamp = DateTime.UtcNow,
                    Level = Level.Critical,
                    Message = "testMessage",
                    LocationInfo = new LocationInfo("className", "methodName", "fileName", "lineNumber")
                }
                );
        }
    }
}