﻿using FluentValidation.Results;
using hpi_user_assessment_tool.Models.APIRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hpi_user_assessment_tool.Utility
{
    public class ValidatorUtil
    {
        public static string getErrorMSG(ValidationResult results)
        {
          string errorResponse = "";
            bool validationSucceeded = results.IsValid;
            IList<ValidationFailure> failures = results.Errors;
            if (!validationSucceeded)
            {
                foreach (ValidationFailure validationFailure in failures)
                {
                    if (string.IsNullOrEmpty(errorResponse))
                    {
                        errorResponse = validationFailure.ErrorMessage;
                    }
                    else if (!string.IsNullOrEmpty(validationFailure.ErrorMessage))
                    {
                        errorResponse = errorResponse + "," + validationFailure.ErrorMessage;
                    }
                }
            }
            return errorResponse;
        }
    }
}