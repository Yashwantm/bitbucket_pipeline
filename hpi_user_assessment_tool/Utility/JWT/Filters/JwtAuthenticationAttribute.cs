﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Services;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Linq;
using hpi_user_assessment_tool.Data;
using System.Net.Http;
using hpi_user_assessment_tool.Utility;

namespace WebApi.Jwt.Filters
{
    public class JwtAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        // private readonly ICustomerProfileService customerProfileService;

        //public JwtAuthenticationAttribute(ICustomerProfileService customerProfileService)
        //{
        //    this.customerProfileService = customerProfileService;
        //}

        public string Realm { get; set; }
        public bool AllowMultiple => false;

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            try
            {
                Logs.Info("JwtAuthenticationAttribute.AuthenticateAsync method has been called having parameters [context, cancellationToken]");
                string token = "";
                var request = context.Request;
                var headers = context.ActionContext.Request.Headers;
                var cookie = headers.GetCookies().Select(c => c[Constants.JWT_TOKEN_COOKIE_NAME]).FirstOrDefault();
                if (cookie == null || string.IsNullOrEmpty(cookie.Value))
                {
                    var authorization = request.Headers.Authorization;

                    if (authorization == null || authorization.Scheme != "Bearer" || string.IsNullOrEmpty(authorization.Parameter))
                    {
                        context.ErrorResult = new AuthenticationFailureResult("Missing Jwt Token", request);
                        Logs.Error("The following error has occured: Missing Jwt Token. " + Constants.UNAUTHORIZED_ACCESS_MSG);
                        throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
                    }
                    // If cookie doesnot exist in the request then validate it through authorization header
                    token = authorization.Parameter;
                }
                else
                {
                    // If cookie exist in the request then validate it through cookie only
                    token = cookie.Value;
                }

                var principal = await AuthenticateJwtToken(token);

                if (principal == null)
                {
                    context.ErrorResult = new AuthenticationFailureResult("Invalid token", request);
                    Logs.Error("The following error has occured: Invalid token. " + Constants.UNAUTHORIZED_ACCESS_MSG);
                    throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
                }

                else
                    context.Principal = principal;
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
            }
        }

        public bool ValidateToken(string token, out UserInfo userInfo)
        {
            userInfo = new UserInfo();
            var simplePrinciple = JwtManager.GetPrincipal(token);

            if (simplePrinciple == null)
            {
                Logs.Error("The following error has occured: " + Constants.UNAUTHORIZED_ACCESS_MSG);
                throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
            }

            var identity = simplePrinciple.Identity as ClaimsIdentity;

            if (identity == null)
                return false;

            if (!identity.IsAuthenticated)
                return false;

            var userId = identity.FindFirst(ClaimTypes.UserData);
            userInfo.userId = userId?.Value;

            var emailId = identity.FindFirst(ClaimTypes.Email);
            userInfo.emailId = emailId?.Value;
            Logs.Info("JwtAuthenticationAttribute.ValidateToken method has been called having parameters  [token, userInfo: {userId: " + userInfo.userId + ", userEmail: " + userInfo.emailId + "}]");
            if (string.IsNullOrEmpty(userInfo.userId) || string.IsNullOrEmpty(userInfo.emailId))
                return false;

            ICustomerProfileService customerProfileService = new CustomerProfileService(new CustomerProfileRepository());
            // if restrict user is set as true then validate the email address with allowed user list
            CMSRestrictedAccess cmsRestrictedAccess = customerProfileService.GetRestrictedAccess();
            if ((bool)cmsRestrictedAccess.isRestrictedAccess)
            {
                string emailAddressStr = userInfo.emailId;
                List<CMSAllowedUser> cmsAllowedUserList = customerProfileService.GetAllAllowedUsers();
                CMSAllowedUser cmsAllowedUser = cmsAllowedUserList.SingleOrDefault(p => string.Equals(p.emailAddress, emailAddressStr, StringComparison.CurrentCultureIgnoreCase));
                if (cmsAllowedUser == null)
                {
                    Logs.Error(Constants.UNAUTHORIZED_ACCESS_MSG);
                    throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
                }
            }

            Logs.Info("User Authenticated successfully !!!");
            return true;
        }

        public Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            Logs.Info("JwtAuthenticationAttribute.AuthenticateJwtToken method has been called having parameter [token: " + token + "]");
            UserInfo userInfo;
            if (ValidateToken(token, out userInfo))
            {
                // based on userId to get more information in order to build local identity
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.UserData, userInfo.userId),
                    new Claim(ClaimTypes.Email, userInfo.emailId)
                };

                var identity = new ClaimsIdentity(claims, "Jwt");
                IPrincipal user = new ClaimsPrincipal(identity);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Logs.Debug("JwtAuthenticationAttribute.ChallengeAsync method has been called having parameters [context, cancellationToken].");
            Challenge(context);
            return Task.FromResult(0);
        }

        private void Challenge(HttpAuthenticationChallengeContext context)
        {
            Logs.Debug("JwtAuthenticationAttribute.Challenge method has been called having parameter [context].");
            string parameter = null;

            if (!string.IsNullOrEmpty(Realm))
                parameter = "realm=\"" + Realm + "\"";

            //context.ChallengeWith("Bearer", parameter);
        }
    }
}
