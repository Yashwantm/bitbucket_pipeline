﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using hpi_user_assessment_tool.Models.APIRequest;
using System.Security.Cryptography;
using System.Configuration;
using System.Web.Http.Controllers;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Jwt
{
    public static class JwtManager
    {
        public static string GenerateToken(UserInfo userInfo, TimeSpan expirationSpan)
        {
            // var hmac = new HMACSHA256();
            // var key = Convert.ToBase64String(hmac.Key);

            var symmetricKey = Convert.FromBase64String(ConfigurationManager.AppSettings["JWTSecretKey"]);
            var tokenHandler = new JwtSecurityTokenHandler();

            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature);
            var JwtHeader = new JwtHeader(signingCredentials);

            var nowTime = DateTime.UtcNow;

            var nowTicks = nowTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var expTicks = nowTicks + expirationSpan;

            var payload = new System.IdentityModel.Tokens.Jwt.JwtPayload
            {
                { "iss", "UAT" }, // Issuer
                { "aud", "HPSC" }, // Intended audience (recipient)
                { "iat", (int)nowTicks.TotalSeconds }, // The iat (issued at) claim identifies the time at which the JWT was issued
                { "exp", (int)expTicks.TotalSeconds },
                { "jti", Guid.NewGuid() }, // The jti (JWT ID) claim provides a unique identifier for the JWT
                { ClaimTypes.UserData, userInfo.userId },
                { ClaimTypes.Email, userInfo.emailId }
            };

            var stoken = new System.IdentityModel.Tokens.Jwt.JwtSecurityToken(JwtHeader, payload);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(ConfigurationManager.AppSettings["JWTSecretKey"]);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey),
                    ClockSkew = TimeSpan.Zero
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public static UserInfo GetUserInfoFromRequestContext(HttpRequestContext requestContext)
        {
            UserInfo userInfo = new UserInfo();
            IEnumerable<Claim> claims = ((System.Security.Claims.ClaimsPrincipal)requestContext.Principal).Claims;

            userInfo.userId = claims.FirstOrDefault(p => p.Type == ClaimTypes.UserData).Value;
            userInfo.emailId = claims.FirstOrDefault(p => p.Type == ClaimTypes.Email).Value;

            return userInfo;
        }

    }
}