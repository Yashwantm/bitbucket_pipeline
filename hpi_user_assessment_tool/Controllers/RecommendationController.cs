﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Data;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Services;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.Response;
using Newtonsoft.Json;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using FluentValidation.Results;
using WebApi.Jwt.Filters;
using hpi_user_assessment_tool.Utility;

namespace hpi_user_assessment_tool.Controllers
{
    //TODO: Need to add Authorization for the API's
    // [Authorize]Recommendation
    [JwtAuthentication]
    [RoutePrefix("api/v1")]
    public class RecommendationController : ApiController
    {

        private readonly IRecommendationService recommendationService;

        public RecommendationController(IRecommendationService recommendationService)
        {
            this.recommendationService = recommendationService;
        }

        [Route("product/recommendation")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedSkus([FromBody] Recommendation recommendation)
        {
            try
            {
                Logs.Info("RecommendationController.getRecommendedSkus /product/recommendation api has been called having parameter [Recommendation]");
                string errorMSG = new SKURecommendationValidator().validateRequest(recommendation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                RecommendationProductSkuDTOResponse response = recommendationService.GetRecommendedSkusAsync(recommendation);
                if (recommendation.countOnly)
                {
                    Logs.Info("The response of RecommendationController.getRecommendedSkus /product/recommendation api is, Response[ response.recommendations, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                    return Ok(response.recommendations.Count);
                }
                else
                {
                    Logs.Info("The response of RecommendationController.getRecommendedSkus /product/recommendation api is, Response[ response.recommendations, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                    return Ok(response);
                }

            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("product/accessories/docks")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedDocks([FromBody] Recommendation recommendation)
        {
            try
            {
                Logs.Info("RecommendationController.getRecommendedDocks /product/accessories/docks api has been called having parameter [Recommendation]");
                string errorMSG = new AccessoryRecommendationValidator().validateRequest(recommendation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                RecommendationProductAccessoryDTOResponse response = recommendationService.GetRecommendedAccessoriesAsync(recommendation, Constants.ACCESSORY_TYPE_DOCK);
                Logs.Info("The response of RecommendationController.getRecommendedDocks /product/accessories/docks api is, Response[ response.recommendation, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("product/accessories/displays")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedDisplays([FromBody] Recommendation recommendation)
        {
            try
            {
                Logs.Info("RecommendationController.getRecommendedDisplays /product/accessories/displays api has been called having parameter [Recommendation]");
                string errorMSG = new AccessoryRecommendationValidator().validateRequest(recommendation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                RecommendationProductAccessoryDTOResponse response = recommendationService.GetRecommendedAccessoriesAsync(recommendation, Constants.ACCESSORY_TYPE_DISPLAY);
                Logs.Info("The response of RecommendationController.getRecommendedDisplays /product/accessories/displays api is, Response[ response.recommendations, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("product/accessories/accessories")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedAccessories([FromBody] Recommendation recommendation)
        {
            try
            {
                Logs.Info("RecommendationController.getRecommendedAccessories /product/accessories/accessories api has been called having parameter [Recommendation]");
                string errorMSG = new AccessoryRecommendationValidator().validateRequest(recommendation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                RecommendationProductAccessoryDTOResponse response = recommendationService.GetRecommendedAccessoriesAsync(recommendation, Constants.ACCESSORY_TYPE_ACCESSORY);
                Logs.Info("The response of RecommendationController.getRecommendedAccessories /product/accessories/accessories api is, Response[ response.recommendations, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("product/accessories/services")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedServices([FromBody] Recommendation recommendation)
        {
            try
            {
                Logs.Info("RecommendationController.getRecommendedServices /product/accessories/services api has been called having parameter [Recommendation]");
                string errorMSG = new AccessoryRecommendationValidator().validateRequest(recommendation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                RecommendationProductAccessoryDTOResponse response = recommendationService.GetRecommendedAccessoriesAsync(recommendation, Constants.ACCESSORY_TYPE_SERVICE);
                Logs.Info("The response of RecommendationController.getRecommendedServices /product/accessories/services api is, Response[ response.recommendations, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("product/accessories/daasServices")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedDaasServices([FromBody] Recommendation recommendation)
        {
            try
            {
                Logs.Info("RecommendationController.getRecommendedDaasServices /product/accessories/daasServices api has been called having parameter [Recommendation]");
                string errorMSG = new AccessoryRecommendationValidator().validateRequest(recommendation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                RecommendationProductDaasAccessoryDTOResponse response = recommendationService.GetRecommendedDaasServicesAsync(recommendation);
                Logs.Info("The response of RecommendationController.getRecommendedDaasServices /product/accessories/daasServices api is, Response[ response.recommendations, Size: " + ((response.recommendations == null) ? '-' : response.recommendations.Count) + "]");
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
