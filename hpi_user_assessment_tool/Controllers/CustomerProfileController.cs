﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Data;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Services;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.Response;
using Newtonsoft.Json;
using hpi_user_assessment_tool.Models.Validation;
using WebApi.Jwt.Filters;
using System.Web;
using System.Security.Claims;
using WebApi.Jwt;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Utility;
using hpi_user_assessment_tool.Models.DTO;
using Microsoft.WindowsAzure.Storage;
using System.Text;
using System.Net.Http.Headers;
using hpi_user_assessment_tool.Models.Constant;

namespace hpi_user_assessment_tool.Controllers
{
    [JwtAuthentication]
    [RoutePrefix("api/v1")]
    public class CustomerProfileController : ApiController
    {

        private readonly ICustomerProfileService customerProfileService;

        public CustomerProfileController(ICustomerProfileService customerProfileService)
        {
            this.customerProfileService = customerProfileService;
        }

        [Route("user")]
        [HttpPost]
        public async Task<IHttpActionResult> addCustomer([FromBody] USRInfoRequest usrInfoRequest)
        {
            try
            {
                Logs.Info("CustomerProfileController.addCustomer /user API has been called having parameter [ USRInfo ]");
                string errorMSG = new USRInfoValidator().validateRequest(usrInfoRequest);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);
                USRInfo usrInfo = new USRInfo();
                usrInfo.userId = tokenUserInfo.userId;
                usrInfo.emailid = tokenUserInfo.emailId;

                var addedUSRInfo = await customerProfileService.AddCustomer(usrInfo, usrInfoRequest);
                return Ok(addedUSRInfo);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user")]
        [HttpPut]
        public async Task<IHttpActionResult> updateCustomer([FromBody] USRInfoRequest usrInfoRequest)
        {
            try
            {
                Logs.Info("CustomerProfileController.updateCustomer /user PUT API has been called having parameter [ USRInfo ]");
                string errorMSG = new USRInfoValidator().validateRequest(usrInfoRequest);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);
                USRInfo usrInfo = new USRInfo();
                usrInfo.userId = tokenUserInfo.userId;
                usrInfo.emailid = tokenUserInfo.emailId;

                var updatedUSRInfo = await customerProfileService.UpdateCustomer(usrInfo, usrInfoRequest);
                Logs.Info("The response of CustomerProfileController.updateCustomer /user api is, Response[AddCustomerResponse, CustomerId: " + ((updatedUSRInfo == null) ? '-' : updatedUSRInfo.CustomerId) + "]");
                return Ok(updatedUSRInfo);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer")]
        [HttpGet]
        public async Task<IHttpActionResult> customer()
        {
            try
            {
                Logs.Info("CustomerProfileController.customer /user/customer api has been called.");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                List<USRCustomer> customer = customerProfileService.GetAllCustomerAsync(tokenUserInfo.userId);

                Logs.Info("The response of CustomerProfileController.customer /user/customer api is, Response[List<USRCustomer>, Size: " + ((customer == null) ? '-' : customer.Count) + "]");
                return Ok(customer);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/{customerId}")]
        [HttpGet]
        public async Task<IHttpActionResult> customerById(int customerId)
        {
            try
            {
                Logs.Info("CustomerProfileController.customerById /user/customer/{customerId} api has been called having parameter [customerId: " + customerId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                USRCustomer customer = customerProfileService.GetCustomerByIdAsync(customerId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.customerById /customer/{customerId} api is, Response[ USRCustomer, CustomerId: " + ((customer == null) ? '-' : customer.Id) + "]");

                return Ok(customer);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/{customerId}/profile")]
        [HttpGet]
        public async Task<IHttpActionResult> profile(int customerId)
        {
            try
            {
                Logs.Info("CustomerProfileController.profile /user/customer/{customerId}/profile api has been called having parameter[ customerId: " + customerId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                List<USRProfile> profile = customerProfileService.GetAllProfileAsync(customerId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.profile /customer/{customerId}/profile api is, Response[ List<USRProfile>, Size: " + ((profile == null) ? '-' : profile.Count) + "]");

                return Ok(profile);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/profile/{profileId}")]
        [HttpGet]
        public async Task<IHttpActionResult> profileById(int profileId)
        {
            try
            {
                Logs.Info("CustomerProfileController.profileById /profile/{profileId} api has been called having parameter[ profileId: " + profileId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                USRProfile profile = customerProfileService.GetProfileByIdAsync(profileId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.profileById /profile/{profileId} api is, Response[ USRProfile, profileId: " + ((profile == null) ? '-' : profile.Id) + "]");

                return Ok(profile);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/profile/{profileId}/validate")]
        [HttpGet]
        public async Task<IHttpActionResult> validateProfileById(int profileId)
        {
            try
            {
                Logs.Info("CustomerProfileController.validateProfileById /profile/{profileId}/validate api has been called having parameter[ profileId: " + profileId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                USRProfileResponse profile = customerProfileService.ValidateProfileByIdAsync(profileId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.validateProfileById /profile/{profileId}/validate api is, Response[ USRProfileResponse, profileId: " + ((profile == null) ? '-' : profile.Id) + "]");

                return Ok(profile);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/delete/{customerId}")]
        [HttpGet]
        public async Task<IHttpActionResult> deleteCustomerById(int customerId)
        {
            try
            {
                Logs.Info("CustomerProfileController.deleteCustomerById /user/customer/delete/{customerId} api has been called having parameter[ customerId: " + customerId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                string response = customerProfileService.DeleteCustomerByIdAsync(customerId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.deleteCustomerById /user/customer/delete/{customerId} api is, Response[ response: " + response + "]");

                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/profile/delete/{profileId}")]
        [HttpGet]
        public async Task<IHttpActionResult> deleteProfileById(int profileId)
        {
            try
            {
                Logs.Info("CustomerProfileController.deleteProfileById /user/customer/profile/delete/{profileId} api has been called having parameter[ profileId: " + profileId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                string response = customerProfileService.DeleteProfileByIdAsync(profileId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.deleteProfileById /user/customer/profile/delete/{profileId} api is, Response[ response: " + response + "]");

                return Ok(response);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("user/customer/profile/download/excel/{profileId}")]
        [HttpGet]
        public HttpResponseMessage downloadExcelProfileById(int profileId)
        {
            try
            {
                Logs.Info("CustomerProfileController.downloadProfileById user/customer/profile/download/excel/{profileId} api has been called having parameter[ profileId: " + profileId + "]");
                UserInfo tokenUserInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);

                HttpResponseMessage excelByteArray = customerProfileService.DownloadExcelProfileByIdAsync(profileId, tokenUserInfo);

                Logs.Info("The response of CustomerProfileController.downloadProfileById user/customer/profile/download/excel/{profileId} api is, Response[ response: file.xlsx");

                return excelByteArray;
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return null;
            }
        }

        [Route("user/customer/profile/generate/pdf/{profileId}")]
        [HttpGet]
        public async Task<IHttpActionResult> generatePDFProfileById(int profileId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    string token = "";
                    var headers = Request.Headers;
                    var cookie = headers.GetCookies().Select(c => c[Constants.JWT_TOKEN_COOKIE_NAME]).FirstOrDefault();
                    if (cookie == null || string.IsNullOrEmpty(cookie.Value))
                    {
                        var authorization = Request.Headers.Authorization;

                        if (authorization == null || authorization.Scheme != "Bearer" || string.IsNullOrEmpty(authorization.Parameter))
                        {
                            Logs.Error("The following error has occured: Missing Jwt Token. " + Constants.UNAUTHORIZED_ACCESS_MSG);
                            throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
                        }
                        // If cookie doesnot exist in the request then validate it through authorization header
                        token = authorization.Parameter;
                    }
                    else
                    {
                        // If cookie exist in the request then validate it through cookie only
                        token = cookie.Value;
                    }

                    UserInfo userInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);
                    USRProfile profile = customerProfileService.GetProfileByIdAsync(profileId, userInfo);
                    if (profile != null)
                    {
                        string fileName = profile.name.Replace(" ", "_") + "_Assessment";

                        foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                        {
                            fileName = fileName.Replace(c, '_');
                        }

                        //string url = Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/Report?profileId="+ profileId+ "&token="+token;
                        string url = Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/Report";
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["WebJobUrl"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        var encoding = new ASCIIEncoding();
                        var authHeader = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(encoding.GetBytes(string.Format("{0}:{1}", ConfigurationManager.AppSettings["WebJobUser"], ConfigurationManager.AppSettings["WebJobPass"]))));
                        client.DefaultRequestHeaders.Authorization = authHeader;
                        var content = new System.Net.Http.StringContent("");
                        HttpResponseMessage response = client.PostAsync(string.Format("api/triggeredwebjobs/{0}/run?arguments={1} {2} {3} {4}", ConfigurationManager.AppSettings["WebJobName"], url, fileName, profileId, token), content).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            return Json(new { message = String.Format("Conversion for {0} {1} failed: {2}", url, fileName, DateTime.Now.ToString()) });
                        }
                        var historyCheck = response.Headers.Location.AbsolutePath;
                        var historyContent = new System.Net.Http.StringContent("");

                        response = client.GetAsync(historyCheck).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            return BadRequest("");
                        }

                        return Ok(Request.RequestUri.GetLeftPart(UriPartial.Authority) + "/api/v1/user/customer/profile/download/pdf/download/{profileId}");
                    }
                    return BadRequest("");
                }
            }
            catch (Exception ex) { return Json(new { message = ex.Message }); }
            return BadRequest("");
        }


        [Route("user/customer/profile/download/pdf/{profileId}")]
        [HttpGet]
        public HttpResponseMessage downloadPDFProfileById(int profileId)
        {
            try
            {
                UserInfo userInfo = JwtManager.GetUserInfoFromRequestContext(RequestContext);
                USRProfile profile = customerProfileService.GetProfileByIdAsync(profileId, userInfo);
                string fileName = profile.name.Replace(" ", "_") + "_Assessment";
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                foreach (char c in System.IO.Path.GetInvalidFileNameChars())
                {
                    fileName = fileName.Replace(c, '_');
                }
                var account = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                var blobContainer = account.CreateCloudBlobClient().GetContainerReference("pdf");

                if (!blobContainer.Exists())
                {
                    Logs.Error("Blobcontainer not exist");
                    return null;
                }

                var blob = blobContainer.GetBlockBlobReference(fileName);

                int totalWait = 0;
                var blobExists = blob.ExistsAsync().Result;
                while (blobExists == false)
                {
                    if (totalWait >= 20000)
                    {
                        break;
                    }
                    System.Threading.Thread.Sleep(2000);
                    totalWait = totalWait + 2000;
                    blobExists = blob.ExistsAsync().Result;
                }

                if (!blobExists)
                {
                    Logs.Error("Blob not exist");
                    return null;
                }

                System.IO.Stream blobStream = blob.OpenReadAsync().Result;
                blobStream.Position = 0;
                byte[] buffer = new byte[blobStream.Length];
                for (int totalBytesCopied = 0; totalBytesCopied < blobStream.Length;)
                    totalBytesCopied += blobStream.Read(buffer, totalBytesCopied, Convert.ToInt32(blobStream.Length) - totalBytesCopied);

                response.Content = new ByteArrayContent(buffer);
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                return response;
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return null;
            }
        }
    }
}
