﻿using hpi_user_assessment_tool.Data;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Services;
using Microsoft.WindowsAzure.Storage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Http.Controllers;
using WebApi.Jwt;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Utility;
using WebApi.Jwt.Filters;
using System.Security.Claims;
using hpi_user_assessment_tool.ViewModel;
using hpi_user_assessment_tool.Models.DTO;
using System.Resources;
using System.Globalization;
using System.Reflection;

namespace hpi_user_assessment_tool.Controllers
{
    public class ReportController : Controller
    {
        public static CustomerProfileRepository customerProfileRepository = new CustomerProfileRepository();
        public static RecommendationRepository recommendationRepository = new RecommendationRepository();
        public static QuestionRepository questionRepository = new QuestionRepository();

        public CustomerProfileService customerProfileService = new CustomerProfileService(customerProfileRepository, recommendationRepository);


        //GET: Report
        public async System.Threading.Tasks.Task<ActionResult> Index(int profileId, string token)
        {
            try
            {
                JwtAuthenticationAttribute jwtAuthentication = new JwtAuthenticationAttribute();


                //string token = "";
                //HttpCookie authCookie = System.Web.HttpContext.Current.Request.Cookies["devicewisetoken"];

                //if (authCookie == null || string.IsNullOrEmpty(authCookie.Value))
                //{
                //    string authHeader = System.Web.HttpContext.Current.Request.Headers["Authorization"];

                //    if (authHeader == null || !authHeader.StartsWith("Bearer"))
                //    {
                //        Logs.Error("The following error has occured: Missing Jwt Token. " + Constants.UNAUTHORIZED_ACCESS_MSG);
                //        throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
                //    }
                //    // If cookie doesnot exist in the request then validate it through authorization header
                //    var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);
                //    token = authHeaderVal.Parameter;
                //}
                //else
                //{
                //    // If cookie exist in the request then validate it through cookie only
                //    token = authCookie.Value;
                //}

                var principal = await jwtAuthentication.AuthenticateJwtToken(token);

                if (principal == null)
                {
                    //context.ErrorResult = new AuthenticationFailureResult("Invalid token", request);
                    Logs.Error("The following error has occured: Invalid token. " + Constants.UNAUTHORIZED_ACCESS_MSG);
                    throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
                }
                else
                {
                    ResourceManager rm = new ResourceManager("hpi_user_assessment_tool.UAT_resources", Assembly.GetExecutingAssembly());
                    String ImageServerURL = rm.GetString("DaasServicesImageServerURL", CultureInfo.CurrentCulture);

                    //TODO: Utilize the JwtManager.GetUserInfoFromRequestContext and update it pass principal
                    UserInfo userInfo = new UserInfo();
                    IEnumerable<Claim> claims = ((System.Security.Claims.ClaimsPrincipal)principal).Claims;

                    userInfo.userId = claims.FirstOrDefault(p => p.Type == ClaimTypes.UserData).Value;
                    userInfo.emailId = claims.FirstOrDefault(p => p.Type == ClaimTypes.Email).Value;

                    AccessoriesProducts accessoriesProducts = new AccessoriesProducts();
                    USRProfile profile = customerProfileService.GetProfileByIdAsync(profileId, userInfo);
                    if (profile != null)
                    {
                        USRCustomer customer = customerProfileService.GetCustomerByIdAsync(profile.CustomerId, userInfo);

                        accessoriesProducts.SelectedProduct = customerProfileService.GetSelectedProductDetails(profile.USRSelectedProduct.sku);
                        List<ProductAccessoryDTO> productAccessoryDTO = new List<ProductAccessoryDTO>();
                        List<ProductDaasServiceDTO> productDaasServiceDTO = new List<ProductDaasServiceDTO>();
                        foreach (var accessory in profile.SelectedAccessories)
                        {
                            List<string> accessorySkuList = accessory.USRSelectedAccessoriesSkus.Select(p => p.sku).ToList();

                            if (Constants.ASSESSMENT_ACCESSORY_TYPE_DAAS_SERVICE.Equals(accessory.category))
                            {

                                List<ProductDaasServiceDTO> productDaasServiceDTOList = customerProfileService.GetDaasServicesDetails(accessorySkuList);
                                foreach (ProductDaasServiceDTO productDaasServiceDTOObj in productDaasServiceDTOList)
                                {
                                    productDaasServiceDTOObj.primaryImageUrl = ImageServerURL + productDaasServiceDTOObj.primaryImageUrl;
                                    productDaasServiceDTO.Add(productDaasServiceDTOObj);
                                }
                            }
                            else
                            {
                                List<ProductAccessoryDTO> accessoryDTOList = customerProfileService.GetAccessoriesDetails(accessorySkuList, accessory.category);
                                foreach (ProductAccessoryDTO productAccessory in accessoryDTOList)
                                {
                                    productAccessoryDTO.Add(productAccessory);
                                }
                            }
                        }
                        accessoriesProducts.AccessoryList = productAccessoryDTO;
                        accessoriesProducts.productDaasServiceDTO = productDaasServiceDTO;
                        accessoriesProducts.profile = profile;
                        accessoriesProducts.customerName = customer.name;
                        setSelectedAnswersText(accessoriesProducts, profile);

                        return View("~/Views/Report/Report.cshtml", accessoriesProducts);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(Constants.UNAUTHORIZED_ACCESS_MSG);
            }
            return View();
        }

        public static void setSelectedAnswersText(AccessoriesProducts accessoriesProducts, USRProfile profile)
        {
            string country = "us";
            string language = "en";
            List<string> questionLabelListToDisplay = new List<string> { "question_security", "question_userWorkplace", "question_supportedOS", "question_userWorkplaceEnvironment", "question_userWorkstyle", "question_onlineCollaboration", "question_userWorkstyleInput", "question_userWorkstylePorts" };
            List<string> answerLabelListToDisplay = new List<string>();
            foreach (var usrProfileAnswer in profile.ProfileAnswers.Where(p => questionLabelListToDisplay.Contains(p.questionLabel)).ToList<USRProfileAnswer>())
            {
                answerLabelListToDisplay.AddRange(usrProfileAnswer.USRProfileAnswerOptions.Select(p => p.answerLabel));
            }

            List<CMSAnswer> cmsSelectedAnswersDB = recommendationRepository.GetAnswerByLabels(answerLabelListToDisplay).Result;
            Dictionary<string, string> answersDictionary = new Dictionary<string, string>();

            foreach (var question in profile.ProfileAnswers)
            {
                if (questionLabelListToDisplay.Contains(question.questionLabel))
                {
                    string answerLabelStr = "";
                    string answerKey = "";
                    foreach (USRProfileAnswerOptions answerLabel in question.USRProfileAnswerOptions)
                    {
                        CMSAnswer cmsAnswer = cmsSelectedAnswersDB.SingleOrDefault(p => p.label == answerLabel.answerLabel);
                        Local localTemp = questionRepository.GetLocalAsync(country, language, cmsAnswer.localKey).Result;
                        string localKey = localTemp == null ? "" : localTemp.value;

                        if (!string.IsNullOrEmpty(answerLabelStr))
                        {
                            answerLabelStr = answerLabelStr + ";" + localKey;
                        }
                        else
                        {
                            answerLabelStr = localKey;
                        }
                    }

                    switch (question.questionLabel)
                    {
                        case "question_security":
                            answerKey = "Security Requirements";
                            break;
                        case "question_userWorkplace":
                            answerKey = "Mobility";
                            break;
                        case "question_supportedOS":
                            answerKey = "Operating Systems";
                            break;
                        case "question_userWorkplaceEnvironment":
                            answerKey = "Work Environment";
                            break;
                        case "question_userWorkstyle":
                            answerKey = "Application Usage";
                            break;
                        case "question_onlineCollaboration":
                            answerKey = "Online Collaboration";
                            break;
                        case "question_userWorkstyleInput":
                            answerKey = "Critical Requirements";
                            break;
                        case "question_userWorkstylePorts":
                            answerKey = "Critical Requirements1";
                            break;
                    }

                    answersDictionary.Add(answerKey, answerLabelStr);
                }
            }
            string criticalRequirements = "";
            string criticalRequirements1 = "";
            answersDictionary.TryGetValue("Critical Requirements", out criticalRequirements);
            answersDictionary.TryGetValue("Critical Requirements1", out criticalRequirements1);
            if (string.IsNullOrEmpty(criticalRequirements))
            {
                criticalRequirements = criticalRequirements1;
            }
            else
            {
                criticalRequirements = criticalRequirements + ";" + criticalRequirements1;
            }
            answersDictionary["Critical Requirements"] = criticalRequirements;
            answersDictionary.Remove("Critical Requirements1");

            accessoriesProducts.selectedAnswerText = answersDictionary;

        }
    }
}