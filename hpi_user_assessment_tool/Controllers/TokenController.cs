﻿using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Services;
using System;
using System.Configuration;
using System.Net;
using System.Web;
using System.Web.Http;
using hpi_user_assessment_tool.Utility;

namespace WebApi.Jwt.Controllers
{
    [RoutePrefix("api/v1/user")]
    public class TokenController : ApiController
    {
        private readonly ICustomerProfileService customerProfileService;
        public TokenController(ICustomerProfileService customerProfileService)
        {
            this.customerProfileService = customerProfileService;
        }

        [AllowAnonymous]
        [Route("getToken")]
        [HttpPost]
        public IHttpActionResult getToken(UserInfo userInfo)
        {
            try
            {
                Logs.Info("TokenController.getToken /getToken API has been called having userInfo as an argument.");
                string errorMSG = new UserInfoValidator(customerProfileService).validateRequest(userInfo);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                // Setting token expiration time
                var expirationSpan = new TimeSpan(
                    Convert.ToInt32(ConfigurationManager.AppSettings["JWTTokenExpiry_days"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["JWTTokenExpiry_hours"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["JWTTokenExpiry_minutes"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["JWTTokenExpiry_seconds"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["JWTTokenExpiry_milliseconds"]));
                string token = JwtManager.GenerateToken(userInfo, expirationSpan);
                if (!string.IsNullOrEmpty(token))
                {
                    Logs.InfoFormat("The response of TokenController.getToken /getToken api is: '" + token + "'");
                    return Ok(token);
                }
                else
                {
                    Logs.Error("An error has occured");
                    return BadRequest("");
                }
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
