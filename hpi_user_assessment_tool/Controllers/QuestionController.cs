﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Data;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Services;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.Cors;
using WebApi.Jwt.Filters;
using hpi_user_assessment_tool.Utility;
using hpi_user_assessment_tool.Models.Response;

namespace hpi_user_assessment_tool.Controllers
{
    //TODO: Need to add Authorization for the API's
    // [Authorize]
    [JwtAuthentication]
    [RoutePrefix("api/v1")]
    public class QuestionController : ApiController
    {

        private readonly IQuestionService questionService;

        public QuestionController(IQuestionService questionService)
        {
            this.questionService = questionService;
        }

        [Route("question")]
        [HttpGet]
        public async Task<IHttpActionResult> question()
        {
            try
            {
                Logs.Info("QuestionController.question /api/v1/question API has been called.");
                GetAllQuestionsResponse getAllQuestionsResponse = questionService.GetAllQuestionAsync();
                Logs.Info("The response of QuestionController.question /api/v1/question api is, Response [GetAllQuestionsResponse: " + getAllQuestionsResponse + "]");
                return Ok(getAllQuestionsResponse);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("question/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> question(int id)
        {
            try
            {
                Logs.Info("QuestionController.question /question/{0} API has been called having parameter [id: " + id + "]");
                CMSQuestion question = await questionService.GetQuestionByIdAsync(id);

                Logs.Info("The response of QuestionController.question /question/{" + id + "} api is, Response [ CMSQuestion, CMSQuestionId: " + ((question == null) ? '-' : question.Id) + "]");
                return Ok(question);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
