﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Data;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Services;
using hpi_user_assessment_tool.Models;
using hpi_user_assessment_tool.Models.Response;
using Newtonsoft.Json;
using hpi_user_assessment_tool.Models.DTO;
using hpi_user_assessment_tool.Models.APIRequest;
using hpi_user_assessment_tool.Models.Constant;
using hpi_user_assessment_tool.Utility;
using WebApi.Jwt.Filters;


namespace hpi_user_assessment_tool.Controllers
{
    //TODO: Need to add Authorization for the API's
    // [Authorize]Validation
    [JwtAuthentication]
    [RoutePrefix("api/v1")]
    public class ValidationController : ApiController
    {

        private readonly IValidationService validationService;

        public ValidationController(IValidationService validationService)
        {
            this.validationService = validationService;
        }

        [Route("validation/tags")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllTags()
        {
            try
            {
                Logs.Info("ValidationController.GetAllTags /validation/tags api has been called");
                List<AnswerTagsDTO> answerTagDTOList = validationService.GetAllTags();
                Logs.Info("The response of ValidationController.GetAllTags /validation/tags is, Response[ List<AnswerTagsDTO>, Size: " + ((answerTagDTOList == null) ? '-' : answerTagDTOList.Count) + "]");
                return Ok(answerTagDTOList);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/tags/answers")]
        [HttpPost]
        public async Task<IHttpActionResult> getTagsByAnswersId([FromBody] Validation validation)
        {
            try
            {
                Logs.Info("ValidationController.getTagsByAnswersId /validation/tags/answers api has been called having parameters [List<int> Validation.answerIds, Size: " + ((validation == null || validation.answerIds == null) ? '-' : validation.answerIds.Count) + "]");
                string errorMSG = new TagsValidationValidator().validateRequest(validation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<AnswerTagsDTO> answerTagDTOList = validationService.GetTagsByAnswersId(validation.answerIds);
                Logs.Info("The response of ValidationController.getTagsByAnswersId /validation/tags/answers is, Response[List<AnswerTagsDTO>, Size: " + ((answerTagDTOList == null) ? '-' : answerTagDTOList.Count) + "]");
                return Ok(answerTagDTOList);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/tags/filtertags")]
        [HttpPost]
        public async Task<IHttpActionResult> getFilterTagsById([FromBody] Validation validation)
        {
            try
            {
                Logs.Info("ValidationController.getFilterTagsById /validation/tags/filtertags api has been called having parameters [List<int> Validation.filterTagIds, Size: " + ((validation == null || validation.filterTagIds == null) ? '-' : validation.filterTagIds.Count) + "]");
                string errorMSG = new FilterTagsValidationValidator().validateRequest(validation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<CMSTag> cmsTagList = validationService.GetFilterTagsByIdAsync(validation.filterTagIds).Result;
                Logs.Info("The response of ValidationController.getFilterTagsById /validation/tags/filtertags api is, Response[List<CMSTag>, Size: " + ((cmsTagList == null) ? '-' : cmsTagList.Count) + "]");
                return Ok(cmsTagList);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/tags/weightedtags")]
        [HttpPost]
        public async Task<IHttpActionResult> getWeightedTagsById([FromBody] Validation validation)
        {
            try
            {
                Logs.Info("ValidationController.getWeightedTagsById /validation/tags/weightedtags api has been called having parameters  [List<int> Validation.weightedTagIds, Size: " + ((validation == null || validation.weightedTagIds == null) ? '-' : validation.weightedTagIds.Count) + "]");
                string errorMSG = new WeightedTagsValidationValidator().validateRequest(validation);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<CMSWeightedTag> cmsWeightedTagList = validationService.GetWeightedTagsByIdAsync(validation.weightedTagIds).Result;
                Logs.Info("The response of ValidationController.getWeightedTagsById /validation/tags/weightedtags api is, Response[List<CMSWeightedTag>, Size: " + ((cmsWeightedTagList == null) ? '-' : cmsWeightedTagList.Count) + "]");
                return Ok(cmsWeightedTagList);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }


        [Route("validation/runscenarios/skus")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedSkusScenarios([FromBody] RecommendationScenarios recommendationScenarios)
        {
            try
            {
                Logs.Info("ValidationController.getRecommendedSkusScenarios /validation/runscenarios/skus api has been called having parameter [recommendationScenarios]");
                string errorMSG = new SKURecommendationScenariosValidator().validateRequest(recommendationScenarios);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = validationService.GetRecommendedSkusScenarios(recommendationScenarios);
                Logs.InfoFormat("The response of ValidationController.getRecommendedSkusScenarios /validation/runscenarios/skus api is, Response[List<ProductSkuScenarioResponseDTO>, Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
                return Ok(productSkuScenarioResponseDTOList);

            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/runscenarios/docks")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedDocksScenarios([FromBody] RecommendationScenarios recommendationScenarios)
        {
            try
            {
                Logs.Info("ValidationController.getRecommendedDocksScenarios /validation/runscenarios/docks api has been called having parameter [recommendationScenarios]");
                string errorMSG = new AccessoryRecommendationScenariosValidator().validateRequest(recommendationScenarios);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = validationService.GetRecommendedAccessoriesScenarios(recommendationScenarios, Constants.ACCESSORY_TYPE_DOCK);
                Logs.InfoFormat("The response of ValidationController.getRecommendedDocksScenarios /validation/runscenarios/docks api is, Response[List<ProductSkuScenarioResponseDTO>, Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
                return Ok(productSkuScenarioResponseDTOList);

            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/runscenarios/displays")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedDisplaysScenarios([FromBody] RecommendationScenarios recommendationScenarios)
        {
            try
            {
                Logs.Info("ValidationController.getRecommendedDisplaysScenarios /validation/runscenarios/docks api has been called having parameter [recommendationScenarios]");
                string errorMSG = new AccessoryRecommendationScenariosValidator().validateRequest(recommendationScenarios);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = validationService.GetRecommendedAccessoriesScenarios(recommendationScenarios, Constants.ACCESSORY_TYPE_DISPLAY);
                Logs.Info("The response of ValidationController.getRecommendedDisplaysScenarios /validation/runscenarios/docks api is, Response[List<ProductSkuScenarioResponseDTO>, Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
                return Ok(productSkuScenarioResponseDTOList);

            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/runscenarios/services")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedServicesScenarios([FromBody] RecommendationScenarios recommendationScenarios)
        {
            try
            {
                Logs.Info("ValidationController.getRecommendedServicesScenarios /validation/runscenarios/services api has been called having parameter [recommendationScenarios]");
                string errorMSG = new AccessoryRecommendationScenariosValidator().validateRequest(recommendationScenarios);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = validationService.GetRecommendedAccessoriesScenarios(recommendationScenarios, Constants.ACCESSORY_TYPE_SERVICE);
                Logs.InfoFormat("The response of ValidationController.getRecommendedServicesScenarios /validation/runscenarios/services api is, Response[List<ProductSkuScenarioResponseDTO>, Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
                return Ok(productSkuScenarioResponseDTOList);

            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("validation/runscenarios/accessories")]
        [HttpPost]
        public async Task<IHttpActionResult> getRecommendedAccessoriesScenarios([FromBody] RecommendationScenarios recommendationScenarios)
        {
            try
            {
                Logs.Info("ValidationController.getRecommendedAccessoriesScenarios /validation/runscenarios/accessories api has been called having parameter [recommendationScenarios]");
                string errorMSG = new AccessoryRecommendationScenariosValidator().validateRequest(recommendationScenarios);
                if (!string.IsNullOrEmpty(errorMSG))
                {
                    Logs.Error(errorMSG);
                    return BadRequest(errorMSG);
                }

                List<ProductSkuScenarioResponseDTO> productSkuScenarioResponseDTOList = validationService.GetRecommendedAccessoriesScenarios(recommendationScenarios, Constants.ACCESSORY_TYPE_ACCESSORY);

                Logs.InfoFormat("The response of ValidationController.getRecommendedAccessoriesScenarios /validation/runscenarios/accessories api is, Response[List<ProductSkuScenarioResponseDTO>, Size: " + ((productSkuScenarioResponseDTOList == null) ? '-' : productSkuScenarioResponseDTOList.Count) + "]");
                return Ok(productSkuScenarioResponseDTOList);

            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
