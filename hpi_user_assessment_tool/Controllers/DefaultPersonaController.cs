﻿using hpi_user_assessment_tool;
using hpi_user_assessment_tool.Data;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using hpi_user_assessment_tool.Services;
using hpi_user_assessment_tool.Models;
using WebApi.Jwt.Filters;
using hpi_user_assessment_tool.Utility;

namespace hpi_user_assessment_tool.Controllers
{
    //TODO: Need to add Authorization for the API's
    // [Authorize]
    [JwtAuthentication]
    [RoutePrefix("api/v1")]
    public class DefaultPersonaController : ApiController
    {

        private readonly IDefaultPersonaService defaultPersonaService;

        public DefaultPersonaController(IDefaultPersonaService defaultPersonaService)
        {
            this.defaultPersonaService = defaultPersonaService;
        }

        [Route("defaultPersona")]
        [HttpGet]
        public async Task<IHttpActionResult> defaultPersona()
        {
            try
            {
                Logs.Info("DefaultPersonaController.defaultPersona /defaultPersona api has been called.");
                List<CMSDefaultPersona> liDefaultPersona = await defaultPersonaService.GetAllDefaultPersonaAsync();
                Logs.Info("The response of DefaultPersonaController.defaultPersona /defaultPersona api is, Response[List<CMSDefaultPersona>, Size: " + ((liDefaultPersona == null) ? '-' : liDefaultPersona.Count) + "]");
                return Ok(liDefaultPersona);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("defaultPersona/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> defaultPersona(int id)
        {
            try
            {
                Logs.Info("DefaultPersonaController.defaultPersona /defaultPersona/" + id + " api having parameter [Id: " + id + "]");
                CMSDefaultPersona defaultPersona = await defaultPersonaService.GetDefaultPersonaByIdAsync(id);
                
                Logs.Info("The response of DefaultPersonaController.defaultPersona /defaultPersona/{id} api is, Response[CMSDefaultPersona, Id: " + ((defaultPersona == null) ? '-' : defaultPersona.Id) + "]");
                return Ok(defaultPersona);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("defaultPersona/profile/{profileAnswerLabel}")]
        [HttpGet]
        public async Task<IHttpActionResult> defaultPersona(string profileAnswerLabel)
        {
            try
            {
                Logs.Info("DefaultPersonaController.defaultPersona /defaultPersona/profile/" + profileAnswerLabel + " api having parameter [profileAnswerLabel: " + profileAnswerLabel + "]");
                CMSDefaultPersona defaultPersona = await defaultPersonaService.GetDefaultPersonaByProfileAnswerLabelAsync(profileAnswerLabel);
                Logs.Info("The response of DefaultPersonaController.defaultPersona /defaultPersona/profile/{profileAnswerLabel} is, Response[CMSDefaultPersona, Id: " + ((defaultPersona == null) ? '-' : defaultPersona.Id) + "]");
                return Ok(defaultPersona);
            }
            catch (Exception ex)
            {
                Logs.ErrorFormat(ex.Message, ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
