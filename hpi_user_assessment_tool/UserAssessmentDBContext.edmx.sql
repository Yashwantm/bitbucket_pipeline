
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/28/2017 09:54:42
-- Generated from EDMX file: E:\workspace\HPI\hewl-hpi-web-api\hp-user-assessment-tool\hewl-hpi-web-api\hpi_user_assessment_tool\UserAssessmentDBContext.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [hp-uat-dev-db];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CMSQuestionCMSQuestionAssessmentStep]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSQuestionAssessmentSteps] DROP CONSTRAINT [FK_CMSQuestionCMSQuestionAssessmentStep];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSQuestionCMSQuestionInsight]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSQuestionInsights] DROP CONSTRAINT [FK_CMSQuestionCMSQuestionInsight];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSQuestionCMSAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSAnswers] DROP CONSTRAINT [FK_CMSQuestionCMSAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSAnswerCMSAnswerInsight]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSAnswerInsights] DROP CONSTRAINT [FK_CMSAnswerCMSAnswerInsight];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSAnswerCMSTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSTags] DROP CONSTRAINT [FK_CMSAnswerCMSTag];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSAnswerCMSWeightedTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSWeightedTags] DROP CONSTRAINT [FK_CMSAnswerCMSWeightedTag];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSTagCMSTagRule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSTagRules] DROP CONSTRAINT [FK_CMSTagCMSTagRule];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSWeightedTagCMSWeightedTagRule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSWeightedTagRules] DROP CONSTRAINT [FK_CMSWeightedTagCMSWeightedTagRule];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSDPWeightedTagCMSDPWeightedTagRule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSDPWeightedTagRules] DROP CONSTRAINT [FK_CMSDPWeightedTagCMSDPWeightedTagRule];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSDefaultPersonaCMSDPWeightedTag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSDPWeightedTags] DROP CONSTRAINT [FK_CMSDefaultPersonaCMSDPWeightedTag];
GO
IF OBJECT_ID(N'[dbo].[FK_UserCustomer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRCustomers] DROP CONSTRAINT [FK_UserCustomer];
GO
IF OBJECT_ID(N'[dbo].[FK_CustomerProfile]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRProfiles] DROP CONSTRAINT [FK_CustomerProfile];
GO
IF OBJECT_ID(N'[dbo].[FK_ProfileProfileAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRProfileAnswers] DROP CONSTRAINT [FK_ProfileProfileAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_ProfileSelectedAccessorie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRSelectedAccessories] DROP CONSTRAINT [FK_ProfileSelectedAccessorie];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSDefaultPersonaCMSDPAnswer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSDPAnswers] DROP CONSTRAINT [FK_CMSDefaultPersonaCMSDPAnswer];
GO
IF OBJECT_ID(N'[dbo].[FK_USRProfileAnswerUSRProfileAnswerOptions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRProfileAnswerOptions] DROP CONSTRAINT [FK_USRProfileAnswerUSRProfileAnswerOptions];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSDPAnswerCMSDPAnswerOptions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CMSDPAnswerOptions] DROP CONSTRAINT [FK_CMSDPAnswerCMSDPAnswerOptions];
GO
IF OBJECT_ID(N'[dbo].[FK_USRSelectedAccessoriesUSRSelectedAccessoriesSku]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRSelectedAccessorySkus] DROP CONSTRAINT [FK_USRSelectedAccessoriesUSRSelectedAccessoriesSku];
GO
IF OBJECT_ID(N'[dbo].[FK_USRProfileUSRSelectedProduct]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[USRSelectedProducts] DROP CONSTRAINT [FK_USRProfileUSRSelectedProduct];
GO
IF OBJECT_ID(N'[dbo].[FK_CMSAnswerimage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[images] DROP CONSTRAINT [FK_CMSAnswerimage];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CMSQuestions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSQuestions];
GO
IF OBJECT_ID(N'[dbo].[CMSQuestionAssessmentSteps]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSQuestionAssessmentSteps];
GO
IF OBJECT_ID(N'[dbo].[CMSQuestionInsights]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSQuestionInsights];
GO
IF OBJECT_ID(N'[dbo].[CMSAnswers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSAnswers];
GO
IF OBJECT_ID(N'[dbo].[CMSAnswerInsights]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSAnswerInsights];
GO
IF OBJECT_ID(N'[dbo].[CMSTags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSTags];
GO
IF OBJECT_ID(N'[dbo].[CMSWeightedTags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSWeightedTags];
GO
IF OBJECT_ID(N'[dbo].[CMSTagRules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSTagRules];
GO
IF OBJECT_ID(N'[dbo].[CMSWeightedTagRules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSWeightedTagRules];
GO
IF OBJECT_ID(N'[dbo].[CMSDefaultPersonas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSDefaultPersonas];
GO
IF OBJECT_ID(N'[dbo].[CMSDPWeightedTags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSDPWeightedTags];
GO
IF OBJECT_ID(N'[dbo].[CMSDPWeightedTagRules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSDPWeightedTagRules];
GO
IF OBJECT_ID(N'[dbo].[CMSDPAnswers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSDPAnswers];
GO
IF OBJECT_ID(N'[dbo].[USRInfoes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRInfoes];
GO
IF OBJECT_ID(N'[dbo].[USRCustomers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRCustomers];
GO
IF OBJECT_ID(N'[dbo].[USRProfiles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRProfiles];
GO
IF OBJECT_ID(N'[dbo].[USRProfileAnswers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRProfileAnswers];
GO
IF OBJECT_ID(N'[dbo].[USRSelectedProducts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRSelectedProducts];
GO
IF OBJECT_ID(N'[dbo].[USRSelectedAccessories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRSelectedAccessories];
GO
IF OBJECT_ID(N'[dbo].[USRProfileAnswerOptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRProfileAnswerOptions];
GO
IF OBJECT_ID(N'[dbo].[CMSDPAnswerOptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CMSDPAnswerOptions];
GO
IF OBJECT_ID(N'[dbo].[USRSelectedAccessorySkus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[USRSelectedAccessorySkus];
GO
IF OBJECT_ID(N'[dbo].[ProductSkus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSkus];
GO
IF OBJECT_ID(N'[dbo].[ProductSkuEZBuyListPrices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSkuEZBuyListPrices];
GO
IF OBJECT_ID(N'[dbo].[ProductFeatures]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductFeatures];
GO
IF OBJECT_ID(N'[dbo].[ProductAccessories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductAccessories];
GO
IF OBJECT_ID(N'[dbo].[ProductEDWIndustryTrends]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductEDWIndustryTrends];
GO
IF OBJECT_ID(N'[dbo].[ProductSeries]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSeries];
GO
IF OBJECT_ID(N'[dbo].[ProductSeriesTopRecAccessories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSeriesTopRecAccessories];
GO
IF OBJECT_ID(N'[dbo].[ProductSeriesFeatureMatrices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSeriesFeatureMatrices];
GO
IF OBJECT_ID(N'[dbo].[ProductSkuAccessories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSkuAccessories];
GO
IF OBJECT_ID(N'[dbo].[Locals]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Locals];
GO
IF OBJECT_ID(N'[dbo].[images]', 'U') IS NOT NULL
    DROP TABLE [dbo].[images];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'CMSQuestions'
CREATE TABLE [dbo].[CMSQuestions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [description] nvarchar(max)  NULL,
    [label] nvarchar(max)  NULL,
    [order] nvarchar(max)  NULL,
    [imageURL] nvarchar(max)  NULL,
    [displayType] nvarchar(max)  NULL,
    [localKey] nvarchar(max)  NULL
);
GO

-- Creating table 'CMSQuestionAssessmentSteps'
CREATE TABLE [dbo].[CMSQuestionAssessmentSteps] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [label] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [CMSQuestion_Id] int  NOT NULL
);
GO

-- Creating table 'CMSQuestionInsights'
CREATE TABLE [dbo].[CMSQuestionInsights] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [label] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [localKey] nvarchar(max)  NULL,
    [CMSQuestionId] int  NOT NULL
);
GO

-- Creating table 'CMSAnswers'
CREATE TABLE [dbo].[CMSAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [label] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [order] nvarchar(max)  NULL,
    [enabled] bit  NULL,
    [localKey] nvarchar(max)  NULL,
    [CMSQuestionId] int  NOT NULL
);
GO

-- Creating table 'CMSAnswerInsights'
CREATE TABLE [dbo].[CMSAnswerInsights] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [label] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [localKey] nvarchar(max)  NULL,
    [CMSAnswerId] int  NOT NULL
);
GO

-- Creating table 'CMSTags'
CREATE TABLE [dbo].[CMSTags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [CMSAnswerId] int  NOT NULL
);
GO

-- Creating table 'CMSWeightedTags'
CREATE TABLE [dbo].[CMSWeightedTags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [weight] nvarchar(max)  NULL,
    [CMSAnswerId] int  NOT NULL
);
GO

-- Creating table 'CMSTagRules'
CREATE TABLE [dbo].[CMSTagRules] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rule] nvarchar(max)  NULL,
    [comparison] nvarchar(max)  NULL,
    [values] nvarchar(max)  NULL,
    [CMSTagId] int  NOT NULL
);
GO

-- Creating table 'CMSWeightedTagRules'
CREATE TABLE [dbo].[CMSWeightedTagRules] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rule] nvarchar(max)  NULL,
    [comparison] nvarchar(max)  NULL,
    [values] nvarchar(max)  NULL,
    [CMSWeightedTagId] int  NOT NULL
);
GO

-- Creating table 'CMSDefaultPersonas'
CREATE TABLE [dbo].[CMSDefaultPersonas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [profileAnswerLabel] nvarchar(max)  NULL,
    [label] nvarchar(max)  NOT NULL,
    [description] nvarchar(max)  NULL,
    [countryCode] nvarchar(max)  NULL
);
GO

-- Creating table 'CMSDPWeightedTags'
CREATE TABLE [dbo].[CMSDPWeightedTags] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [weight] nvarchar(max)  NULL,
    [CMSDefaultPersonaId] int  NOT NULL
);
GO

-- Creating table 'CMSDPWeightedTagRules'
CREATE TABLE [dbo].[CMSDPWeightedTagRules] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [rule] nvarchar(max)  NULL,
    [comparison] nvarchar(max)  NULL,
    [values] nvarchar(max)  NULL,
    [CMSDPWeightedTagId] int  NOT NULL
);
GO

-- Creating table 'CMSDPAnswers'
CREATE TABLE [dbo].[CMSDPAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [questionLabel] nvarchar(max)  NOT NULL,
    [CMSDefaultPersonaId] int  NOT NULL
);
GO

-- Creating table 'USRInfoes'
CREATE TABLE [dbo].[USRInfoes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [userId] int  NOT NULL
);
GO

-- Creating table 'USRCustomers'
CREATE TABLE [dbo].[USRCustomers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [size] nvarchar(max)  NULL,
    [industry] nvarchar(max)  NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'USRProfiles'
CREATE TABLE [dbo].[USRProfiles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [defaultPersonaLabel] nvarchar(max)  NULL,
    [CustomerId] int  NOT NULL
);
GO

-- Creating table 'USRProfileAnswers'
CREATE TABLE [dbo].[USRProfileAnswers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [questionLabel] nvarchar(max)  NULL,
    [ProfileId] int  NOT NULL
);
GO

-- Creating table 'USRSelectedProducts'
CREATE TABLE [dbo].[USRSelectedProducts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [sku] nvarchar(max)  NULL,
    [itemId] nvarchar(max)  NULL,
    [USRProfileUSRSelectedProduct_USRSelectedProduct_Id] int  NOT NULL
);
GO

-- Creating table 'USRSelectedAccessories'
CREATE TABLE [dbo].[USRSelectedAccessories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [category] nvarchar(max)  NULL,
    [ProfileId] int  NOT NULL
);
GO

-- Creating table 'USRProfileAnswerOptions'
CREATE TABLE [dbo].[USRProfileAnswerOptions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [answerLabel] nvarchar(max)  NULL,
    [USRProfileAnswerId] int  NOT NULL
);
GO

-- Creating table 'CMSDPAnswerOptions'
CREATE TABLE [dbo].[CMSDPAnswerOptions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [answerLabel] nvarchar(max)  NULL,
    [CMSDPAnswerId] int  NOT NULL
);
GO

-- Creating table 'USRSelectedAccessorySkus'
CREATE TABLE [dbo].[USRSelectedAccessorySkus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [sku] nvarchar(max)  NULL,
    [USRSelectedAccessoriesId] int  NOT NULL
);
GO

-- Creating table 'ProductSkus'
CREATE TABLE [dbo].[ProductSkus] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [country] nvarchar(max)  NOT NULL,
    [language] nvarchar(max)  NOT NULL,
    [marketingCategory] nvarchar(max)  NOT NULL,
    [pmoid] nvarchar(max)  NOT NULL,
    [sku] nvarchar(max)  NOT NULL,
    [itemId] nvarchar(max)  NOT NULL,
    [name] nvarchar(max)  NOT NULL,
    [bigSeries] nvarchar(max)  NOT NULL,
    [smallSeries] nvarchar(max)  NOT NULL,
    [smallSeriesPmoid] nvarchar(max)  NOT NULL,
    [productLine] nvarchar(max)  NOT NULL,
    [status] nvarchar(max)  NOT NULL,
    [ann_date] nvarchar(max)  NOT NULL,
    [full_date] nvarchar(max)  NOT NULL,
    [obsolete_date] nvarchar(max)  NOT NULL,
    [primaryImageUrl] nvarchar(max)  NOT NULL,
    [audio] nvarchar(max)  NOT NULL,
    [audioFeat] nvarchar(max)  NOT NULL,
    [batteryType] nvarchar(max)  NOT NULL,
    [display] nvarchar(max)  NOT NULL,
    [displaydes] nvarchar(max)  NOT NULL,
    [expanslots] nvarchar(max)  NOT NULL,
    [facet_cap] nvarchar(max)  NOT NULL,
    [facet_feat] nvarchar(max)  NOT NULL,
    [facet_formfactor] nvarchar(max)  NOT NULL,
    [facet_graphics] nvarchar(max)  NOT NULL,
    [facet_maxres] nvarchar(max)  NOT NULL,
    [facet_memstd] nvarchar(max)  NOT NULL,
    [facet_os] nvarchar(max)  NOT NULL,
    [facet_processormax] nvarchar(max)  NOT NULL,
    [facet_procesortype] nvarchar(max)  NOT NULL,
    [facet_prodcat] nvarchar(max)  NOT NULL,
    [facet_prodtype] nvarchar(max)  NOT NULL,
    [facet_protocols] nvarchar(max)  NOT NULL,
    [facet_scrnsizeus] nvarchar(max)  NOT NULL,
    [facet_segment] nvarchar(max)  NOT NULL,
    [facet_seriesname] nvarchar(max)  NOT NULL,
    [facet_subbrand] nvarchar(max)  NOT NULL,
    [facet_subcategory] nvarchar(max)  NOT NULL,
    [facet_usage] nvarchar(max)  NOT NULL,
    [facet_weightus] nvarchar(max)  NOT NULL,
    [graphicseg_01card_01] nvarchar(max)  NOT NULL,
    [ioports] nvarchar(max)  NOT NULL,
    [ioports_2] nvarchar(max)  NOT NULL,
    [keybrd] nvarchar(max)  NOT NULL,
    [mousepntgdevice] nvarchar(max)  NOT NULL,
    [netinterface] nvarchar(max)  NOT NULL,
    [processor] nvarchar(max)  NOT NULL,
    [processorname] nvarchar(max)  NOT NULL,
    [prodname] nvarchar(max)  NOT NULL,
    [securitymgmt] nvarchar(max)  NOT NULL,
    [software] nvarchar(max)  NOT NULL,
    [wirelesstech] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductSkuEZBuyListPrices'
CREATE TABLE [dbo].[ProductSkuEZBuyListPrices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [sku] nvarchar(max)  NOT NULL,
    [listPrice] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductFeatures'
CREATE TABLE [dbo].[ProductFeatures] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [pmoid] nvarchar(max)  NOT NULL,
    [country] nvarchar(max)  NOT NULL,
    [language] nvarchar(max)  NOT NULL,
    [ksp_01_headline_short] nvarchar(max)  NOT NULL,
    [ksp_01_suppt_01_medium] nvarchar(max)  NOT NULL,
    [ksp_01_suppt_02_medium] nvarchar(max)  NOT NULL,
    [ksp_01_suppt_03_medium] nvarchar(max)  NOT NULL,
    [ksp_01_suppt_04_medium] nvarchar(max)  NOT NULL,
    [ksp_01_suppt_05_medium] nvarchar(max)  NOT NULL,
    [ksp_02_headline_short] nvarchar(max)  NOT NULL,
    [ksp_02_suppt_01_medium] nvarchar(max)  NOT NULL,
    [ksp_02_suppt_02_medium] nvarchar(max)  NOT NULL,
    [ksp_02_suppt_03_medium] nvarchar(max)  NOT NULL,
    [ksp_02_suppt_04_medium] nvarchar(max)  NOT NULL,
    [ksp_02_suppt_05_medium] nvarchar(max)  NOT NULL,
    [ksp_03_headline_short] nvarchar(max)  NOT NULL,
    [ksp_03_suppt_01_medium] nvarchar(max)  NOT NULL,
    [ksp_03_suppt_02_medium] nvarchar(max)  NOT NULL,
    [ksp_03_suppt_03_medium] nvarchar(max)  NOT NULL,
    [ksp_03_suppt_04_medium] nvarchar(max)  NOT NULL,
    [ksp_03_suppt_05_medium] nvarchar(max)  NOT NULL,
    [ksp_04_headline_short] nvarchar(max)  NOT NULL,
    [ksp_04_suppt_01_medium] nvarchar(max)  NOT NULL,
    [ksp_04_suppt_02_medium] nvarchar(max)  NOT NULL,
    [ksp_04_suppt_03_medium] nvarchar(max)  NOT NULL,
    [ksp_04_suppt_04_medium] nvarchar(max)  NOT NULL,
    [ksp_04_suppt_05_medium] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductAccessories'
CREATE TABLE [dbo].[ProductAccessories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [country] nvarchar(max)  NOT NULL,
    [language] nvarchar(max)  NOT NULL,
    [marketing_category] nvarchar(max)  NOT NULL,
    [marketing_category_pmoid] nvarchar(max)  NOT NULL,
    [marketing_sub_category] nvarchar(max)  NOT NULL,
    [marketing_sub_category_pmoid] nvarchar(max)  NOT NULL,
    [pmoid] nvarchar(max)  NOT NULL,
    [sku] nvarchar(max)  NOT NULL,
    [name] nvarchar(max)  NOT NULL,
    [bigSeries] nvarchar(max)  NOT NULL,
    [smallSeries] nvarchar(max)  NOT NULL,
    [smallSeriespmoid] nvarchar(max)  NOT NULL,
    [status] nvarchar(max)  NOT NULL,
    [ann_date] nvarchar(max)  NOT NULL,
    [full_date] nvarchar(max)  NOT NULL,
    [obsolete_date] nvarchar(max)  NOT NULL,
    [primaryImageUrl] nvarchar(max)  NOT NULL,
    [accoustemisspressby] nvarchar(max)  NOT NULL,
    [aspectRatio] nvarchar(max)  NOT NULL,
    [batterylife] nvarchar(max)  NOT NULL,
    [batterytype] nvarchar(max)  NOT NULL,
    [colour] nvarchar(max)  NOT NULL,
    [dimenus] nvarchar(max)  NOT NULL,
    [displayarea] nvarchar(max)  NOT NULL,
    [displaypixel] nvarchar(max)  NOT NULL,
    [displaytype] nvarchar(max)  NOT NULL,
    [ecom_facet_prodtype] nvarchar(max)  NOT NULL,
    [ecom_facet_usage] nvarchar(max)  NOT NULL,
    [energyeffcomp] nvarchar(max)  NOT NULL,
    [facet_environ] nvarchar(max)  NOT NULL,
    [facet_feat] nvarchar(max)  NOT NULL,
    [facet_interface] nvarchar(max)  NOT NULL,
    [facet_maxres] nvarchar(max)  NOT NULL,
    [facet_prodcat] nvarchar(max)  NOT NULL,
    [facet_prodtype] nvarchar(max)  NOT NULL,
    [facet_scrnsizeus] nvarchar(max)  NOT NULL,
    [facet_scrnsizemet] nvarchar(max)  NOT NULL,
    [facet_segment] nvarchar(max)  NOT NULL,
    [facet_subcategory] nvarchar(max)  NOT NULL,
    [facet_usage] nvarchar(max)  NOT NULL,
    [filter_coverage_cp] nvarchar(max)  NOT NULL,
    [filter_response_cp] nvarchar(max)  NOT NULL,
    [filter_supptype_cp_01] nvarchar(max)  NOT NULL,
    [filter_supptype_cp_05] nvarchar(max)  NOT NULL,
    [filter_term_cp] nvarchar(max)  NOT NULL,
    [formfactor] nvarchar(max)  NOT NULL,
    [info_quickqpec_doc_ww] nvarchar(max)  NOT NULL,
    [ioports] nvarchar(max)  NOT NULL,
    [keybrdtype] nvarchar(max)  NOT NULL,
    [keyboardfeat] nvarchar(max)  NOT NULL,
    [led] nvarchar(max)  NOT NULL,
    [monitorfeat] nvarchar(max)  NOT NULL,
    [multimedia] nvarchar(max)  NOT NULL,
    [prodfinish] nvarchar(max)  NOT NULL,
    [productcolour] nvarchar(max)  NOT NULL,
    [powerconsump] nvarchar(max)  NOT NULL,
    [weightus] nvarchar(max)  NOT NULL,
    [wirelesstechno] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductEDWIndustryTrends'
CREATE TABLE [dbo].[ProductEDWIndustryTrends] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [years] nvarchar(max)  NOT NULL,
    [country] nvarchar(max)  NOT NULL,
    [category] nvarchar(max)  NOT NULL,
    [quantity] nvarchar(max)  NOT NULL,
    [percent] nvarchar(max)  NOT NULL,
    [rank] nvarchar(max)  NOT NULL,
    [series_pmoid] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductSeries'
CREATE TABLE [dbo].[ProductSeries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [country] nvarchar(max)  NOT NULL,
    [language] nvarchar(max)  NOT NULL,
    [marketing_category] nvarchar(max)  NOT NULL,
    [pmoid] nvarchar(max)  NOT NULL,
    [name] nvarchar(max)  NOT NULL,
    [bigSeries] nvarchar(max)  NOT NULL,
    [bigSeriesPmoid] nvarchar(max)  NOT NULL,
    [smallSeries] nvarchar(max)  NOT NULL,
    [smallSeriesPmoid] nvarchar(max)  NOT NULL,
    [primaryImageURL] nvarchar(max)  NOT NULL,
    [facet_formfactor] nvarchar(max)  NOT NULL,
    [facet_prodcat] nvarchar(max)  NOT NULL,
    [facet_prodtype] nvarchar(max)  NOT NULL,
    [facet_seriesname] nvarchar(max)  NOT NULL,
    [facet_usage] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductSeriesTopRecAccessories'
CREATE TABLE [dbo].[ProductSeriesTopRecAccessories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [series_pmoid] nvarchar(max)  NOT NULL,
    [accessory_sku] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductSeriesFeatureMatrices'
CREATE TABLE [dbo].[ProductSeriesFeatureMatrices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Series_PMOID] nvarchar(max)  NOT NULL,
    [Design] nvarchar(max)  NOT NULL,
    [security] nvarchar(max)  NOT NULL,
    [manageability] nvarchar(max)  NOT NULL,
    [collaboration] nvarchar(max)  NOT NULL,
    [docking] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ProductSkuAccessories'
CREATE TABLE [dbo].[ProductSkuAccessories] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [sku_pmoid] nvarchar(max)  NOT NULL,
    [accessory_sku] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Locals'
CREATE TABLE [dbo].[Locals] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [country] nvarchar(max)  NOT NULL,
    [language] nvarchar(max)  NOT NULL,
    [key] nvarchar(max)  NOT NULL,
    [value] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'images'
CREATE TABLE [dbo].[images] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CMSAnswerId] int  NOT NULL,
    [url] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'CMSQuestions'
ALTER TABLE [dbo].[CMSQuestions]
ADD CONSTRAINT [PK_CMSQuestions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSQuestionAssessmentSteps'
ALTER TABLE [dbo].[CMSQuestionAssessmentSteps]
ADD CONSTRAINT [PK_CMSQuestionAssessmentSteps]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSQuestionInsights'
ALTER TABLE [dbo].[CMSQuestionInsights]
ADD CONSTRAINT [PK_CMSQuestionInsights]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSAnswers'
ALTER TABLE [dbo].[CMSAnswers]
ADD CONSTRAINT [PK_CMSAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSAnswerInsights'
ALTER TABLE [dbo].[CMSAnswerInsights]
ADD CONSTRAINT [PK_CMSAnswerInsights]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSTags'
ALTER TABLE [dbo].[CMSTags]
ADD CONSTRAINT [PK_CMSTags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSWeightedTags'
ALTER TABLE [dbo].[CMSWeightedTags]
ADD CONSTRAINT [PK_CMSWeightedTags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSTagRules'
ALTER TABLE [dbo].[CMSTagRules]
ADD CONSTRAINT [PK_CMSTagRules]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSWeightedTagRules'
ALTER TABLE [dbo].[CMSWeightedTagRules]
ADD CONSTRAINT [PK_CMSWeightedTagRules]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSDefaultPersonas'
ALTER TABLE [dbo].[CMSDefaultPersonas]
ADD CONSTRAINT [PK_CMSDefaultPersonas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSDPWeightedTags'
ALTER TABLE [dbo].[CMSDPWeightedTags]
ADD CONSTRAINT [PK_CMSDPWeightedTags]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSDPWeightedTagRules'
ALTER TABLE [dbo].[CMSDPWeightedTagRules]
ADD CONSTRAINT [PK_CMSDPWeightedTagRules]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSDPAnswers'
ALTER TABLE [dbo].[CMSDPAnswers]
ADD CONSTRAINT [PK_CMSDPAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRInfoes'
ALTER TABLE [dbo].[USRInfoes]
ADD CONSTRAINT [PK_USRInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRCustomers'
ALTER TABLE [dbo].[USRCustomers]
ADD CONSTRAINT [PK_USRCustomers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRProfiles'
ALTER TABLE [dbo].[USRProfiles]
ADD CONSTRAINT [PK_USRProfiles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRProfileAnswers'
ALTER TABLE [dbo].[USRProfileAnswers]
ADD CONSTRAINT [PK_USRProfileAnswers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRSelectedProducts'
ALTER TABLE [dbo].[USRSelectedProducts]
ADD CONSTRAINT [PK_USRSelectedProducts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRSelectedAccessories'
ALTER TABLE [dbo].[USRSelectedAccessories]
ADD CONSTRAINT [PK_USRSelectedAccessories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRProfileAnswerOptions'
ALTER TABLE [dbo].[USRProfileAnswerOptions]
ADD CONSTRAINT [PK_USRProfileAnswerOptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CMSDPAnswerOptions'
ALTER TABLE [dbo].[CMSDPAnswerOptions]
ADD CONSTRAINT [PK_CMSDPAnswerOptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'USRSelectedAccessorySkus'
ALTER TABLE [dbo].[USRSelectedAccessorySkus]
ADD CONSTRAINT [PK_USRSelectedAccessorySkus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductSkus'
ALTER TABLE [dbo].[ProductSkus]
ADD CONSTRAINT [PK_ProductSkus]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductSkuEZBuyListPrices'
ALTER TABLE [dbo].[ProductSkuEZBuyListPrices]
ADD CONSTRAINT [PK_ProductSkuEZBuyListPrices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductFeatures'
ALTER TABLE [dbo].[ProductFeatures]
ADD CONSTRAINT [PK_ProductFeatures]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductAccessories'
ALTER TABLE [dbo].[ProductAccessories]
ADD CONSTRAINT [PK_ProductAccessories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductEDWIndustryTrends'
ALTER TABLE [dbo].[ProductEDWIndustryTrends]
ADD CONSTRAINT [PK_ProductEDWIndustryTrends]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductSeries'
ALTER TABLE [dbo].[ProductSeries]
ADD CONSTRAINT [PK_ProductSeries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductSeriesTopRecAccessories'
ALTER TABLE [dbo].[ProductSeriesTopRecAccessories]
ADD CONSTRAINT [PK_ProductSeriesTopRecAccessories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductSeriesFeatureMatrices'
ALTER TABLE [dbo].[ProductSeriesFeatureMatrices]
ADD CONSTRAINT [PK_ProductSeriesFeatureMatrices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductSkuAccessories'
ALTER TABLE [dbo].[ProductSkuAccessories]
ADD CONSTRAINT [PK_ProductSkuAccessories]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Locals'
ALTER TABLE [dbo].[Locals]
ADD CONSTRAINT [PK_Locals]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'images'
ALTER TABLE [dbo].[images]
ADD CONSTRAINT [PK_images]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CMSQuestion_Id] in table 'CMSQuestionAssessmentSteps'
ALTER TABLE [dbo].[CMSQuestionAssessmentSteps]
ADD CONSTRAINT [FK_CMSQuestionCMSQuestionAssessmentStep]
    FOREIGN KEY ([CMSQuestion_Id])
    REFERENCES [dbo].[CMSQuestions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSQuestionCMSQuestionAssessmentStep'
CREATE INDEX [IX_FK_CMSQuestionCMSQuestionAssessmentStep]
ON [dbo].[CMSQuestionAssessmentSteps]
    ([CMSQuestion_Id]);
GO

-- Creating foreign key on [CMSQuestionId] in table 'CMSQuestionInsights'
ALTER TABLE [dbo].[CMSQuestionInsights]
ADD CONSTRAINT [FK_CMSQuestionCMSQuestionInsight]
    FOREIGN KEY ([CMSQuestionId])
    REFERENCES [dbo].[CMSQuestions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSQuestionCMSQuestionInsight'
CREATE INDEX [IX_FK_CMSQuestionCMSQuestionInsight]
ON [dbo].[CMSQuestionInsights]
    ([CMSQuestionId]);
GO

-- Creating foreign key on [CMSQuestionId] in table 'CMSAnswers'
ALTER TABLE [dbo].[CMSAnswers]
ADD CONSTRAINT [FK_CMSQuestionCMSAnswer]
    FOREIGN KEY ([CMSQuestionId])
    REFERENCES [dbo].[CMSQuestions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSQuestionCMSAnswer'
CREATE INDEX [IX_FK_CMSQuestionCMSAnswer]
ON [dbo].[CMSAnswers]
    ([CMSQuestionId]);
GO

-- Creating foreign key on [CMSAnswerId] in table 'CMSAnswerInsights'
ALTER TABLE [dbo].[CMSAnswerInsights]
ADD CONSTRAINT [FK_CMSAnswerCMSAnswerInsight]
    FOREIGN KEY ([CMSAnswerId])
    REFERENCES [dbo].[CMSAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSAnswerCMSAnswerInsight'
CREATE INDEX [IX_FK_CMSAnswerCMSAnswerInsight]
ON [dbo].[CMSAnswerInsights]
    ([CMSAnswerId]);
GO

-- Creating foreign key on [CMSAnswerId] in table 'CMSTags'
ALTER TABLE [dbo].[CMSTags]
ADD CONSTRAINT [FK_CMSAnswerCMSTag]
    FOREIGN KEY ([CMSAnswerId])
    REFERENCES [dbo].[CMSAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSAnswerCMSTag'
CREATE INDEX [IX_FK_CMSAnswerCMSTag]
ON [dbo].[CMSTags]
    ([CMSAnswerId]);
GO

-- Creating foreign key on [CMSAnswerId] in table 'CMSWeightedTags'
ALTER TABLE [dbo].[CMSWeightedTags]
ADD CONSTRAINT [FK_CMSAnswerCMSWeightedTag]
    FOREIGN KEY ([CMSAnswerId])
    REFERENCES [dbo].[CMSAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSAnswerCMSWeightedTag'
CREATE INDEX [IX_FK_CMSAnswerCMSWeightedTag]
ON [dbo].[CMSWeightedTags]
    ([CMSAnswerId]);
GO

-- Creating foreign key on [CMSTagId] in table 'CMSTagRules'
ALTER TABLE [dbo].[CMSTagRules]
ADD CONSTRAINT [FK_CMSTagCMSTagRule]
    FOREIGN KEY ([CMSTagId])
    REFERENCES [dbo].[CMSTags]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSTagCMSTagRule'
CREATE INDEX [IX_FK_CMSTagCMSTagRule]
ON [dbo].[CMSTagRules]
    ([CMSTagId]);
GO

-- Creating foreign key on [CMSWeightedTagId] in table 'CMSWeightedTagRules'
ALTER TABLE [dbo].[CMSWeightedTagRules]
ADD CONSTRAINT [FK_CMSWeightedTagCMSWeightedTagRule]
    FOREIGN KEY ([CMSWeightedTagId])
    REFERENCES [dbo].[CMSWeightedTags]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSWeightedTagCMSWeightedTagRule'
CREATE INDEX [IX_FK_CMSWeightedTagCMSWeightedTagRule]
ON [dbo].[CMSWeightedTagRules]
    ([CMSWeightedTagId]);
GO

-- Creating foreign key on [CMSDPWeightedTagId] in table 'CMSDPWeightedTagRules'
ALTER TABLE [dbo].[CMSDPWeightedTagRules]
ADD CONSTRAINT [FK_CMSDPWeightedTagCMSDPWeightedTagRule]
    FOREIGN KEY ([CMSDPWeightedTagId])
    REFERENCES [dbo].[CMSDPWeightedTags]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSDPWeightedTagCMSDPWeightedTagRule'
CREATE INDEX [IX_FK_CMSDPWeightedTagCMSDPWeightedTagRule]
ON [dbo].[CMSDPWeightedTagRules]
    ([CMSDPWeightedTagId]);
GO

-- Creating foreign key on [CMSDefaultPersonaId] in table 'CMSDPWeightedTags'
ALTER TABLE [dbo].[CMSDPWeightedTags]
ADD CONSTRAINT [FK_CMSDefaultPersonaCMSDPWeightedTag]
    FOREIGN KEY ([CMSDefaultPersonaId])
    REFERENCES [dbo].[CMSDefaultPersonas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSDefaultPersonaCMSDPWeightedTag'
CREATE INDEX [IX_FK_CMSDefaultPersonaCMSDPWeightedTag]
ON [dbo].[CMSDPWeightedTags]
    ([CMSDefaultPersonaId]);
GO

-- Creating foreign key on [UserId] in table 'USRCustomers'
ALTER TABLE [dbo].[USRCustomers]
ADD CONSTRAINT [FK_UserCustomer]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[USRInfoes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserCustomer'
CREATE INDEX [IX_FK_UserCustomer]
ON [dbo].[USRCustomers]
    ([UserId]);
GO

-- Creating foreign key on [CustomerId] in table 'USRProfiles'
ALTER TABLE [dbo].[USRProfiles]
ADD CONSTRAINT [FK_CustomerProfile]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[USRCustomers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CustomerProfile'
CREATE INDEX [IX_FK_CustomerProfile]
ON [dbo].[USRProfiles]
    ([CustomerId]);
GO

-- Creating foreign key on [ProfileId] in table 'USRProfileAnswers'
ALTER TABLE [dbo].[USRProfileAnswers]
ADD CONSTRAINT [FK_ProfileProfileAnswer]
    FOREIGN KEY ([ProfileId])
    REFERENCES [dbo].[USRProfiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProfileProfileAnswer'
CREATE INDEX [IX_FK_ProfileProfileAnswer]
ON [dbo].[USRProfileAnswers]
    ([ProfileId]);
GO

-- Creating foreign key on [ProfileId] in table 'USRSelectedAccessories'
ALTER TABLE [dbo].[USRSelectedAccessories]
ADD CONSTRAINT [FK_ProfileSelectedAccessorie]
    FOREIGN KEY ([ProfileId])
    REFERENCES [dbo].[USRProfiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ProfileSelectedAccessorie'
CREATE INDEX [IX_FK_ProfileSelectedAccessorie]
ON [dbo].[USRSelectedAccessories]
    ([ProfileId]);
GO

-- Creating foreign key on [CMSDefaultPersonaId] in table 'CMSDPAnswers'
ALTER TABLE [dbo].[CMSDPAnswers]
ADD CONSTRAINT [FK_CMSDefaultPersonaCMSDPAnswer]
    FOREIGN KEY ([CMSDefaultPersonaId])
    REFERENCES [dbo].[CMSDefaultPersonas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSDefaultPersonaCMSDPAnswer'
CREATE INDEX [IX_FK_CMSDefaultPersonaCMSDPAnswer]
ON [dbo].[CMSDPAnswers]
    ([CMSDefaultPersonaId]);
GO

-- Creating foreign key on [USRProfileAnswerId] in table 'USRProfileAnswerOptions'
ALTER TABLE [dbo].[USRProfileAnswerOptions]
ADD CONSTRAINT [FK_USRProfileAnswerUSRProfileAnswerOptions]
    FOREIGN KEY ([USRProfileAnswerId])
    REFERENCES [dbo].[USRProfileAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_USRProfileAnswerUSRProfileAnswerOptions'
CREATE INDEX [IX_FK_USRProfileAnswerUSRProfileAnswerOptions]
ON [dbo].[USRProfileAnswerOptions]
    ([USRProfileAnswerId]);
GO

-- Creating foreign key on [CMSDPAnswerId] in table 'CMSDPAnswerOptions'
ALTER TABLE [dbo].[CMSDPAnswerOptions]
ADD CONSTRAINT [FK_CMSDPAnswerCMSDPAnswerOptions]
    FOREIGN KEY ([CMSDPAnswerId])
    REFERENCES [dbo].[CMSDPAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSDPAnswerCMSDPAnswerOptions'
CREATE INDEX [IX_FK_CMSDPAnswerCMSDPAnswerOptions]
ON [dbo].[CMSDPAnswerOptions]
    ([CMSDPAnswerId]);
GO

-- Creating foreign key on [USRSelectedAccessoriesId] in table 'USRSelectedAccessorySkus'
ALTER TABLE [dbo].[USRSelectedAccessorySkus]
ADD CONSTRAINT [FK_USRSelectedAccessoriesUSRSelectedAccessoriesSku]
    FOREIGN KEY ([USRSelectedAccessoriesId])
    REFERENCES [dbo].[USRSelectedAccessories]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_USRSelectedAccessoriesUSRSelectedAccessoriesSku'
CREATE INDEX [IX_FK_USRSelectedAccessoriesUSRSelectedAccessoriesSku]
ON [dbo].[USRSelectedAccessorySkus]
    ([USRSelectedAccessoriesId]);
GO

-- Creating foreign key on [USRProfileUSRSelectedProduct_USRSelectedProduct_Id] in table 'USRSelectedProducts'
ALTER TABLE [dbo].[USRSelectedProducts]
ADD CONSTRAINT [FK_USRProfileUSRSelectedProduct]
    FOREIGN KEY ([USRProfileUSRSelectedProduct_USRSelectedProduct_Id])
    REFERENCES [dbo].[USRProfiles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_USRProfileUSRSelectedProduct'
CREATE INDEX [IX_FK_USRProfileUSRSelectedProduct]
ON [dbo].[USRSelectedProducts]
    ([USRProfileUSRSelectedProduct_USRSelectedProduct_Id]);
GO

-- Creating foreign key on [CMSAnswerId] in table 'images'
ALTER TABLE [dbo].[images]
ADD CONSTRAINT [FK_CMSAnswerimage]
    FOREIGN KEY ([CMSAnswerId])
    REFERENCES [dbo].[CMSAnswers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CMSAnswerimage'
CREATE INDEX [IX_FK_CMSAnswerimage]
ON [dbo].[images]
    ([CMSAnswerId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------